package p08_web_server;

import java.time.LocalDateTime;

public class Pedido {
	
	private String ipCliente;
	private LocalDateTime diaYHora;
	private WSURL urlReq;
	
	// constructores
	
	public Pedido(String ipCliente, LocalDateTime diaYHora, String urlReq) {
		super();
		this.ipCliente = ipCliente;
		this.diaYHora = diaYHora;
		this.urlReq = new WSURL(urlReq);
	}

	// getters / setters
	
	public String getIpCliente() {
		return this.ipCliente;
	}
	
	public LocalDateTime getDiaYHora() {
		return this.diaYHora;
	}
	
	public WSURL getURL() {
		return this.urlReq;
	}
	
}
