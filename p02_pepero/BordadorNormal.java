package p02_pepero;

/**
 * bordador normal
 * sólo puede trabajar sobre uniformes que no tienen una leyenda previa
 */
public class BordadorNormal extends Bordador {

	// consultas
	
	/**
	 * indica si el bordador puede cambiar la leyenda del uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return !uniforme.tieneLeyenda();
	}

}
