package p02_pepero.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import p02_pepero.BordadorDescosedor;
import p02_pepero.BordadorNormal;
import p02_pepero.CentroDeProcesamiento;
import p02_pepero.Costurero;
import p02_pepero.Deposito;
import p02_pepero.Fabrica;
import p02_pepero.Material;
import p02_pepero.Oficina;
import p02_pepero.Remendon;
import p02_pepero.Taller;
import p02_pepero.TintoreriaGrossa;
import p02_pepero.TintoreriaNormal;
import p02_pepero.Uniforme;

public class CentroDeProcesamientoTests {

	private Material lino = new Material("lino", 4, 5);
	private Material denim = new Material("denim", 6, 3);
	private Material poliester = new Material("poliéster", 5, 4); 
	
	private Uniforme uni1 = new Uniforme(lino, "rojo", 5);
	private Uniforme uni2 = new Uniforme(lino, "verde", 5);
	private Uniforme uni3 = new Uniforme(lino, "azul", 5);
	private Uniforme uni4 = new Uniforme(denim, "cian", 5);
	private Uniforme uni5 = new Uniforme(denim, "magenta", 5);
	private Uniforme uni6 = new Uniforme(denim, "amarillo", 5);
	private Uniforme uni7 = new Uniforme(poliester, "blanco", 5);
	private Uniforme uni8 = new Uniforme(poliester, "negro", 5);
	private Uniforme uni9 = new Uniforme(denim, "gris", 5);
	
	private Fabrica fabri = new Fabrica("fabri", 8.0);
	private Deposito depo = new Deposito("depo");
	private Oficina ofi = new Oficina("ofi", "verdemar");
	
	private CentroDeProcesamiento cdp = new CentroDeProcesamiento();
	
	private TintoreriaNormal tinton = new TintoreriaNormal();
	private TintoreriaGrossa tintog = new TintoreriaGrossa();
	private Costurero costu = new Costurero();
	private Remendon remen = new Remendon();
	private BordadorNormal bordan = new BordadorNormal();
	private BordadorDescosedor bordad = new BordadorDescosedor();
	
	@Before
	public void b4() {
		// para ofi, listos para entregar
		uni1.setCliente(ofi);
		uni1.setColor(uni1.colorRequerido());
		uni1.setLeyenda(uni1.leyendaRequerida());
		uni1.setValor(300);
		
		uni2.setCliente(ofi);
		uni2.setColor(uni2.colorRequerido());
		uni2.setLeyenda(uni2.leyendaRequerida());
		uni2.setValor(300);
		
		// para ofi, no está listo
		uni3.setCliente(ofi);
		uni3.setValor(300);
		
		// para depo, listos para entregar
		uni4.setCliente(depo);
		uni4.setCosturasExtra(1);
		uni4.setColor(uni4.colorRequerido());
		uni4.setLeyenda(uni4.leyendaRequerida());
		uni4.setValor(400);
		
		uni5.setCliente(depo);
		uni5.setCosturasExtra(1);
		uni5.setColor(uni5.colorRequerido());
		uni5.setLeyenda(uni5.leyendaRequerida());
		uni5.setValor(400);
		
		// para depo, no está listo
		uni6.setCliente(depo);
		uni6.setValor(400);
		
		// para fabri, no están listos
		uni7.setCliente(fabri);
		uni7.setValor(350);
		
		uni8.setCliente(fabri);
		uni8.setValor(350);
		
		uni9.setCliente(depo);
		uni9.setColor("beige");
		
		tinton.agregarMaterialTenhible(lino);
		tinton.agregarMaterialTenhible(denim);
		tinton.agregarColorTransformable("blanco");
		tinton.agregarColorTransformable("beige");
		
		for (Uniforme uniforme : Arrays.asList(uni1, uni2, uni3, uni4, uni5, uni6, uni7, uni8, uni9)) {
			cdp.recibirUniforme(uniforme);	
		}
		
		for (Taller taller : Arrays.asList(tinton, tintog, costu, remen, bordan, bordad)) {
			cdp.agregarTaller(taller);			
		}
		
	}
	
	@Test
	public void testEj4aClientesConUniformesListos() {
		
		assertTrue(cdp.clientesConUniformesListos().contains(ofi));
		assertTrue(cdp.clientesConUniformesListos().contains(depo));
		assertFalse(cdp.clientesConUniformesListos().contains(fabri));
		
		uni7.setCosturasExtra(3);
		uni7.setLeyenda(uni7.leyendaRequerida());
		assertTrue(cdp.clientesConUniformesListos().contains(fabri));

	}
	
	@Test
	public void testEj4bCuantosUniformesListos() {
		
		assertEquals(4, cdp.cantUniformesListos());

		uni7.setCosturasExtra(3);
		uni7.setLeyenda(uni7.leyendaRequerida());
		assertEquals(5, cdp.cantUniformesListos());

	}
	
	@Test
	public void testEj4cValorTotalUniformesListos() {
		
		assertEquals(1400, cdp.valorTotalUniformesListos(), 0);
		
		uni7.setCosturasExtra(3);
		uni7.setLeyenda(uni7.leyendaRequerida());
		assertEquals(1750, cdp.valorTotalUniformesListos(), 0);
		
	}

	@Test // parcial, no comprueba que ofi los recibió
	public void testEj5aRegistroDeEnvios() {
		
		assertTrue(cdp.clientesConUniformesListos().contains(ofi));
		
		cdp.entregarUniformesListosDe(ofi);
		assertFalse(cdp.clientesConUniformesListos().contains(ofi));
		
	}
	
	@Test
	public void testEj6c1QueTalleresTrabajaronEnUniformesDelCliente() {
		
		assertTrue(cdp.talleresQueProcesaronUniformesDe(fabri).isEmpty());
		
		assertFalse(cdp.talleresQueProcesaronUniformesDe(fabri).contains(remen));
		
		uni7.pasarPorTaller(remen);
		assertTrue(cdp.talleresQueProcesaronUniformesDe(fabri).contains(remen));
		
	}
	
	@Test
	public void testEj6c2QueUniformesPasaronPorUnTaller() {
		
		assertTrue(cdp.uniformesQuePasaronPor(remen).isEmpty());
		
		assertFalse(cdp.uniformesQuePasaronPor(remen).contains(uni1));
		
		uni1.pasarPorTaller(remen);
		assertTrue(cdp.uniformesQuePasaronPor(remen).contains(uni1));
		
	}
	
	@Test // parcial, sólo verifia que no pase por taller cuando no es ne'sario
	public void testEj7EnviarATaller() {
		// pasarPor(taller) ya está testeado en Uniforme

		// tintorerias
		assertFalse(uni9.pasoPor(tintog));
		assertFalse(uni9.pasoPor(tinton));

		assertFalse(uni9.tieneColorRequerido());
		
		cdp.enviarUniformeA(tinton, uni9);
		assertTrue(uni9.tieneColorRequerido());
		assertTrue(uni9.pasoPor(tinton));
		assertFalse(uni9.pasoPor(tintog));
		
		cdp.enviarUniformeA(tintog, uni9);
		assertFalse(uni9.pasoPor(tintog));
		
		// costurero
		uni9.setRefuerzos(1);
		uni9.setCosturasExtra(1);
		assertTrue(uni9.tieneResistenciaRequerida());
		assertFalse(uni9.pasoPor(costu));
		
		cdp.enviarUniformeA(costu, uni9);
		assertFalse(uni9.pasoPor(costu));
		
		uni9.setCosturasExtra(0);
		assertFalse(uni9.tieneResistenciaRequerida());
		
		cdp.enviarUniformeA(costu, uni9);
		assertTrue(uni9.pasoPor(costu));
		
		// remendon
		uni9.setRefuerzos(1);
		uni9.setCosturasExtra(1);
		assertTrue(uni9.tieneResistenciaRequerida());
		assertFalse(uni9.pasoPor(remen));
		
		cdp.enviarUniformeA(remen, uni9);
		assertFalse(uni9.pasoPor(remen));
		
		uni9.setCosturasExtra(0);
		assertFalse(uni9.tieneResistenciaRequerida());
		
		cdp.enviarUniformeA(remen, uni9);
		assertTrue(uni9.pasoPor(remen));
				
		// bordadores
		assertFalse(uni9.pasoPor(bordad));
		assertFalse(uni9.pasoPor(bordan));

		assertFalse(uni9.tieneLeyendaRequerida());
		
		cdp.enviarUniformeA(bordad, uni9);
		assertTrue(uni9.tieneLeyendaRequerida());
		assertTrue(uni9.pasoPor(bordad));
		assertFalse(uni9.pasoPor(bordan));
		
		cdp.enviarUniformeA(bordan, uni9);
		assertFalse(uni9.pasoPor(bordan));
		
	}
	
}
