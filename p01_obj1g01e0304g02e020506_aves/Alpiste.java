package p01_obj1g01e0304g02e020506_aves;

public class Alpiste implements Comida {

	protected int gramos;

	// constructor
	public Alpiste(int gramos) {
		super();
		this.gramos = gramos;
	}
	
	@Override
	public int energia() {
		return this.gramos * 4;
	}

}
