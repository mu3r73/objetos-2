package p07_civilization;

public class EstadoConvulsionada extends EstadoNormal {
	
	@Override
	public boolean esFeliz(Ciudad ciudad) {
		return ciudad.getPotenciaTotDeUMilitares() >= 100;
	}
	
	@Override
	public void aumentarTesoroImperioProduccEdifEconomicos(Ciudad ciudad) {
		// no pasa nada
	}
	
	@Override
	public void boostearEdifCultural(Ciudad ciudad) {
		// no pasa nada
	}
	
	@Override
	public void boostearEdifEconomico(Ciudad ciudad) {
		// no pasa nada
	}
	
	@Override
	public void boostearEdifMilitar(Ciudad ciudad) {
		// no pasa nada
	}
	
	@Override
	public Estado getEstadoMejor() {
		return new EstadoNormal();
	}
	
	@Override
	public Estado getEstadoPeor() {
		return this;
	}
	
}
