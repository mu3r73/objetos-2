package venta_masiva.modelo.pedido;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.cliente.Movimiento;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.sector.SectorSimple;

/**
 * representa un pedido -
 * tiene un cliente, una fecha, y una lista de productos (+ un envío)
 */
public class Pedido extends Movimiento {

	private Collection<Producto> productos = new ArrayList<>();
	private Envio envio;
	
	// constructores
	public Pedido(Cliente cliente, Producto ... productos) {
		this(cliente, LocalDateTime.now(), productos);
	}
	
	public Pedido(Cliente cliente, LocalDateTime fecha, Producto ... productos) {
		super(cliente, fecha);
		this.setProductos(productos);
		cliente.agregarPedido(this);
	}
	
	// getters / setters
	public Collection<Producto> getProductos() {
		return this.productos;
	}
	
	public void setProductos(Producto ... productos) {
		this.productos.clear();
		for (Producto producto : productos) {
			this.agregarProducto(producto);
		}
	}
	
	// altas a colecciones
	public void agregarProducto(Producto producto) {
		this.productos.add(producto);
	}
	
	// consultas
	/**
	 * retorna el precio de venta total del pedido
	 * @return la suma de los precios de venta de los productos del pedido
	 */
	public double getPrecioVentaTotal() {
		return this.productos.stream()
				.mapToDouble(producto -> producto.getPrecioVenta())
				.sum();
	}
	
	/**
	 * indica si el pedido tiene un envío asociado
	 * @return true si el pedido tiene envío; false en caso contrario
	 */
	public boolean tieneEnvio() {
		return this.envio != null;
	}
	
	// acciones
	/**
	 * retorna un nuevo envío en base al pedido 
	 * @return un nuevo envío, con un elemento para cada producto en este pedido
	 */
	public Envio crearEnvio() {
		Envio envio = new Envio(Expedicion.getNumEnvio(), this);
		this.agregarseAEnvio(envio);
		return envio;
	}
	
	/**
	 * agrega este pedido a un envío previamente creado
	 * @param envio envío pre-existente
	 */
	public void agregarseAEnvio(Envio envio) {
		envio.agregarPedido(this);
		this.envio = envio;
		this.agregarElementosAEnvio(envio);
	}
	
	/**
	 * agrega un elemento al envío para cada producto en este pedido
	 * @param envio envío al que se van a agregar los elementos
	 */
	private void agregarElementosAEnvio(Envio envio) {
		for (Producto producto : this.productos) {
			this.agregarElemento(envio, producto);
		}
	}
	
	/**
	 * agrega un elemento al envío, correspondiente al producto indicado
	 * (genera una excepción si no hay stock del producto en depósito)
	 * @param envio envío al que se va a agregar el elemento
	 * @param producto producto para el que se desea agregar un elemento
	 */
	private void agregarElemento(Envio envio, Producto producto) {
		Deposito depo = Deposito.getInstancia();
		Optional<Elemento<? extends Producto>> opte = depo.obtenerUnElementoDe(producto);
		if (opte.isPresent()) {
			Elemento<? extends Producto> elemento = opte.get();
			SectorSimple ss = depo.hallarSectorQueContieneElementoNro(elemento.getNroInventario());
			ss.retirarElemento(elemento);
			envio.agregarElemento(elemento);
			envio.agregarEnvoltorios(elemento.crearEnvoltorios());
			envio.agregarCierres(elemento.crearCierres());
		} else {
			throw new RuntimeException("no hay stock de " + producto.getDescripcion()
										+ " en depósito para el pedido de " + this.cliente.getNombre());
		}
	}

}
