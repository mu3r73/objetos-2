package venta_masiva.modelo.item.producto;

import java.util.Collection;
import java.util.HashSet;

import venta_masiva.modelo.item.elemento.Botella;

/**
 * representa un vino, con peso, costo, precio de venta, bodega y cepas
 */
public class Vino extends Producto {

	private String bodega;
	private Collection<String> cepas = new HashSet<>();

	// constructor
	public Vino(double peso, double costo, double precioVenta,
			String bodega, String ... cepas) {
		super(peso, costo, precioVenta);
		this.bodega = bodega;
		this.setCepas(cepas);
	}

	// getters / setters
	public void setCepas(String ... cepas) {
		this.cepas.clear();
		for (String cepa : cepas) {
			this.agregarCepa(cepa);
		}
	}

	public String getBodega() {
		return this.bodega;
	}
	
	// altas a colecciones
	public void agregarCepa(String cepa) {
		this.cepas.add(cepa);
	}

	@Override
	public String getDescripcion() {
		return this.bodega + ", cepa " + this.cepas.toString();
	}
	
	// acciones
	/**
	 * retorna una nueva botella, correspondiente al vino
	 * @param nroInventario número de inventario a asignarle a la botella
	 * @return una nueva botella, correspondiente al vino
	 */
	public Botella crearBotella(int nroInventario) {
		return new Botella(this, nroInventario);
	}

}
