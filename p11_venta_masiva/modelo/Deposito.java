package venta_masiva.modelo;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.sector.SectorCompuesto;
import venta_masiva.modelo.sector.SectorSimple;

/**
 * clase singleton -
 * representa un depósito, dividido en sectores
 */
public class Deposito {

	private static Deposito instancia;
	
	// números correlativos de inventario para elementos en depósito
	private static int nroInventario = 0;
	
	// sector principal del depósito ('contiene' a todos los demás)
	private SectorCompuesto sectorRaiz;
	
	// constructor
	private Deposito() {
		super();
		this.sectorRaiz = new SectorCompuesto("Deposito");
	}
	
	// instancia - singleton
	public static Deposito getInstancia() {
		if (Deposito.instancia == null) {
			Deposito.instancia = new Deposito();
		}
		return Deposito.instancia;
	}
	
	// getters / setters
	public SectorCompuesto getSectorRaiz() {
		return this.sectorRaiz;
	}
	
	// consultas
	/**
	 * retorna el sector compuesto del depósito, con el nombre indicado
	 * (requiere que no haya sectores con nombres repetidos)
	 * @param nombre nombre del sector buscado
	 * @return el sector con ese nombre
	 */
	public SectorCompuesto hallarSectorCompuestoLlamado(String nombre) {
		return this.sectorRaiz.hallarSectorCompuestoLlamado(nombre);
	}
	
	/**
	 * retorna un sector simple del depósito, con el nombre indicado
	 * (requiere que no haya sectores con nombres repetidos)
	 * @param nombre nombre del sector buscado
	 * @return el sector con ese nombre
	 */
	public SectorSimple hallarSectorSimpleLlamado(String nombre) {
		return this.sectorRaiz.hallarSectorSimpleLlamado(nombre);
	}
	
	/**
	 * retorna el sector del depósito que contiene el elemento con el número de inventario indicado
	 * (si no hay stock del elemento en depósito, genera una excepción)
	 * @param nroInventario número de inventario del elemento buscado
	 * @return el sector que contiene el elemento con ese número de inventario
	 */
	public SectorSimple hallarSectorQueContieneElementoNro(int nroInventario) {
		SectorSimple ss = this.sectorRaiz.hallarSectorQueContieneElementoNro(nroInventario);
		if (ss == null) {
			throw new RuntimeException("no hay un elemento en el depósito con número de inventario " + nroInventario);
		}
		return ss;
	}
	
	/**
	 * retorna los libros en depósito que pertenecen al/los autor/es indicado/s
	 * @param autores autor/es buscados
	 * @return los libros de ese/esos autor/es
	 */
	public Set<Libro> obtenerLibrosDe(String ... autores) {
		return this.sectorRaiz.obtenerLibrosDe(autores);
	}
	
	/**
	 * retorna los elementos en depósito correspondientes al producto indicado
	 * @param producto producto buscado
	 * @return los elementos correspondientes a ese producto
	 */
	public Set<Elemento<? extends Producto>> obtenerElementosDe(Producto producto) {
		return this.sectorRaiz.obtenerElementosDe(producto);
	}
	
	/**
	 * retorna un elemento en depósito correspondiente al producto indicado
	 * @param producto producto buscado
	 * @return un elemento correspondiente a ese producto
	 */
	public Optional<Elemento<? extends Producto>> obtenerUnElementoDe(Producto producto) {
		return this.obtenerElementosDe(producto).stream()
				.findAny();
	}
	
	/**
	 * retorna los faltantes en depósito para satisfacer los pedidos en Expedición aún no enviados
	 * @return map de producto a cantidad faltante para satisfacer los pedidos no enviados
	 */
	public Map<Producto, Integer> obtenerFaltantes() {
		Map<Producto, Integer> faltantes = new HashMap<>();
		for (Producto producto : Store.getInstancia().getProductos()) {
			int necesito = Expedicion.getInstancia().cuantosElementosFaltaEnviar(producto);
			int hay = this.cantidadEnStock(producto);
			if (hay - necesito < 0) {
				faltantes.put(producto, necesito - hay);
			}
		}
		return faltantes;
	}
	
	/**
	 * retorna la cantidad en stock del producto indicado
	 * @param producto producto buscado
	 * @return cantidad en stock de ese producto
	 */
	private int cantidadEnStock(Producto producto) {
		return this.obtenerElementosDe(producto).size();
	}
	
	// acciones
	/**
	 * retorna un número único de inventario (para ser asignado a un elemento)
	 * @return número único de inventario
	 */
	public static int getNroInventario() {
		return Deposito.nroInventario++;
	}
	
	// para pruebas
	/**
	 * elimina *todos* los elementos en el depósito
	 */
	public void eliminarElementos() {
		this.sectorRaiz.eliminarElementos();
	}
	
	// datos de prueba
	/**
	 * genera una estructura de sectores posible para el depósito
	 */
	public void cargarDatosDePrueba() {
		if (!this.sectorRaiz.estaVacio()) {
			return;
		}
		
		// creo estructura
		SectorCompuesto pb = new SectorCompuesto("Planta Baja");
		SectorCompuesto a = new SectorCompuesto("A");
		a.agregarSubSectores(new SectorSimple("A1"), new SectorSimple("A2"));
		pb.agregarSubSectores(a, new SectorSimple("B"), new SectorSimple("C"));
		
		SectorSimple pp = new SectorSimple("Primer Piso");
		
		SectorCompuesto ss = new SectorCompuesto("Subsuelo");
		SectorCompuesto f = new SectorCompuesto("F");
		f.agregarSubSectores(new SectorSimple("F1"), new SectorSimple("F2"));
		ss.agregarSubSectores(new SectorSimple("D"), new SectorSimple("E"), f);
		
		this.sectorRaiz.agregarSubSectores(pb, pp, ss);

		/*
		 * Depósito
		 *  |
		 *  +--Planta Baja
		 *  |   |
		 *  |   +--Sector A
		 *  |   |   |
		 *  |   |   +--Sector A1
		 *  |   |   |
		 *  |   |   +--Sector A2
		 *  |	|
		 *  |   +--Sector B
		 *  |   |
		 *  |   +--Sector C
		 *  |
		 *  +--Primer Piso
		 *  |
		 *  +--Subsuelo
		 *      |
		 *      +--Sector D
		 *      |
		 *      +--Sector E
		 *      |
		 *      +--Sector F
		 *          |
		 *          +--Sector F1
		 *          |
		 *          +--Sector F2
		 */
	}
	
}
