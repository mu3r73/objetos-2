package p03_minions;

/**
 * bíclope
 * Minion con 1 ojo
 */
public class Ciclope extends Minion {

	// consultas
	
	/**
	 * retorna la dificultad de defender el sector
	 */
	@Override
	public double dificultadDeDefender(Sector sector) {
		return 2 * sector.getAmenaza();
	}

	/**
	 * retorna la fuerza del Biclope
	 */
	@Override
	protected double fuerza() {
		return super.fuerza() / 2;
	}

}
