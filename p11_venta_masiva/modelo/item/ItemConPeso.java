package venta_masiva.modelo.item;

/**
 * clase abstracta -
 * representa un item con peso
 */
public abstract class ItemConPeso {

	private double peso; // peso en gramos
	
	// constructor
	public ItemConPeso(double peso) {
		super();
		this.peso = peso;
	}
	
	// getters / setters
	public double getPeso() {
		return this.peso;
	}
	
}
