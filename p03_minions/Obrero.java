package p03_minions;

import java.util.HashSet;
import java.util.Set;

/**
 * Obrero: Rol específico de un empleado
 */
public class Obrero extends Rol {

	private Set<Herramienta> herramientas = new HashSet<>();

	// constructores
	
	public Obrero(Minion minion) {
		super(minion);
	}
	
	// altas a colecciones

	public void agregarHerramienta(Herramienta h) {
		this.herramientas.add(h);
	}
	
	// consultas
	
	/**
	 * indica si el Obrero tiene las herramientas necesarias
	 */
	@Override
	protected boolean tieneHerramientasNecesarias(Set<Herramienta> hs) {
		return super.tieneHerramientasNecesarias(hs)
				|| this.tieneTodasLasHerramientasEn(hs);
	}
	
	/**
	 * indica si el obrero tiene todas las herramientas en hs
	 */
	private boolean tieneTodasLasHerramientasEn(Set<Herramienta> hs) {
		return hs.containsAll(hs);
	}

}
