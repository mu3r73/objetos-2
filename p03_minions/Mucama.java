package p03_minions;

/**
 * Mucama: Rol específico de un empleado
 */
public class Mucama extends Rol {

	// constructores
	
	public Mucama(Minion minion) {
		super(minion);
	}
	
	// consultas

	/**
	 * endica si la Mucama puede defender el sector
	 */
	@Override
	public boolean puedeDefender(Sector sector) {
		return false;
	}
	
	/**
	 * indica si la Mucama puede limpiar el sector
	 */
	@Override
	public boolean puedeLimpiar(Sector sector) {
		return true;
	}

	// acciones
	
	/**
	 * lanza error si se le pide a la mucama que defienda el sector
	 */
	@Override
	public void defender(Sector sector) {
		throw new RuntimeException("mucama no puede defender sector");
	}

	/**
	 * limpia el sector
	 */
	@Override
	public void limpiar(Sector sector) {
		// no pasa nada
	}

}
