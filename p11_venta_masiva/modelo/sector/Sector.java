package venta_masiva.modelo.sector;

import java.util.Set;

import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;

/**
 * clase abstracta -
 * representa un sector de una estructura 'depósito'
 */
public abstract class Sector {

	protected String nombre;

	// constructor
	public Sector(String nombre) {
		this.nombre = nombre;
	}

	// getters / setters
	public String getNombre() {
		return this.nombre;
	}
	
	// consultas
	/**
	 * retorna un sector compuesto de la estructura, con el nombre indicado
	 * (requiere que no haya sectores con nombres repetidos)
	 * @param nombre nombre del sector buscado
	 * @return el sector con ese nombre
	 */
	public abstract SectorCompuesto hallarSectorCompuestoLlamado(String nombre);
	
	/**
	 * retorna un sector simple de la estructura con el nombre indicado
	 * (requiere que no haya sectores con nombres repetidos)
	 * @param nombre nombre del sector buscado
	 * @return el sector con ese nombre
	 */
	public abstract SectorSimple hallarSectorSimpleLlamado(String nombre);
	
	/**
	 * indica si el sector está vacío
	 * @return true si el sector está vacío, false en caso contrario
	 */
	public abstract boolean estaVacio();
	
	/**
	 * retorna el sector de la estructura que contiene un elemento con el número de inventario indicado
	 * @param nroInventario número de inventario del elemento buscado
	 * @return el sector que contiene el elemento con ese número de inventario
	 */
	public abstract SectorSimple hallarSectorQueContieneElementoNro(int nroInventario);
	
	/**
	 * retorna los libros en la estructura que pertenecen al/los autor/es indicado/s
	 * @param autores autor/es buscados
	 * @return los libros de ese/esos autor/es
	 */
	public abstract Set<Libro> obtenerLibrosDe(String ... autores);
	
	/**
	 * retorna los elementos en la estructura correspondientes al producto indicado
	 * @param producto producto buscado
	 * @return los elementos correspondientes a ese producto
	 */
	public abstract Set<Elemento<? extends Producto>> obtenerElementosDe(Producto producto);
	
	// para pruebas
	/**
	 * elimina *todos* los elementos en la estructura
	 */
	public abstract void eliminarElementos();
	
}
