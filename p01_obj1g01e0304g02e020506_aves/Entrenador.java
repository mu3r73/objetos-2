package p01_obj1g01e0304g02e020506_aves;

public class Entrenador {

	public void entrenar(Ave ave) {
		ave.volar(10);
		ave.comer(new Alpiste(300));
		ave.volar(10);
		ave.haceLoQueQuieras();
	}
	
}
