package p02_pepero.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.BordadorDescosedor;
import p02_pepero.BordadorNormal;
import p02_pepero.Costurero;
import p02_pepero.Deposito;
import p02_pepero.Fabrica;
import p02_pepero.Material;
import p02_pepero.Oficina;
import p02_pepero.Remendon;
import p02_pepero.TintoreriaGrossa;
import p02_pepero.TintoreriaNormal;
import p02_pepero.Uniforme;

public class UniformeTests {

	private Material lino = new Material("lino", 4, 5);
	
	private Uniforme uni = new Uniforme(lino, "púrpura", 5);
	
	private Fabrica fabri = new Fabrica("fabri", 8.0);
	private Deposito depo = new Deposito("depo");
	private Oficina ofi = new Oficina("ofi", "verdemar");

	private TintoreriaNormal tinton = new TintoreriaNormal();
	private TintoreriaGrossa tintog = new TintoreriaGrossa();
	private Costurero costu = new Costurero();
	private Remendon remen = new Remendon();
	private BordadorNormal bordan = new BordadorNormal();
	private BordadorDescosedor bordad = new BordadorDescosedor();
	
	@Test
	public void testEj1GradoDeResistencia() {
		
		uni.setRefuerzos(2);
		uni.setCosturasExtra(3);
		assertEquals(8.0, uni.resistencia(), 0);
		
	}

	@Test
	public void testEj3aEsDelColorCorrecto() {
		
		// uni es púrpura
		
		// fábrica = le viene bien cualquier color
		uni.setCliente(fabri);
		assertTrue(uni.tieneColorRequerido());
		
		uni.setColor("rojo");
		assertTrue(uni.tieneColorRequerido());
		
		// depósito = espera color negro, azul o marrón
		uni.setCliente(depo); //
		assertFalse(uni.tieneColorRequerido());
		
		uni.setColor("azul");
		assertTrue(uni.tieneColorRequerido());
				
		// oficina = espera color verdemar
		uni.setCliente(ofi);
		assertFalse(uni.tieneColorRequerido());
		
		uni.setColor("verdemar");
		assertTrue(uni.tieneColorRequerido());
		
	}
	
	@Test
	public void testEj3bTieneGradoDeResistenciaSuficiente() {
		
		uni.setRefuerzos(2);
		uni.setCosturasExtra(2);
		// resistencia < 8
		
		uni.setCliente(fabri); // espera resistencia >= 8
		assertFalse(uni.tieneResistenciaRequerida());
		
		uni.agregarCosturaExtra();
		// resistencia == 8
		assertTrue(uni.tieneResistenciaRequerida());
		
	}

	@Test
	public void testEj3cTieneLeyendaCorrecta() {
		
		uni.setCliente(ofi);
		uni.setLeyenda("pepero");
		assertFalse(uni.tieneLeyendaRequerida());
		
		uni.setLeyenda(uni.leyendaRequerida());
		assertTrue(uni.tieneLeyendaRequerida());
		
	}
	
	@Test
	public void testEj3dYaSePuedeEntregar() {
		
		uni.setCliente(depo);
		assertFalse(uni.yaSePuedeEntregar());
		
		uni.setColor("negro");
		assertFalse(uni.yaSePuedeEntregar());
		
		uni.setRefuerzos(2);
		uni.setCosturasExtra(3);
		assertFalse(uni.yaSePuedeEntregar());
		
		uni.setLeyenda("depo");
		assertTrue(uni.yaSePuedeEntregar());
		
	}
	
	@Test
	public void testEj6aPasarPorTaller() {

		uni.setCliente(depo);

		// tintoreria normal
		assertFalse(uni.pasoPor(tinton)); // etc.

		assertFalse(uni.tieneColorRequerido());
		
		uni.pasarPorTaller(tinton);
		assertTrue(uni.tieneColorRequerido());

		assertTrue(uni.pasoPor(tinton)); // etc.

		// tintoreria grossa
		uni.setColor("fucsia");
		assertFalse(uni.tieneColorRequerido());

		uni.pasarPorTaller(tintog);
		assertTrue(uni.tieneColorRequerido());
		
		// costurero
		uni.setRefuerzos(2);
		uni.setCosturasExtra(1);
		assertFalse(uni.tieneResistenciaRequerida());

		uni.pasarPorTaller(costu);
		assertTrue(uni.tieneResistenciaRequerida());
		
		// remendon
		uni.setRefuerzos(1);
		uni.setCosturasExtra(2);
		assertFalse(uni.tieneResistenciaRequerida());

		uni.pasarPorTaller(remen);
		assertTrue(uni.tieneResistenciaRequerida());
		
		// bordador normal
		assertFalse(uni.tieneLeyendaRequerida());

		uni.pasarPorTaller(bordan);
		assertTrue(uni.tieneLeyendaRequerida());
		
		// bordador descosedor
		uni.setLeyenda("pepero");
		assertFalse(uni.tieneLeyendaRequerida());

		uni.pasarPorTaller(bordad);
		assertTrue(uni.tieneLeyendaRequerida());
		
	}

	@Test
	public void testEj6b1PorCuantosTalleresPaso() {
		
		assertEquals(0, uni.porCuantosTalleresPaso());
		
		uni.setCliente(depo);
		uni.pasarPorTaller(tintog);
		uni.pasarPorTaller(costu);
		uni.pasarPorTaller(remen);
		uni.pasarPorTaller(bordad);
		
		assertEquals(4, uni.porCuantosTalleresPaso());
		
	}
	
	@Test
	public void testEj6b2PasoPorTaller() {
		
		assertFalse(uni.pasoPor(tinton));
		
		uni.setCliente(depo);
		uni.pasarPorTaller(tinton);
		
		assertTrue(uni.pasoPor(tinton));
		
	}
	
	@Test
	public void testEj6b3UltimoTallerPorElQuePaso() {
		
		uni.setCliente(depo);
		uni.pasarPorTaller(costu);
		assertEquals(costu, uni.ultimoTaller());
		
		uni.pasarPorTaller(remen);
		assertEquals(remen, uni.ultimoTaller());
		
	}
	
}
