package venta_masiva.modelo.item.elemento;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import venta_masiva.modelo.item.paquete.Cierre;
import venta_masiva.modelo.item.paquete.Cinta;
import venta_masiva.modelo.item.paquete.Envoltorio;
import venta_masiva.modelo.item.paquete.FilmAlveolar;
import venta_masiva.modelo.item.producto.Vino;

/**
 * representa una botella de un vino -
 * tiene un número de inventario (que debería ser único)
 */
public class Botella extends Elemento<Vino> {
	
	// constructor
	public Botella(Vino vino, int nroInventario) {
		super(vino, nroInventario);
	}
	
	// getters / setters
	public Vino getVino() {
		return this.getProducto();
	}
	
	// acciones
	@Override
	public Collection<Envoltorio> crearEnvoltorios() {
		return new HashSet<>(Arrays.asList(new FilmAlveolar()));
	}
	
	@Override
	public Collection<Cierre> crearCierres() {
		return IntStream.range(0, this.getCantCintas())
			.mapToObj(n -> new Cinta())
			.collect(Collectors.toSet());
	}
	
	/**
	 * retorna la cantidad de cintas necesaria para cerrar el envoltorio
	 * @return cantidad de cintas necesaria para cerrar el envoltorio, según el peso de la botella
	 */
	private int getCantCintas() {
		if (this.getProducto().getPeso() <= 1000) {
			return 1;
		} else {
			return 3;
		}
	}
	
}
