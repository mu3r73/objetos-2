package p07_civilization;

public class HumorPerseguido implements Humor {

	@Override
	public int getDisconformidad(Ciudad ciudad) {
		int disconf = Integer.max(3, ciudad.getCantHabitantes() / 40000);
		
		if (ciudad.getCantUMilitares() == 0) {
			disconf += 10;
		}
		if ((ciudad.getCantUMilitares() >= 1) && (ciudad.getCantUMilitares() <= 3)) {
			disconf += 5;
		}
		
		return disconf;
	}
	
	@Override
	public int getTranquilidad(EdificioCultural edificio) {
		return 1;
	}
	
	@Override
	public int getTranquilidad(EdificioEconomico edificio) {
		if (edificio.getProduccion() <= 500) {
			return 10;
		} else {
			return 15;
		}
	}
	
	@Override
	public int getTranquilidad(EdificioMilitar edificio) {
		return 3 * edificio.getPotenciaUnidadesMilitares();
	}
	
}
