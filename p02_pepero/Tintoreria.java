package p02_pepero;

/**
 * tintorería genérica
 * puede cambiar el color de un uniforme
 */
public abstract class Tintoreria extends Taller {

	// consultas
	
	/**
	 * indica si el uniforme necesita que le cambien el color
	 */
	@Override
	public boolean necesitaProcesar(Uniforme uniforme) {
		return !uniforme.tieneColorRequerido();
	}
	
	// acciones

	/**
	 * cambia el color del uniforme
	 */
	@Override
	public void procesar(Uniforme uniforme) {
		uniforme.setColor(uniforme.colorRequerido());
	}
	
}
