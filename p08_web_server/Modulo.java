package p08_web_server;

public interface Modulo {
	
	public boolean puedeAtender(Pedido pedido);
	
	public Respuesta atender(Pedido pedido);
	
}
