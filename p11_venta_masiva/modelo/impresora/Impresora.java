package venta_masiva.modelo.impresora;

/**
 * interface -
 * impresora de etiquetas
 */
public interface Impresora {
	
	/**
	 * 'imprime' una línea en blanco
	 */
	public void saltarLinea();
	
	/**
	 * imprime el texto indicado
	 * @param texto texto a imprimir
	 */
	public void ponerTexto(String texto);

}
