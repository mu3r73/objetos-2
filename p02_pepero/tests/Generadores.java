package p02_pepero.tests;

import java.util.HashSet;
import java.util.Set;

import p02_pepero.Material;
import p02_pepero.Uniforme;

public class Generadores {

	/**
	 * genera cant uniformes del material y valor indicados
	 * sólo para tests
	 */
	public static Set<Uniforme> generarUniformes(int cant, Material material, double valor) {
		Set<Uniforme> us = new HashSet<>();
		for (int i = 0; i < cant; i++) {
			us.add(new Uniforme(material, "natural", 0));
		}
		for (Uniforme uniforme : us) {
			uniforme.setValor(valor);
		}
		return us;
	}
	
}
