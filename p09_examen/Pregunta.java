package p09_examen;

public abstract class Pregunta {

	protected String pregunta;
	protected double puntaje;
	
	// getters / setters
	
	public double getPuntaje() {
		return this.puntaje;
	}
	
	public void setPuntaje(double p) {
		this.puntaje = p;
	}
	
}
