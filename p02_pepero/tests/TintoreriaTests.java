package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.Deposito;
import p02_pepero.Material;
import p02_pepero.TintoreriaGrossa;
import p02_pepero.TintoreriaNormal;
import p02_pepero.Uniforme;

public class TintoreriaTests {

	private Material lino = new Material("lino", 4, 5);

	private Uniforme uni = new Uniforme(lino, "rojo", 5);
	
	private Deposito depo = new Deposito("depo");
	
	private TintoreriaNormal tinton = new TintoreriaNormal();
	private TintoreriaGrossa tintog = new TintoreriaGrossa();

	@Test
	public void testNecesitaProcesar() {
		
		uni.setCliente(depo);
		assertTrue(tintog.necesitaProcesar(uni));
		assertTrue(tinton.necesitaProcesar(uni));
		
		uni.setColor(uni.colorRequerido());
		assertFalse(tintog.necesitaProcesar(uni));
		assertFalse(tinton.necesitaProcesar(uni));
		
	}

}
