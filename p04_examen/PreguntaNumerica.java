package p04_examen;

public class PreguntaNumerica extends Pregunta {

	private double respuestaCorrecta;
	private double margen;
	private double puntajeMarginal;
	
	// getters y setters
	
	public void setRespuestaCorrecta (double respuesta) {
		this.respuestaCorrecta = respuesta;
	}
	
	public void setMargen(double margen) {
		this.margen = margen;
	}
	
	public double getPuntajeMarginal() {
		return puntajeMarginal;
	}
	
	// consultas
	
	public boolean esCorrecta(RespuestaNumerica respuesta) {
		return this.esExacta(respuesta)
				|| this.esMarginal(respuesta);
	}

	protected boolean esExacta(RespuestaNumerica respuesta) {
		return respuesta.getNumero() == this.respuestaCorrecta;
	}
	
	protected boolean esMarginal(RespuestaNumerica respuesta) {
		return respuesta.getNumero() >= this.respuestaCorrecta - margen
				&& respuesta.getNumero() <= this.respuestaCorrecta + margen;
	}
	
}
