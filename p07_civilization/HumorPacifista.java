package p07_civilization;

public class HumorPacifista implements Humor {

	@Override
	public int getDisconformidad(Ciudad ciudad) {
		return (ciudad.getCantHabitantes() / 15000) + ciudad.getCantUMilitares();
	}
	
	@Override
	public int getTranquilidad(EdificioCultural edificio) {
		return 15;
	}
	
	@Override
	public int getTranquilidad(EdificioEconomico edificio) {
		return 12;
	}
	
	@Override
	public int getTranquilidad(EdificioMilitar edificio) {
		return -3;
	}
	
}
