package p03_minions;

/**
 * Tarea genérica que puede realizar un Minion
 */
public interface Tarea {

	// consultas
	
	/**
	 * retorna la dificultad de la tarea (depende del minion)
	 */
	public abstract double dificultad(Minion minion);
	
	/**
	 * indica si la tarea puede ser ejecutada por el minion
	 */
	public abstract boolean puedeSerEjecutadaPor(Minion minion);
	
	// acciones
	
	/**
	 * la tarea es ejecutada por el minion
	 */
	public abstract void serEjecutadaPor(Minion minion);

}
