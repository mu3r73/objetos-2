package venta_masiva.modelo.sector;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import venta_masiva.modelo.item.elemento.Botella;
import venta_masiva.modelo.item.elemento.Ejemplar;
import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;

public class SectorSimple extends Sector {

	private Collection<Ejemplar> ejemplares = new HashSet<>();
	private Collection<Botella> botellas = new HashSet<>();

	// constructor
	public SectorSimple(String nombre) {
		super(nombre);
	}

	// altas a colecciones
	public void agregarEjemplares(Ejemplar ... ejemplares) {
		for (Ejemplar ejemplar : ejemplares) {
			this.ejemplares.add(ejemplar);			
		}
	}

	public void agregarBotellas(Botella ... botellas) {
		for (Botella botella : botellas) {
			this.botellas.add(botella);	
		}
	}

	// consultas
	@Override
	public SectorCompuesto hallarSectorCompuestoLlamado(String nombre) {
		return null;
	}
	
	@Override
	public SectorSimple hallarSectorSimpleLlamado(String nombre) {
		if (this.nombre.equals(nombre)) {
			return this;
		} else {
			return null;
		}
	}

	@Override
	public boolean estaVacio() {
		return this.ejemplares.isEmpty() && this.botellas.isEmpty();
	}
	
	@Override
	public SectorSimple hallarSectorQueContieneElementoNro(int nroInventario) {
		if (this.getTodosLosElementos().stream()
				.anyMatch(elemento -> elemento.getNroInventario() == nroInventario)) {
			return this;
		} else {
			return null;
		}
	}
	
	/**
	 * retorna todos los elementos en el sector
	 * @return todos los elementos (ejemplares + botellas) en el sector
	 */
	public Set<Elemento<? extends Producto>> getTodosLosElementos() {
		Set<Elemento<? extends Producto>> res = new HashSet<>();
		res.addAll(this.ejemplares);
		res.addAll(this.botellas);
		return res;
	}

	@Override
	public Set<Libro> obtenerLibrosDe(String ... autores) {
		return this.ejemplares.stream()
				.filter(ejemplar -> ejemplar.getLibro().tieneAutores(autores))
				.map(ejemplar -> ejemplar.getLibro())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Elemento<? extends Producto>> obtenerElementosDe(Producto producto) {
		return this.getTodosLosElementos().stream()
				.filter(elemento -> elemento.getProducto().equals(producto))
				.collect(Collectors.toSet());
	}
	
	/**
	 * elimina del sector el elemento indicado
	 * (si el elemento no está en el sector, genera una excepción)
	 * @param elemento elemento a eliminar
	 */
	public void retirarElemento(Elemento<? extends Producto> elemento) {
		boolean removed = this.ejemplares.remove(elemento);
		if (!removed) {
			removed = this.botellas.remove(elemento);
		}
		if (!removed) {
			throw new RuntimeException("el elemento " + elemento.getNroInventario()
										+ " no está en el sector " + this.nombre);
		}
	}
	
	// bajas de colecciones
	/**
	 * quita todos los elementos de este sector, y los pone en el sector indicado
	 * @param ss2 sector donde poner los elementos
	 */
	public void vaciar(SectorSimple ss2) {
		this.vaciarEjemplares(ss2);
		this.vaciarBotellas(ss2);
	}
	
	/**
	 * quita todos los ejemplares de este sector, y los pone en el sector indicado
	 * @param ss2 sector donde poner los ejemplares
	 */
	private void vaciarEjemplares(SectorSimple ss2) {
		for (Ejemplar ejemplar : this.ejemplares) {
			ss2.agregarEjemplares(ejemplar);
		}
		this.ejemplares.clear();
	}
	
	/**
	 * quita todas las botellas de este sector, y las pone en el sector indicado
	 * @param ss2 sector donde poner las botellas
	 */
	private void vaciarBotellas(SectorSimple ss2) {
		for (Botella botella : this.botellas) {
			ss2.agregarBotellas(botella);
		}
		this.botellas.clear();
	}
	
	// acciones
	// para pruebas
	@Override
	public void eliminarElementos() {
		this.ejemplares.clear();
		this.botellas.clear();
	}
	
}
