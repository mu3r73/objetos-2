package p02_pepero.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p02_pepero.Fabrica;
import p02_pepero.Material;

public class FabricaTests {

	private Material lino = new Material("lino", 4, 5);
	
	private Fabrica fabri1 = new Fabrica("fabri1", 8.0);
	private Fabrica fabri2 = new Fabrica("fabri2", 6.5);
	
	@Before
	public void b4() {
		// fabri1 recibe 100 uniformes con valor = 300
		fabri1.recibirUniformes(Generadores.generarUniformes(100, lino, 300));
		
		// fabri2 recibe 101 uniformes con valor = 300
		fabri2.recibirUniformes(Generadores.generarUniformes(101, lino, 300));
	}
	
	@Test
	public void test5bCuantoHayQueFacturarle() {
		
		assertEquals(30000, fabri1.totalAFacturar(), 0);
		
		assertEquals(27876, fabri2.totalAFacturar(), 0);
		
	}

}
