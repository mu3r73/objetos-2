package p10_car_service.service.agregado.preliminar;

import p10_car_service.service.agregado.ServiceAgregado;
import p10_car_service.service.base.ServiceBase;

public abstract class ServiceAgregadoPreliminar extends ServiceAgregado {

	public ServiceAgregadoPreliminar(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public void aplicar() {
		this.aplicarAgregado();
		this.servBase.aplicar();
	}
	
}
