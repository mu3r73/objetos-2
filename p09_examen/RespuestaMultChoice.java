package p09_examen;

public class RespuestaMultChoice extends Respuesta<PreguntaMultChoice> {

	private String opcion;

	// getters / setters
	
	public void setPregunta(PreguntaMultChoice pregunta) {
		this.pregunta = pregunta;
	}
	
	public String getOpcion() {
		return this.opcion;
	}
	
	public void setOpcion(String respuesta) {
		this.opcion = respuesta;
	}
	
	// consultas
	
	@Override
	public double puntajeObtenido() {
		if (this.esCorrecta()) {
			return this.pregunta.getPuntaje();
		} else {
			return 0;
		}
	}

	@Override
	public boolean esCorrecta() {
		return this.pregunta.esCorrecta(this);
	}

}
