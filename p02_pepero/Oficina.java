package p02_pepero;

/**
 * oficina
 * no tiene requisitos para la resistencia de sus uniformes
 * requiere uniformes de un color determinado (específico de la instancia)
 */
public class Oficina extends Cliente {

	private String colorRequerido;
	
	// constructores
	
	public Oficina(String nombre, String colorRequerido) {
		super(nombre);
		this.colorRequerido = colorRequerido;
	}

	// consultas
	
	/**
	 * retorna el color requerido por la oficina
	 */
	@Override
	public String colorRequerido() {
		return this.colorRequerido;
	}

	/**
	 * indica si el uniforme es del color requerido
	 */
	@Override
	public boolean tieneColorCorrecto(Uniforme uniforme) {
		return uniforme.getColor().equals(this.colorRequerido);
	}

	/**
	 * indica que un uniforme siempre tiene la resistencia requerida
	 */
	@Override
	public boolean tieneResistenciaCorrecta(Uniforme uniforme) {
		return true;
	}

}
