package p01_obj1g05e03_remiseria;

public class AutoDistinto implements Vehiculo {

	protected final int capacidad;
	protected final int velocidadMaxima;
	protected final int peso;
	protected final String color;
	
	// constructor
	public AutoDistinto(int capacidad, int velocidadMaxima, int peso, String color) {
		super();
		this.capacidad = capacidad;
		this.velocidadMaxima = velocidadMaxima;
		this.peso = peso;
		this.color = color;
	}

	@Override
	public int capacidad() {
		return this.capacidad;
	}

	@Override
	public int velocidadMaxima() {
		return this.velocidadMaxima;
	}

	@Override
	public int peso() {
		return this.peso;
	}

	@Override
	public String color() {
		return this.color;
	}

}
