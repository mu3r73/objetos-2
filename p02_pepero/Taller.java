package p02_pepero;

/**
 * taller genérico de procesamiento de uniformes
 */
public abstract class Taller {

	// consultas
	
	/**
	 * ejercicio 2
	 * indica si el taller puede procesar el uniforme
	 */
	public abstract boolean puedeProcesar(Uniforme uniforme);
	
	/**
	 * indica si el uniforme necesita las modificaciones que puede realizar el taller
	 */
	public abstract boolean necesitaProcesar(Uniforme uniforme);

	/**
	 * indica si el taller puede y necesita procesar el uniforme
	 */
	public boolean tieneSentidoProcesar(Uniforme uniforme) {
		return this.puedeProcesar(uniforme)
				&& this.necesitaProcesar(uniforme);
	}

	// acciones

	/**
	 * realiza las modificaciones que puede sobre el uniforme
	 */
	public abstract void procesar(Uniforme uniforme);
	
}
