package venta_masiva.modelo.cliente;

import java.time.LocalDateTime;

/**
 * clase abstracta -
 * representa un movimiento - tiene un cliente y una fecha
 */
public abstract class Movimiento {

	protected Cliente cliente;
	private LocalDateTime fecha;

	// constructor
	public Movimiento(Cliente cliente, LocalDateTime fecha) {
		super();
		this.cliente = cliente;
		this.fecha = fecha;
	}
	
	// getters / setters
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public LocalDateTime getFecha() {
		return this.fecha;
	}

}
