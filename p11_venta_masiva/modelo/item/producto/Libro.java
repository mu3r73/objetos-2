package venta_masiva.modelo.item.producto;

import java.util.Collection;
import java.util.HashSet;

import venta_masiva.modelo.item.elemento.Ejemplar;

/**
 * representa un libro, con peso, costo, precio de venta, alto, ancho, espesor, título y autores
 */
public class Libro extends Producto {

	private double alto; // en cm
	private double ancho; // en cm
	private double espesor; // en cm
	private String titulo;
	private Collection<String> autores = new HashSet<>();

	// constructor
	public Libro(double peso, double costo, double precioVenta,
			double alto, double ancho, double espesor, String titulo, String ... autores) {
		super(peso, costo, precioVenta);
		this.alto = alto;
		this.ancho = ancho;
		this.espesor = espesor;
		this.titulo = titulo;
		this.setAutores(autores);
	}
	
	// getters / setters
	public void setAutores(String ... autores) {
		this.autores.clear();
		for (String autor : autores) {
			this.agregarAutor(autor);
		}
	}
	
	public String getTitulo() {
		return this.titulo;
	}
	
	// altas a colecciones
	public void agregarAutor(String autor) {
		this.autores.add(autor);
	}
	
	// consultas
	/**
	 * indica si los autores indicados estén incluidos en la lista de autores del libro
	 * @param autores autores a verificar
	 * @return true si los autores corresponden al libro; false en caso contrario
	 */
	public boolean tieneAutores(String ... autores) {
		boolean res = true;
		for (String autor : autores) {
			if (!this.tieneAutor(autor)) {
				res = false;
				break;
			}
		}
		return res;
	}
	
	/**
	 * indica si el autor está incluido en la lista de autores del libro
	 * @param autor0 autor a verificar
	 * @return true si el autor corresponde al libro; false en caso contrario
	 */
	private boolean tieneAutor(String autor0) {
		return this.autores.stream()
				.anyMatch(autor -> autor.equals(autor0));
	}
	
	/**
	 * retorna la superficie necesaria para envolver el libro
	 * @return superficie de embalaje
	 */
	public double getSuperficieEmbalaje() {
		return (this.alto + this.espesor * 4) * (this.ancho * 2 + this.espesor * 3);
	}

	@Override
	public String getDescripcion() {
		return this.titulo + ", de " + this.autores.toString();
	}
	
	// acciones
	/**
	 * retorna un nuevo ejemplar, correspondiente al libro
	 * @param nroInventario número de inventario a asignarle al ejemplar
	 * @return un nuevo ejemplar, correspondiente al libro
	 */
	public Ejemplar crearEjemplar(int nroInventario) {
		return new Ejemplar(this, nroInventario);
	}

}
