package p03_minions;

import java.util.Set;

/**
 * Rol genérico de un Minion
 */
public abstract class Rol {

	Minion minion;
	
	// constructores
	
	public Rol(Minion minion) {
		super();
		this.minion = minion;
	}

	// consultas
	
	/**
	 * indica si el Minion puede realizar la tarea
	 */
	public boolean puedeEjecutar(Tarea tarea) {
		return tarea.puedeSerEjecutadaPor(this.minion);
	}
	
	/**
	 * indica si el Minion puede arreglar la máquina
	 */
	public boolean puedeArreglar(Maquina maquina) {
		return this.minion.tieneSuficienteEstamina(maquina.getComplejidad())
				&& this.tieneHerramientasNecesarias(maquina.getHerramientasReq());
	}

	/**
	 * indica si el Minion puede defender el sector
	 */
	public boolean puedeDefender(Sector sector) {
		return this.minion.tieneSuficienteFuerza(sector.getAmenaza());
	}

	/**
	 * indica si el Minion puede limpiar el sector
	 */
	public boolean puedeLimpiar(Sector sector) {
		return this.minion.tieneSuficienteEstamina(sector.getEstaminaReq());
	}

	/**
	 * indica si el Minion tiene las herramientas necesarias
	 */
	protected boolean tieneHerramientasNecesarias(Set<Herramienta> hs) {
		return hs.isEmpty();
	}

	/**
	 * retorna la fuerza del Minion (depende del rol específico)
	 */
	public double fuerza() {
		return 2 + (this.minion.getEstamina() / 2);
	}

	// acciones
	
	/**
	 * si puede ejecutar la tarea, lo hace
	 * si no, lanza error
	 */
	public void ejecutar(Tarea tarea) {
		if (this.puedeEjecutar(tarea)) {
			tarea.serEjecutadaPor(this.minion);
			this.minion.agregarTarea(tarea);
		} else {
			throw new RuntimeException("no se puede ejecutar la tarea");
		}
	}

	/**
	 * arregla la máquina
	 */
	public void arreglar(Maquina maquina) {
		this.minion.reducirEstamina(maquina.getComplejidad());
	}

	/**
	 * defiende el sector
	 */
	public void defender(Sector sector) {
		this.minion.reducirEstamina(this.minion.getEstamina() / 2);
	}

	/**
	 * limpia el sector
	 */
	public void limpiar(Sector sector) {
		this.minion.reducirEstamina(sector.getEstaminaReq());
	}
	
}
