package venta_masiva.modelo.expedicion;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.impresora.Impresora;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.pedido.Envio;
import venta_masiva.modelo.pedido.Pedido;

/**
 * clase singleton -
 * representa el departamento de expedición del servicio -
 * tiene una colección de pedidos, una coleccion de envíos en tránsito y una colección de envíos entregados 
 */
public class Expedicion {
	
	private static Expedicion instancia;
	
	private Collection<Pedido> pedidos = new HashSet<>();
	private Collection<Envio> enviosEnTransito = new HashSet<>();
	private Collection<Envio> enviosEntregados = new HashSet<>();
	
	// números correlativos para envíos
	private static int numEnvio = 0;
	
	private Impresora impresora;
	
	// constructor
	private Expedicion() {
		super();
	}
	
	// instancia - singleton
	public static Expedicion getInstancia() {
		if (Expedicion.instancia == null) {
			Expedicion.instancia = new Expedicion();
		}
		return Expedicion.instancia;
	}
	
	// getters / setters
	public Impresora getImpresora() {
		return this.impresora;
	}
	
	public void setImpresora(Impresora impresora) {
		this.impresora = impresora;
	}
	
	// altas a colecciones
	public void agregarPedido(Pedido pedido) {
		this.pedidos.add(pedido);
	}
	
	// consultas
	/**
	 * indica si el cliente tiene algún envío en tránsito
	 * @param cliente el cliente a consultar
	 * @return true si hay algún envío en tránsito para el cliente; false en caso contrario
	 */
	public boolean tieneEnviosEnTransito(Cliente cliente) {
		return this.enviosEnTransito.stream()
				.anyMatch(envio -> envio.getCliente().equals(cliente));
	}
	
	/**
	 * retorna los pedidos sin enviar
	 * @return pedidos que no tienen envío asociado
	 */
	public Collection<Pedido> getPedidosSinEnviar() {
		return this.pedidos.stream()
				.filter(pedido -> !pedido.tieneEnvio())
				.collect(Collectors.toSet());
	}
	
	/**
	 * retorna la cantidad de elementos que falta enviar para el producto indicado
	 * @param producto el producto a consultar
	 * @return cantidad de 'apariciones' del producto en pedidos sin enviar
	 */
	public int cuantosElementosFaltaEnviar(Producto producto) {
		return this.obtenerProductosSinEnviar(producto).size();
	}
	
	/**
	 * retorna las instancias del producto en pedidos sin enviar
	 * @param producto0 el producto a consultar
	 * @return las 'apariciones' del producto en pedidos sin enviar
	 */
	private Collection<Producto> obtenerProductosSinEnviar(Producto producto0) {
		return this.getPedidosSinEnviar().stream()
				.flatMap(pedido -> pedido.getProductos().stream())
				.filter(producto -> producto.equals(producto0))
				.collect(Collectors.toList());
	}
	
	/**
	 * retorna el ranking de clientes, ordenados por importe total de sus pedidos entre las fechas indicadas
	 * @param desde fecha inicial (inclusiva) del período
	 * @param hasta fecha final (inclusiva) del período
	 * @return ranking de clientes, ordenados por importe total de sus pedidos entre esas fechas
	 */
	public List<ClienteConImporteTotal> getRankingClientesOrdenImporteTotal(LocalDateTime desde, LocalDateTime hasta) {
		return getClientesConImporteTotal(desde, hasta).stream()
				.sorted(Comparator.comparing(ClienteConImporteTotal::getImporteTotal).reversed())
				.collect(Collectors.toList());
	}
	
	/**
	 * retorna la lista de clientes + el importe total de los pedidos de cada uno entre las fechas indicadas
	 * @param desde fecha inicial (inclusiva) del período
	 * @param hasta fecha final (inclusiva) del período
	 * @return lista de clientes + el importe total de los pedidos de cada uno de ellos entre esas fechas
	 */
	private List<ClienteConImporteTotal> getClientesConImporteTotal(LocalDateTime desde, LocalDateTime hasta) {
		List<ClienteConImporteTotal> res = new ArrayList<>();
		for (Cliente cliente : Store.getInstancia().getClientes()) {
			double importeTot = this.getImporteTotalDe(cliente, desde, hasta);
			if (importeTot > 0) {
				res.add(new ClienteConImporteTotal(cliente, importeTot));
			}
		}
		return res;
	}
	
	/**
	 * retorna el importe total de los pedidos del cliente entre las fechas indicadas
	 * @param cliente el cliente a consultar
	 * @param desde fecha inicial (inclusiva) del período
	 * @param hasta fecha final (inclusiva) del período
	 * @return el importe total de los pedidos del cliente entre esas fechas
	 */
	private double getImporteTotalDe(Cliente cliente, LocalDateTime desde, LocalDateTime hasta) {
		return this.getPedidosEnPeriodoDe(cliente, desde, hasta)
				.mapToDouble(pedido -> pedido.getPrecioVentaTotal())
				.sum();
	}
	
	/**
	 * retorna un stream con los pedidos realizados por el cliente entre las fechas indicadas
	 * @param cliente el cliente a consultar
	 * @param desde fecha inicial (inclusiva) del período
	 * @param hasta fecha final (inclusiva) del período
	 * @return stream con los pedidos realizados por el cliente entre esas fechas
	 */
	private Stream<Pedido> getPedidosEnPeriodoDe(Cliente cliente, LocalDateTime desde, LocalDateTime hasta) {
		return this.getPedidosDe(cliente)
				.filter(pedido -> ((pedido.getFecha().isAfter(desde) || pedido.getFecha().isEqual(desde))
									&& (pedido.getFecha().isBefore(hasta) || pedido.getFecha().isEqual(hasta))));
	}
	
	/**
	 * retorna un stream con los pedidos realizados por el cliente
	 * @param cliente el cliente a consultar
	 * @return stream con los pedidos realizados por ese cliente
	 */
	private Stream<Pedido> getPedidosDe(Cliente cliente) {
		return this.pedidos.stream()
				.filter(pedido -> pedido.getCliente().equals(cliente));
	}
	
	// acciones
	/**
	 * procesa un pedido (crea un nuevo envío, y lo agrega a envíos en tránsito) 
	 * @param pedido el pedido a procesar
	 */
	public void procesar(Pedido pedido) {
		this.agregarPedido(pedido);
		Envio envio = pedido.crearEnvio();
		this.enviosEnTransito.add(envio);
	}
	
	/**
	 * entrega un pedido (lo saca de 'en tránsito' y lo agrega a 'entregados')
	 * @param envio el envío a entregar
	 */
	public void entregar(Envio envio) {
		this.enviosEnTransito.remove(envio);
		this.enviosEntregados.add(envio);
	}
	
	/**
	 * retorna un número único de envío
	 * @return número único de envío
	 */
	public static int getNumEnvio() {
		return Expedicion.numEnvio++;
	}
	
	// para pruebas
	/**
	 * elimina *todos* los pedidos y envíos
	 */
	public void eliminarPedidosYEnvios() {
		this.pedidos.clear();
		this.enviosEnTransito.clear();
		this.enviosEntregados.clear();
	}
	
}
