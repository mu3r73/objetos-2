package p08_web_server;

import java.util.Collection;
import java.util.HashSet;

public class FalsoModulo implements Modulo {
	
	private Collection<String> extAceptables = new HashSet<>();
	private int demora;
	private String payload;
	
	// constructores
	
	public FalsoModulo(Collection<String> extAceptables, int demora, String payload) {
		super();
		this.extAceptables = extAceptables;
		this.demora = demora;
		this.payload = payload;
	}
	
	// altas a colecciones
	
	public void agregarExtAceptable(String ext) {
		this.extAceptables.add(ext);
	}
	
	// consultas
	
	@Override
	public boolean puedeAtender(Pedido pedido) {
		return this.extAceptables.contains(pedido.getURL().getExtension());
	}
	
	// acciones
	
	@Override
	public Respuesta atender(Pedido pedido) {
//		Respuesta resp = new Respuesta(Math.toIntExact(
//				pedido.getDiaYHora().until(LocalDateTime.now(), ChronoUnit.MILLIS)));
		Respuesta resp = new Respuesta(this.demora);
		resp.setPayload(this.payload);
		return resp;
	}
	
}
