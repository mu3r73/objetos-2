package p10_car_service;

public class Cliente {

	private boolean esVIP;

	public Cliente(boolean esVIP) {
		super();
		this.esVIP = esVIP;
	}
	
	public boolean esVIP() {
		return this.esVIP;
	}
	
}
