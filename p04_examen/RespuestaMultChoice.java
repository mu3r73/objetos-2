package p04_examen;

public class RespuestaMultChoice extends Respuesta {

	private PreguntaMultChoice pregunta;
	private String opcion;

	// getters / setters
	
	public void setPregunta(PreguntaMultChoice pregunta) {
		this.pregunta = pregunta;
	}
	
	public String getOpcion() {
		return this.opcion;
	}
	
	public void setOpcion(String respuesta) {
		this.opcion = respuesta;
	}
	
	// consultas
	
	@Override
	public double puntajeObtenido() {
		if (this.estaAprobada()) {
			return this.pregunta.getPuntaje();
		} else {
			return 0;
		}
	}

	@Override
	public boolean estaAprobada() {
		return this.pregunta.esCorrecta(this);
	}

}
