package venta_masiva.modelo.item.paquete;

/**
 * representa una etiqueta para cerrar un envoltorio, tiene un peso
 */
public class Etiqueta extends Cierre {
	
	// constructor
	public Etiqueta() {
		super(20);
	}
	
}
