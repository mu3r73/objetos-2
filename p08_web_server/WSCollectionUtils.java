package p08_web_server;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class WSCollectionUtils {
	
	public static <R> Collection<R> filtrar(Collection<R> rs, Predicate<R> p) {
		return rs.stream()
				.filter(p)
				.collect(Collectors.toSet());
	}
	
}
