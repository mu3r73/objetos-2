package venta_masiva.modelo.impresora;

import java.util.ArrayList;
import java.util.List;

/**
 * clase singleton -
 * impresora para pruebas -
 * no imprime, solamente loggea resultados 
 */
public class ImpresoraLogger implements Impresora {
	
	private static ImpresoraLogger instancia;
	private List<String> log = new ArrayList<>();
	
	// constructor
	private ImpresoraLogger() {
		super();
	}
	
	// instancia - singleton
	public static ImpresoraLogger getInstancia() {
		if (ImpresoraLogger.instancia == null) {
			ImpresoraLogger.instancia = new ImpresoraLogger();
		}
		return ImpresoraLogger.instancia;
	}
	
	// getters / setters
	/**
	 * retorna todas las líneas loggeadas
	 * @return líneas loggeadas
	 */
	public List<String> getLog() {
		return this.log;
	}
	
	// acciones
	/**
	 * elimina todas las líneas loggeadas
	 */
	public void borrarLog() {
		this.log.clear();
	}
	
	@Override
	public void saltarLinea() {
		this.log.add("");
	}
	
	@Override
	public void ponerTexto(String texto) {
		this.log.add(texto);
	}

}
