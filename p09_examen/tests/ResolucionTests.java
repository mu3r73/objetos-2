package p09_examen.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import p04_examen.Enunciado;
import p04_examen.PreguntaMultChoice;
import p04_examen.PreguntaNumerica;
import p04_examen.PreguntaSecuencia;
import p04_examen.Resolucion;
import p04_examen.RespuestaMultChoice;
import p04_examen.RespuestaNumerica;
import p04_examen.RespuestaSecuencia;

public class ResolucionTests {

	@Test
	public void test() {
		
		Enunciado e = new Enunciado();
		
		PreguntaMultChoice p1 = new PreguntaMultChoice();
		p1.setOpcionCorrecta("a");
		p1.setPuntaje(5);
		
		PreguntaNumerica p2 = new PreguntaNumerica();
		p2.setRespuestaCorrecta(15);
		p2.setMargen(3);
		p2.setPuntaje(10);
		
		PreguntaSecuencia p3 = new PreguntaSecuencia();
		p3.setOrdenCorrecto("abcd");
		p3.setPuntaje(20);
		
		e.agregarPregunta(p1);
		e.agregarPregunta(p2);
		e.agregarPregunta(p3);
		
		Resolucion r = new Resolucion();
		
		RespuestaMultChoice r1 = new RespuestaMultChoice();
		r1.setPregunta(p1);
		r1.setOpcion("a");
		
		RespuestaNumerica r2 = new RespuestaNumerica();
		r2.setPregunta(p2);
		r2.setNumero(5);
		
		RespuestaSecuencia r3 = new RespuestaSecuencia();
		r3.setPregunta(p3);
		r3.setSecuencia("dcba");
		
		r.agregarRespuesta(r1);
		r.agregarRespuesta(r2);
		r.agregarRespuesta(r3);
		
		assertEquals(25, r.puntajeObt(), 0);
		
	}

}
