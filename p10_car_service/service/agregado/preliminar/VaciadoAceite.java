package p10_car_service.service.agregado.preliminar;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class VaciadoAceite extends ServiceAgregadoPreliminar {

	public VaciadoAceite(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.VACIADO_ACEITE;
	}

	@Override
	protected int getTiempoAgregado() {
		return 15;
	}

	@Override
	protected double getPrecioAgregado() {
		if (this.incluyeServicio(Tipo.CARGA_ACEITE)) {
			return 0;
		} else {
			return this.getPrecioLista();
		}
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().vaciar(Nivel.CANT_ACEITE);
	}

}
