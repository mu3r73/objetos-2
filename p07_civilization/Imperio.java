package p07_civilization;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Imperio {
	
	private String nombre;
	private Collection<Ciudad> ciudades = new HashSet<>();
	private Collection<UnidadMilitar> uMilitares = new HashSet<>();
	private int tesoro;
	private Collection<Tecnologia> tecnologias = new HashSet<>();
	private int factorTranquilidad;
	private Humor humor = new HumorPacifista();
	
	// constructores
	
	public Imperio(String nombre, int tesoro, int factorTranquilidad) {
		super();
		this.nombre = nombre;
		this.tesoro = tesoro;
		this.factorTranquilidad = factorTranquilidad;
	}
	
	// altas/bajas de/a colecciones
	
	public void agregarCiudad(Ciudad ciudad) {
		this.ciudades.add(ciudad);
	}
	
	public void agregarUMilitar(UnidadMilitar uMilitar) {
		this.uMilitares.add(uMilitar);
	}
	
	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public int getTesoro() {
		return this.tesoro;
	}

	public int getFactorTranquilidad() {
		return this.factorTranquilidad;
	}
	
	public Humor getHumor() {
		return this.humor;
	}
	
	public void setHumor(Humor humor) {
		this.humor = humor;
	}
	
	// modificaciones
	
	public void incTesoro(int suma) {
		this.tesoro += suma;
	}
	
	public void decTesoro(int suma) {
		this.tesoro -= suma;
	}
	
	// consultas
	
	// req 1
	public List<Ciudad> ciudadesPorCulturaOrdDesc() {
		return this.ciudades.stream()
				.sorted(Comparator.comparing(ciudad -> ciudad.getCultura()))
				.sorted(Collections.reverseOrder())
				.collect(Collectors.toList());
	}
	
	// req 4
	public boolean puedeIncorporarTecnologia(Tecnologia tecnologia) {
		return !this.tecnologias.contains(tecnologia)
				&& this.tecnologias.containsAll(tecnologia.getTecnologiasRequeridas());
	}
	
	// req 5.a
	public int getIngresosPorTurno() {
		return this.ciudades.stream()
				.mapToInt(ciudad -> ciudad.getIngresosPorTurno())
				.sum();
	}
	
	// req 5.b
	public int getEgresosPorTurno() {
		return this.ciudades.stream()
				.mapToInt(ciudad -> ciudad.getEgresosPorTurno())
				.sum();
	}
	
	// req 5.c
	public int getPotenciaGeneradaPorTurno() {
		return this.ciudades.stream()
				.mapToInt(ciudad -> ciudad.getPotenciaGeneradaPorTurno())
				.sum();
	}
	
	public Collection<Tecnologia> getTecnologiasFaltantesPara(Tecnologia tecnologia) {
		return tecnologia.getTecnologiasFaltantes(this.tecnologias);
	}
	
	public int getCulturaAportadaPorTecnologias() {
		return this.tecnologias.stream()
				.mapToInt(tecn -> tecn.getCultura())
				.sum();
	}
	
	// acciones
	
	public void incorporarTecnologia(Tecnologia tecnologia) {
		if (this.puedeIncorporarTecnologia(tecnologia)) {
			this.tecnologias.add(tecnologia);
		}
	}
	
	// req 6
	public void jugarTurno() {
		this.ciudades.forEach(
			ciudad -> ciudad.jugarTurno()
		);
	}
	
}
