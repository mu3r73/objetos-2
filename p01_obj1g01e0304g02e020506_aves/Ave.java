package p01_obj1g01e0304g02e020506_aves;

public interface Ave {

	public abstract void comer(Comida c);
	
	public abstract void volar(int km);
	
	public abstract void haceLoQueQuieras();
	
}
