package p07_civilization;

public class EdificioEconomico extends Edificio {
	
	private int produccion;
	
	// constructores
	
	public EdificioEconomico(Ciudad ciudad, int costoMantenimiento, int costoConstruccion, int produccion) {
		super(ciudad, costoMantenimiento, costoConstruccion);
		this.produccion = produccion;
	}
	
	// getters / setters
	
	public int getProduccion() {
		return this.produccion;
	}
	
	// consultas
	
	@Override
	public int getCultura() {
		if (this.produccion <= 500) {
			return 2;
		} else {
			return 3;
		}
	}

	@Override
	public int getTranquilidad() {
		// double dispatch
		return this.getCiudad().getImperio().getHumor().getTranquilidad(this);
	}
	
	@Override
	public int getValor() {
		return this.getProduccion();
	}
	
	@Override
	public boolean esDestacado() {
		return this.produccion > 200;
	}
	
	// acciones
	
	@Override
	public void boostear() {
		this.produccion *= 2;
	}

}
