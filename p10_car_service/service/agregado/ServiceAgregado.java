package p10_car_service.service.agregado;

import p10_car_service.Cliente;
import p10_car_service.service.Service;
import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Vehiculo;

public abstract class ServiceAgregado extends Service {
	
	protected ServiceBase servBase;

	public ServiceAgregado(ServiceBase servBase) {
		super();
		this.servBase = servBase;
	}

	@Override
	public Vehiculo getVehiculo() {
		return this.servBase.getVehiculo();
	}


	@Override
	public Cliente getCliente() {
		return this.servBase.getCliente();
	}
	
	@Override
	public boolean seAbreCapot() {
		return this.servBase.seAbreCapot();
	}

	@Override
	public boolean seTrabajaSobreChapa() {
		return this.servBase.seTrabajaSobreChapa();
	}

	@Override
	public boolean incluyeServicio(Tipo tipo) {
		return this.getTipo().equals(tipo) || this.servBase.getTipo().equals(tipo);
	}

	@Override
	public int getTiempo() {
		return this.servBase.getTiempo() + this.getTiempoAgregado();
	}

	protected abstract int getTiempoAgregado();

	@Override
	public double getPrecio() {
		return this.servBase.getPrecio() + this.getPrecioAgregado();
	}

	protected abstract double getPrecioAgregado();
	
	public abstract void aplicarAgregado();

}
