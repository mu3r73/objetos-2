package p02_pepero;

import java.util.HashSet;
import java.util.Set;

/**
 * cliente genérico
 * tiene un nombre y una colección de uniformes
 */
public abstract class Cliente {

	private String nombre;
	protected Set<Uniforme> uniformes = new HashSet<>();

	// constructores
	
	public Cliente(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	// altas a colecciones
	
	/**
	 * agrega a su colección de uniformes todos los que recibe en us
	 */
	public void recibirUniformes(Set<Uniforme> us) {
		this.uniformes.addAll(us);
	}

	// consultas
	
	/**
	 * retorna el color requerido por el cliente
	 */
	public abstract String colorRequerido();
	
	/**
	 * indica si el uniforme es del color requerido
	 */
	public abstract boolean tieneColorCorrecto(Uniforme uniforme);
	
	/**
	 * retorna la leyenda requerida por el cliente
	 */
	public String leyendaRequerida() {
		return this.nombre;
	}
	
	/**
	 * indica si el uniforme tiene la leyenda requerida
	 */
	public boolean tieneLeyendaCorrecta(Uniforme uniforme) {
		return this.nombre.equals(uniforme.getLeyenda());
	}
	
	/**
	 * indica si el uniforme tiene la resistencia requerida
	 */
	public abstract boolean tieneResistenciaCorrecta(Uniforme uniforme);

	/**
	 * ejercicio 5.b
	 * retorna cuánto hay que facturarle al cliente
	 */
	public double totalAFacturar() {
		return this.uniformes.stream()
				.mapToDouble(uniforme -> uniforme.getValor())
				.sum();
	}

}
