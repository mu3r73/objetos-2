package p04_examen;

public class RespuestaSecuencia extends Respuesta {

	private PreguntaSecuencia pregunta;
	private String secuencia;

	// getters / setters
	
	public void setPregunta(PreguntaSecuencia pregunta) {
		this.pregunta = pregunta;
	}
	
	public String getSecuencia() {
		return this.secuencia;
	}
	
	public void setSecuencia(String respuesta) {
		this.secuencia = respuesta;
	}
	
	// consultas
	
	@Override
	public double puntajeObtenido() {
		if (this.estaAprobada()) {
			return this.pregunta.getPuntaje();
		} else {
			return 0;
		}
	}

	@Override
	public boolean estaAprobada() {
		return this.pregunta.esCorrecta(this);
	}

}
