package p01_obj1g05e03_remiseria;

public class AutoStandardAGas implements Vehiculo {

	protected final boolean tieneTanqueAdicional;
	protected final int capacidadSinTanqueAdicional = 4;
	protected final int capacidadConTanqueAdicional = 3;
	protected final int velocidadMaximaConTanqueAdicional = 120; 
	protected final int velocidadMaximaSinTanqueAdicional = 110;
	protected final int pesoSinTanqueAdicional = 1200;
	protected final int pesoTanqueAdicional = 150;
	protected final String color = "azul";
	
	// constructor
	public AutoStandardAGas(boolean tieneTanqueAdicional) {
		super();
		this.tieneTanqueAdicional = tieneTanqueAdicional;
	}
	
	@Override
	public int capacidad() {
		if (this.tieneTanqueAdicional) {
			return this.capacidadConTanqueAdicional;
		} else {
			return this.capacidadSinTanqueAdicional;
		}
	}
	
	@Override
	public int velocidadMaxima() {
		if (this.tieneTanqueAdicional) {
			return this.velocidadMaximaConTanqueAdicional;
		} else {
			return this.velocidadMaximaSinTanqueAdicional;
		}
	}
	
	@Override
	public int peso() {
		if (this.tieneTanqueAdicional) {
			return this.pesoSinTanqueAdicional + this.pesoTanqueAdicional;
		} else {
			return this.pesoSinTanqueAdicional;
		}
	}

	@Override
	public String color() {
		return this.color;
	}

}
