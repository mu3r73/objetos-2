package venta_masiva.modelo.sector;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;

public class SectorCompuesto extends Sector {

	private Collection<Sector> sectores = new HashSet<>();

	// constructor
	public SectorCompuesto(String nombre) {
		super(nombre);
	}
	
	// altas a colecciones
	public void agregarSubSectores(Sector ... sectores) {
		for (Sector sector : sectores) {
			this.sectores.add(sector);
		}
	}
	
	// consultas
	@Override
	public SectorCompuesto hallarSectorCompuestoLlamado(String nombre) {
		if (this.nombre.equals(nombre)) {
			return this;
		} else {
			return this.sectores.stream()
					.map(sector -> sector.hallarSectorCompuestoLlamado(nombre))
					.filter(sector -> sector != null)
					.findAny()
					.orElse(null);
		}
	}

	@Override
	public SectorSimple hallarSectorSimpleLlamado(String nombre) {
		return this.sectores.stream()
				.map(sector -> sector.hallarSectorSimpleLlamado(nombre))
				.filter(sector -> sector != null)
				.findAny()
				.orElse(null);
	}
	
	@Override
	public boolean estaVacio() {
		return this.sectores.isEmpty();
	}

	@Override
	public SectorSimple hallarSectorQueContieneElementoNro(int nroInventario) {
		return this.sectores.stream()
				.map(sector -> sector.hallarSectorQueContieneElementoNro(nroInventario))
//				.peek(sector -> System.out.println((sector == null) ? "null" : sector.getNombre()))
				.filter(sector -> sector != null)
				.findAny()
				.orElse(null);
		
		// versión de Ramiro
//		SectorSimple res = null;
//		for (Iterator<Sector> iterador = this.sectores.iterator(); iterador.hasNext() && res == null;) {
//			res = iterador.next().hallarSectorQueContieneElementoNro(nroInventario);
//		}
//		return res;
	}

	@Override
	public Set<Libro> obtenerLibrosDe(String ... autores) {
		return this.sectores.stream()
				.flatMap(sector -> sector.obtenerLibrosDe(autores).stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Elemento<? extends Producto>> obtenerElementosDe(Producto producto) {
		return this.sectores.stream()
				.flatMap(sector -> sector.obtenerElementosDe(producto).stream())
				.collect(Collectors.toSet());
	}
	
	// acciones
	// para pruebas
	@Override
	public void eliminarElementos() {
		for (Sector sector : this.sectores) {
			sector.eliminarElementos();
		}
	}
	
}
