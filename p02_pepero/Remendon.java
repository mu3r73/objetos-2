package p02_pepero;

/**
 * remendón
 * puede agregar un refuerzo a un uniforme
 */
public class Remendon extends Taller {

	// consultas
	
	/**
	 * indica si el remendón puede agregar un refuerzo al uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return uniforme.soportaUnRefuerzoMas();
	}

	/**
	 * indica si el uniforme necesita un refuerzo
	 */
	@Override
	public boolean necesitaProcesar(Uniforme uniforme) {
		return !uniforme.tieneResistenciaRequerida();
	}

	// acciones
	
	/**
	 * agrega un refuerzo al uniforme
	 */
	@Override
	public void procesar(Uniforme uniforme) {
		uniforme.agregarRefuerzo();
	}

}
