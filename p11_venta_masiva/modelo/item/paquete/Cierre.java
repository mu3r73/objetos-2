package venta_masiva.modelo.item.paquete;

import venta_masiva.modelo.item.ItemConPeso;

/**
 * clase abstracta -
 * representa un item para cerrar un envoltorio, tiene un peso
 */
public abstract class Cierre extends ItemConPeso {
	
	// constructor
	public Cierre(double peso) {
		super(peso);
	}
	
}
