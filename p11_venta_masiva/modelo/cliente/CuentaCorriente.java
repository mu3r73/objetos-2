package venta_masiva.modelo.cliente;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import venta_masiva.modelo.pedido.Pedido;

/**
 * representa la cuenta corriente de un cliente -
 * tiene una colección de pedidos y una colección de pagos
 */
public class CuentaCorriente {

	private Collection<Pedido> pedidos;
	private Collection<Pago> pagos;
	
	// constructor
	public CuentaCorriente() {
		super();
		this.pedidos = new HashSet<>();
		this.pagos = new HashSet<>();
	}
	
	// altas a colecciones
	public void agregarPedido(Pedido pedido) {
		this.pedidos.add(pedido);
	}
	
	public void agregarPago(Pago pago) {
		this.pagos.add(pago);
	}
	
	// consultas
	/**
	 * retorna el saldo de la cuenta corriente
	 * @return total de los pagos efectuados, menos total de los precios de venta de los pedidos
	 */
	public double getSaldo() {
		return this.totalPagos() - this.totalPedidos();
	}
	
	/**
	 * retorna el total de los pedidos
	 * @return total de los precios de venta de los pedidos
	 */
	private double totalPedidos() {
		return this.pedidos.stream()
				.mapToDouble(pedido -> pedido.getPrecioVentaTotal())
				.sum();
	}
	
	/**
	 * retorna el total de los pagos
	 * @return total de los pagos
	 */
	private double totalPagos() {
		return this.pagos.stream()
				.mapToDouble(pago -> pago.getMonto())
				.sum();
	}
	
	/**
	 * retorna los movimientos realizados, ordenados por fecha
	 * @return movimientos de la cuenta, ordenados cronológicamente
	 */
	public List<Movimiento> getMovimientosOrdenFecha() {
		List<Movimiento> ms = new ArrayList<>();
		ms.addAll(this.pedidos);
		ms.addAll(this.pagos);
		
		return ms.stream()
			.sorted(Comparator.comparing(mov -> mov.getFecha()))
			.collect(Collectors.toList());
	}
	
	// acciones
	// para pruebas
	/**
	 * elimina *todos* los pedidos y los pagos de la cuenta corriente
	 */
	public void reset() {
		this.pedidos.clear();
		this.pagos.clear();
	}

}
