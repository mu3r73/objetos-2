package p03_minions;

/**
 * DefenderSector: Tarea específica que puede realizar un Minion
 */
public class DefenderSector implements Tarea {

	protected Sector sector;
	
	// constructores
	
	public DefenderSector(Sector sector) {
		super();
		this.sector = sector;
	}

	// consultas
	
	/**
	 * retorna la dificultad de defender el sector para el minion
	 */
	@Override
	public double dificultad(Minion minion) {
		return minion.dificultadDeDefender(this.sector);
	}

	/**
	 * indica si el sector puede ser defendido por el minion
	 */
	@Override
	public boolean puedeSerEjecutadaPor(Minion minion) {
		return minion.puedeDefender(this.sector);
	}

	// acciones
	
	/**
	 * el sector es defendido por el minion 
	 */
	@Override
	public void serEjecutadaPor(Minion minion) {
		minion.defender(this.sector);
	}

}
