package p03_minions;

/**
 * ArreglarMaquina: Tarea específica que puede realizar un Minion
 */
public class ArreglarMaquina implements Tarea {

	protected Maquina maquina;
	
	// constructores
	
	public ArreglarMaquina(Maquina maquina) {
		super();
		this.maquina = maquina;
	}

	// consultas
	
	/**
	 * retorna la dificultad de arreglar la máquina para el minion
	 */
	@Override
	public double dificultad(Minion minion) {
		return 2 * maquina.getComplejidad();
	}

	/**
	 * indica si la máquina puede ser arreglada por el minion 
	 */
	@Override
	public boolean puedeSerEjecutadaPor(Minion minion) {
		return minion.puedeArreglar(this.maquina);
	}

	// acciones
	
	/**
	 * la máquina es arreglada por el minion
	 */
	@Override
	public void serEjecutadaPor(Minion minion) {
		minion.arreglar(this.maquina);
	}

}
