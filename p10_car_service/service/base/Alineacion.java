package p10_car_service.service.base;

import p10_car_service.Cliente;
import p10_car_service.service.Tipo;
import p10_car_service.vehiculo.Nivel;
import p10_car_service.vehiculo.Vehiculo;

public class Alineacion extends ServiceBase {

	public Alineacion(Vehiculo vehiculo, Cliente cliente) {
		super(vehiculo, cliente);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.ALINEACION;
	}

	@Override
	public int getTiempo() {
		return 120;
	}

	@Override
	public double getPrecio() {
		return this.getPrecioLista();
	}

	@Override
	public void aplicar() {
		this.vehiculo.vaciar(Nivel.DESVIO_ALINEACION);
	}

}
