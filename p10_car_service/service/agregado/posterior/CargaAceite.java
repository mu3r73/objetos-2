package p10_car_service.service.agregado.posterior;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class CargaAceite extends ServiceAgregadoPosterior {

	public CargaAceite(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.CARGA_ACEITE;
	}
	
	@Override
	public boolean seAbreCapot() {
		return true;
	}

	@Override
	protected int getTiempoAgregado() {
		return 20;
	}

	@Override
	protected double getPrecioAgregado() {
		return this.getPrecioLista();
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().cargar(Nivel.CANT_ACEITE, 1);
	}

}
