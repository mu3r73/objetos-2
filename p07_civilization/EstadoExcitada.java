package p07_civilization;

import java.util.Optional;

public class EstadoExcitada extends EstadoNormal {
	
	@Override
	public boolean esFeliz(Ciudad ciudad) {
		return ciudad.getCultura() > 80;
	}
	
	@Override
	public void aumentarPoblacion(Ciudad ciudad) {
		if (ciudad.esFeliz()) {
			ciudad.incPoblacionPorc(5 + 8);
		} else {
			ciudad.incPoblacionPorc(5);
		}
	}
	
	@Override
	public void descontarTesoroImperioMantenimEdif(Ciudad ciudad) {
		ciudad.getImperio().decTesoro(ciudad.getEgresosPorTurnoEdifEconomicosYMilitares());
	}
	
	@Override
	public void aumentarTesoroImperioProduccEdifEconomicos(Ciudad ciudad) {
		ciudad.getImperio().incTesoro(ciudad.getIngresosPorTurno()
										+ ciudad.getAporteEdificioCulturalMaxIrradiacion());
	}
	
	@Override
	public void boostearEdifCultural(Ciudad ciudad) {
		for (EdificioCultural edif : ciudad.getEdifCulturalConMinIrradiacion(2)) {
			edif.boostear();
		}
	}
	
	@Override
	public void boostearEdifEconomico(Ciudad ciudad) {
		for (EdificioEconomico edif : ciudad.getEdifEconomicoConMinProduccion(2)) {
			edif.boostear();
		}		
	}
	
	@Override
	public void boostearEdifMilitar(Ciudad ciudad) {
		Optional<EdificioMilitar> edif = ciudad.getEdifMilitarAleatorio();
		if (edif.isPresent()) {
			edif.get().boostear();
		}
	}
	
	@Override
	public Estado getEstadoMejor() {
		return this;
	}
	
	@Override
	public Estado getEstadoPeor() {
		return new EstadoNormal();
	}
	
}
