package p02_pepero.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p02_pepero.Material;
import p02_pepero.Oficina;

public class OficinaTests {

	private Material poliester = new Material("poliéster", 5, 4); 
	
	private Oficina ofi1 = new Oficina("ofi1", "a rayas rojo y verde fluo");
	private Oficina ofi2 = new Oficina("ofi2", "verde línea este");
	
	@Before
	public void b4() {
		// ofi1 recibe 100 uniformes con valor = 350
		ofi1.recibirUniformes(Generadores.generarUniformes(100, poliester, 350));
		
		// ofi2 recibe 101 uniformes con valor = 350
		ofi2.recibirUniformes(Generadores.generarUniformes(101, poliester, 350));
	}
	
	
	@Test
	public void test5bCuantoHayQueFacturarle() {
		
		assertEquals(35000, ofi1.totalAFacturar(), 0);
		
		assertEquals(35350, ofi2.totalAFacturar(), 0);
	}
	
}
