package p10_car_service.vehiculo;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Vehiculo {

	private double superficie;
	private Color color;
	private Map<Nivel, Double> niveles = new HashMap<>();
	private Map<Nivel, Double> nivelesMax = new HashMap<>();
	private Collection<Bujia> bujias = new HashSet<>();

	public Vehiculo(double superficie, Color color) {
		super();
		this.superficie = superficie;
		this.color = color;
	}
	
	public void agregarBujia(Bujia bujia) {
		this.bujias.add(bujia);
	}
	
	public void agregarBujias(Bujia ... bujias) {
		for (Bujia bujia : bujias) {
			this.bujias.add(bujia);
		}
	}
	
	public void sacarBujia(Bujia bujia) {
		this.bujias.remove(bujia);
	}
	
	public Bujia getBujiaMenosPotente() {
		return this.bujias.stream()
				.min(Comparator.comparing(bujia -> bujia.getPotencia()))
				.get();
	}
	
	public double getSuperficie() {
		return this.superficie;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public Double getValor(Nivel nivel) {
		return this.niveles.getOrDefault(nivel, 0.0);
	}
	
	public double getValorMax(Nivel nivel) {
		return this.nivelesMax.getOrDefault(nivel, 0.0);
	}
	
	public void setValorMax(Nivel nivel, double valor) {
		this.nivelesMax.put(nivel, valor);
	}
	
	public void vaciar(Nivel nivel) {
		this.niveles.put(nivel, 0.0);
	}
	
	public void cargar(Nivel nivel, double valor) {
		this.niveles.put(nivel, Math.min(this.getValor(nivel) + valor, this.getValorMax(nivel)));
	}

	public void pintar(Color color) {
		this.color = color;
	}
	
}
