package p03_minions;

/**
 * bíclope
 * Minion con 2 ojos
 */
public class Biclope extends Minion {

	// consultas
	
	/**
	 * retorna la dificultad de defender el sector
	 */
	@Override
	public double dificultadDeDefender(Sector sector) {
		return sector.getAmenaza();
	}

	// acciones
	
	/**
	 * incrementa estámina al consumir una fruta 
	 */
	@Override
	public void comer(Fruta fruta) {
		super.comer(fruta);
		this.estamina = Math.min(estamina, 10);
	}

}
