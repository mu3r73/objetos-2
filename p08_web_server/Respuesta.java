package p08_web_server;

public class Respuesta {
	
	private int demora;
	private StatusCode statusCode;
	private String payload;
	private Pedido pedido;
	
	// constructores
	
	public Respuesta(int demora) {
		super();
		this.demora = demora;
	}

	public Respuesta(int demora, StatusCode statusCode, String payload) {
		super();
		this.demora = demora;
		this.statusCode = statusCode;
		this.payload = payload;
	}
	
	// getters / setters
	
	public int getDemora() {
		return this.demora;
	}
	
	public StatusCode getStatusCode() {
		return this.statusCode;
	}
	
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getPayload() {
		return this.payload;
	}
	
	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public Pedido getPedido() {
		return this.pedido;
	}
	
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
}
