package p01_obj1g01e0102_contador.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import p01_obj1g01e0102_contador.Contador;

public class ContadorTests {

	@Test
	public void test() {
		Contador c = new Contador();
		c.reset();
		c.inc();
		c.inc();
		c.dec();
		c.inc();
		assertEquals(2, c.valorActual());
		assertEquals("incremento", c.ultimoComando());
	}

}
