package p02_pepero;

/**
 * bordador genérico
 * puede cambiar la leyenda de un uniforme 
 */
public abstract class Bordador extends Taller {

	// consultas
	
	/**
	 * indica si el uniforme necesita que le cambien la leyenda
	 */
	@Override
	public boolean necesitaProcesar(Uniforme uniforme) {
		return !uniforme.tieneLeyendaRequerida();
	}

	// acciones
	
	/**
	 * cambia la leyenda de un uniforme
	 */
	@Override
	public void procesar(Uniforme uniforme) {
		uniforme.setLeyenda(uniforme.leyendaRequerida());
	}

}
