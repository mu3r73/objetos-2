package p02_pepero;

import java.util.ArrayList;
import java.util.List;

/**
 * uniforme
 */
public class Uniforme {

	private Material material;
	private String color;
	private String leyenda;
	private int refuerzos = 0;
	private int maxRefuerzos;
	private int costurasExtra = 0;
	private Cliente cliente;
	private double valor;
	private List<Taller> talleres = new ArrayList<>();
	
	// constructores
	
	public Uniforme(Material material, String color, int maxRefuerzos) {
		this.material = material;
		this.color = color;
		this.maxRefuerzos = maxRefuerzos;
	}
	
	// getters y setters
	
	public Material getMaterial() {
		return this.material;
	}
	
	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLeyenda() {
		return this.leyenda;
	}

	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}
	
	public void setRefuerzos(int refuerzos) {
		this.refuerzos = refuerzos;
	}

	public void setCosturasExtra(int costurasExtra) {
		this.costurasExtra = costurasExtra;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double getValor() {
		return this.valor;
	}
	
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public List<Taller> getTalleres() {
		return talleres;
	}

	// consultas
	
	/**
	 * ejercicio 1
	 * retorna el grado de resistencia del uniforme
	 */
	public double resistencia() {
		return (material.getResistencia() + this.refuerzos / 2)
				* (1 + 0.2 * this.costurasExtra);
	}

	/**
	 * ejercicio 3.b
	 * indica si el uniforme tiene el grado de resistencia suficiente
	 */
	public boolean tieneResistenciaRequerida() {
		return this.cliente.tieneResistenciaCorrecta(this);
	}

	/**
	 * retorna el color requerido para el uniforme
	 */
	public String colorRequerido() {
		return this.cliente.colorRequerido();
	}
	
	/**
	 * ejercicio 3.a
	 * indica si el uniforme es del color correcto
	 */
	public boolean tieneColorRequerido() {
		return this.cliente.tieneColorCorrecto(this);
	}

	/**
	 * retorna la leyenda requerida para el uniforme
	 */
	public String leyendaRequerida() {
		return cliente.leyendaRequerida();
	}
	
	/**
	 * ejercicio 3.c
	 * indica si el uniforme tiene estampada la leyenda correcta
	 */
	public boolean tieneLeyendaRequerida() {
		return this.cliente.tieneLeyendaCorrecta(this);
	}

	/**
	 * indica si el uniforme tiene bordada alguna leyenda
	 */
	public boolean tieneLeyenda() {
		return this.leyenda != null;
	}

	/**
	 * ejercicio 3.d
	 * indica si el uniforme ya está listo para entregar
	 */
	public boolean yaSePuedeEntregar() {
		return this.tieneColorRequerido()
				&& this.tieneLeyendaRequerida()
				&& this.tieneResistenciaRequerida();
	}

	/**
	 * indica si se le puede agregar una costura extra al uniforme
	 */
	public boolean soportaCosturaExtra() {
		return this.costurasExtra < this.material.getMaxCosturasExtra();
	}

	/**
	 * indica si se le puede agregar un refuerzo más al uniforme
	 */
	public boolean soportaUnRefuerzoMas() {
		return this.refuerzos < this.maxRefuerzos;
	}

	/**
	 * ejercicio 6.b.i
	 * retorna la cantidad de talleres por los que pasó el uniforme
	 */
	public int porCuantosTalleresPaso() {
		return talleres.size();
	}
	
	/**
	 * ejercicio 6.b.ii
	 * indica si el uniforma pasó por el taller
	 */
	public boolean pasoPor(Taller taller) {
		return talleres.contains(taller);
	}
	
	/**
	 * ejercicio 6.b.iii
	 * retorna el último taller por el que pasó el uniforme
	 */
	public Taller ultimoTaller() {
		return this.talleres.get(talleres.size() - 1);
	}
	
	// acciones
	
	/**
	 * agrega una costura extra al uniforme
	 */
	public void agregarCosturaExtra() {
		this.costurasExtra++;
	}

	/**
	 * agrega un refuerzo al uniforme
	 */
	public void agregarRefuerzo() {
		this.refuerzos++;
	}

	/**
	 * ejercicio 6.a
	 * registra el paso de un uniforme por un taller
	 */
	public void pasarPorTaller(Taller taller) {
		taller.procesar(this);
		this.talleres.add(taller);
	}
	
}
