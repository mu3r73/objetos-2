package venta_masiva.modelo.item.paquete;

/**
 * representa un envoltorio tipo film alveolar, tiene un peso
 */
public class FilmAlveolar extends Envoltorio {
	
	// constructor
	public FilmAlveolar() {
		super(80);
	}

}
