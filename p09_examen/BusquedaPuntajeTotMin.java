package p09_examen;

public class BusquedaPuntajeTotMin extends Busqueda {

	private int puntajeTotMin;

	// getters / setters
	
	public void setPuntajeTotMin(int puntajeTotMin) {
		this.puntajeTotMin = puntajeTotMin;
	}

	// consultas

	@Override
	public boolean califica(Aspirante aspirante) {
		return super.califica(aspirante)
				&& aspirante.puntajeTotObt() >= this.puntajeTotMin;
	}

}
