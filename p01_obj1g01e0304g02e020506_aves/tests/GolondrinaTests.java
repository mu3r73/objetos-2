package p01_obj1g01e0304g02e020506_aves.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import p01_obj1g01e0304g02e020506_aves.Alpiste;
import p01_obj1g01e0304g02e020506_aves.Golondrina;

public class GolondrinaTests {

	@Test
	public void test() {
		Golondrina pepita = new Golondrina();
		pepita.comer(new Alpiste(100));
		pepita.volar(10);
		pepita.volar(20);
		assertEquals(350, pepita.energia());
		assertFalse(pepita.estaDebil());
		assertFalse(pepita.estaFeliz());
		assertEquals(80, pepita.cuantoQuiereVolar());
	}
	
}
