package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.Costurero;
import p02_pepero.Deposito;
import p02_pepero.Material;
import p02_pepero.Uniforme;

public class CostureroTests {

	private Material lino = new Material("lino", 4, 5);

	private Uniforme uni = new Uniforme(lino, "púrpura", 5);
	
	private Deposito depo = new Deposito("depo");

	private Costurero costu = new Costurero();

	@Test
	public void testEj2PuedeProcesar() {
		
		uni.setRefuerzos(2);
		uni.setCosturasExtra(1);
		assertTrue(costu.puedeProcesar(uni));
		
		uni.setCosturasExtra(5);
		assertFalse(costu.puedeProcesar(uni));
		
	}
	
	@Test
	public void testNecesitaProcesar() {
		
		uni.setCliente(depo);
		uni.setRefuerzos(2);
		uni.setCosturasExtra(1);
		
		assertTrue(costu.necesitaProcesar(uni));
		
		uni.agregarCosturaExtra();
		
		assertFalse(costu.necesitaProcesar(uni));
		
	}

}
