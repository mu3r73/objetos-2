package p10_car_service.vehiculo;

public class Bujia {

	private double potencia;

	public Bujia(double potencia) {
		super();
		this.potencia = potencia;
	}

	public double getPotencia() {
		return this.potencia;
	}

	public void setPotencia(double potencia) {
		this.potencia = potencia;
	}
	
}
