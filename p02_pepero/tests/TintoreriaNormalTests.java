package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.Material;
import p02_pepero.TintoreriaNormal;
import p02_pepero.Uniforme;

public class TintoreriaNormalTests {
	
	private Material lino = new Material("lino", 4, 5);
	private Material denim = new Material("denim", 6, 3);
	private Material poliester = new Material("poliéster", 5, 4); 
	
	@Test
	public void testEj2PuedeProcesar() {
		TintoreriaNormal laReCarola = new TintoreriaNormal();
		laReCarola.agregarMaterialTenhible(lino);
		laReCarola.agregarMaterialTenhible(denim);
		laReCarola.agregarColorTransformable("blanco");
		laReCarola.agregarColorTransformable("amarillo");
		
		Uniforme u1 = new Uniforme(lino, "blanco", 5);
		Uniforme u2 = new Uniforme(poliester, "amarillo", 5);
		Uniforme u3 = new Uniforme(denim, "fucsia", 5);

		assertTrue(laReCarola.puedeProcesar(u1));
		assertFalse(laReCarola.puedeProcesar(u2));
		assertFalse(laReCarola.puedeProcesar(u3));
	}

}
