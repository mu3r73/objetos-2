package venta_masiva.modelo.item.elemento;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import venta_masiva.modelo.item.paquete.Cierre;
import venta_masiva.modelo.item.paquete.Envoltorio;
import venta_masiva.modelo.item.paquete.Etiqueta;
import venta_masiva.modelo.item.paquete.Folio;
import venta_masiva.modelo.item.producto.Libro;

/**
 * representa un ejemplar de un libro -
 * tiene un número de inventario (que debería ser único)
 */
public class Ejemplar extends Elemento<Libro> {
	
	int cantFolios = 0;

	// constructor
	public Ejemplar(Libro libro, int nroInventario) {
		super(libro, nroInventario);
	}
	
	// getters / setters
	public Libro getLibro() {
		return this.getProducto();
	}
	
	// acciones
	@Override
	public Collection<Envoltorio> crearEnvoltorios() {
		return IntStream.range(0, this.getCantFolios())
				.mapToObj(n -> new Folio())
				.collect(Collectors.toSet());
	}
	
	@Override
	public Collection<Cierre> crearCierres() {
		int cant = this.getCantFolios() * 3;
		return IntStream.range(0, cant)
				.mapToObj(n -> new Etiqueta())
				.collect(Collectors.toSet());
	}
	
	/**
	 * retorna la cantidad de folios necesaria para envolver el ejemplar
	 * @return cantidad de folios necesaria para envolver el ejemplar, según su superficie
	 */
	private int getCantFolios() {
		if (this.cantFolios == 0) {
			Double cantReal = this.getLibro().getSuperficieEmbalaje() / Folio.getSuperficie();
			this.cantFolios = cantReal.intValue();
			if (this.cantFolios < cantReal) {
				this.cantFolios++;
			}
		}
		return this.cantFolios;
	}
	
}
