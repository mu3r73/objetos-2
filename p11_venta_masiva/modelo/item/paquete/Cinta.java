package venta_masiva.modelo.item.paquete;

/**
 * representa una cinta para cerrar un envoltorio, tiene un peso
 */
public class Cinta extends Cierre {
	
	// constructor
	public Cinta() {
		super(10);
	}
	
}
