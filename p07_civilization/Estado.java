package p07_civilization;

// req 6
// state
public abstract class Estado {
	
	public abstract boolean esFeliz(Ciudad ciudad);
	
	public abstract void aumentarPoblacion(Ciudad ciudad);
	
	public abstract void descontarTesoroImperioMantenimEdif(Ciudad ciudad);
	
	public abstract void aumentarTesoroImperioProduccEdifEconomicos(Ciudad ciudad);
	
	public abstract void crearUnidadesMilitaresEdifMilitares(Ciudad ciudad);
	
	public void boostearEdificios(Ciudad ciudad) {
		this.boostearEdifCultural(ciudad);
		this.boostearEdifEconomico(ciudad);
		this.boostearEdifMilitar(ciudad);
	}
	
	public abstract void boostearEdifCultural(Ciudad ciudad);
	
	public abstract void boostearEdifEconomico(Ciudad ciudad);
	
	public abstract void boostearEdifMilitar(Ciudad ciudad);

	public abstract Estado getEstadoMejor();

	public abstract Estado getEstadoPeor();

}
