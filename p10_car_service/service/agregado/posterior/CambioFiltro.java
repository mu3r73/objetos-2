package p10_car_service.service.agregado.posterior;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class CambioFiltro extends ServiceAgregadoPosterior {

	public CambioFiltro(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.CAMBIO_FILTRO;
	}

	@Override
	protected int getTiempoAgregado() {
		return 40;
	}
	
	@Override
	protected double getPrecioAgregado() {
		if (this.getCliente().esVIP()) {
			return 0;
		} else {
			return this.getPrecioLista();
		}
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().vaciar(Nivel.DESGASTE_FILTRO_NAFTA);
	}

}
