package venta_masiva.modelo.item.elemento;

import java.util.Collection;

import venta_masiva.modelo.item.paquete.Cierre;
import venta_masiva.modelo.item.paquete.Envoltorio;
import venta_masiva.modelo.item.producto.Producto;

/**
 * clase parametrizada abstracta - 
 * representa un elemento, basado en un producto
 * @param <P> producto base
 */
public abstract class Elemento<P extends Producto> {
	
	private P producto;
	private int nroInventario;
	
	// constructor
	public Elemento(P producto, int nroInventario) {
		super();
		this.producto = producto;
		this.nroInventario = nroInventario;
	}
	
	// getters / setters
	public int getNroInventario() {
		return this.nroInventario;
	}
	
	public P getProducto() {
		return producto;
	}
	
	// acciones
	/**
	 * retorna los envoltorios necesarios para el elemento
	 * @return envoltorios (tipo y cantidad) requeridos para el elemento
	 */
	public abstract Collection<Envoltorio> crearEnvoltorios();
	
	/**
	 * retorna los cierres necesarios para el elemento
	 * @return cierres (tipo y cantidad) requeridos para el elemento
	 */
	public abstract Collection<Cierre> crearCierres();
	
}
