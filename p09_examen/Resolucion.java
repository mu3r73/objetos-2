package p09_examen;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Resolucion {

	private Aspirante aspirante;
	private Enunciado examen;
	private Set<Respuesta<Pregunta>> respuestas = new HashSet<>();

	// altas a colecciones
	
	public void agregarRespuesta(Respuesta<Pregunta> respuesta) {
		respuestas.add(respuesta);
	}

	// consultas
	
	public double puntajeObt() {
		return this.respuestas.stream()
				.mapToDouble(respuesta -> respuesta.puntajeObtenido())
				.sum();
	}
	
	public boolean estaAprobada() {
		return this.examen.estaAprobada(this);
	}

	public int cantRespCorrectas() {
		return this.respuestas.stream()
				.filter(respuesta -> respuesta.esCorrecta())
				.collect(Collectors.toSet())
				.size();
	}

	public boolean perteneceA(Aspirante aspirante) {
		return aspirante == this.aspirante;
	}

	public boolean esPara(Enunciado examen) {
		return examen == this.examen;
	}
	
	public Respuesta<Pregunta> getRespuestaAPregunta(Pregunta pregunta) {
		return this.respuestas.stream()
			.filter(respuesta -> respuesta.getPregunta().equals(pregunta))
			.findAny()
			.get();
	}
	
}
