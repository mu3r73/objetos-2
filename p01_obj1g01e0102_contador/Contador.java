package p01_obj1g01e0102_contador;

public class Contador {

	protected int c = 0;
	protected String ultcom = "";
	
	public void reset() {
		this.c = 0;
		this.ultcom = "reset";
	}
	
	public void inc() {
		this.c++;
		this.ultcom = "incremento";
	}
	
	public void dec() {
		this.c--;
		this.ultcom = "decremento";
	}
	
	public int valorActual() {
		return this.c;
	}
	
	public String ultimoComando() {
		return this.ultcom;
	}
	
}
