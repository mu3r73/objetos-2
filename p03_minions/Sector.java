package p03_minions;

/**
 * Sector: puede ser defendido o limpiado por un minion
 */
public class Sector {

	protected final int estaminaReq = 1;
	protected double amenaza = 0;
	
	// getters / setters
	
	public double getEstaminaReq() {
		return this.estaminaReq;
	}
	
	public double getAmenaza() {
		return this.amenaza;
	}

	public void setAmenaza(double amenaza) {
		this.amenaza = amenaza;
	}
	
}
