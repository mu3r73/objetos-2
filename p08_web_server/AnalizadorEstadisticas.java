package p08_web_server;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class AnalizadorEstadisticas implements Analizador {
	
	private Collection<Respuesta> respuestas = new HashSet<>();
	
	// altas a colecciones
	
	@Override
	public void registrar(Respuesta respuesta, Optional<Modulo> modulo) {
		this.respuestas.add(respuesta);
	}
	
	// consultas
	
	public float tiempoDeRespuestaPromedio() {
		return this.respuestas.stream()
				.mapToInt(resp -> resp.getDemora())
				.sum()
				/ this.respuestas.size();
	}
	
	public int cantRespuestasEntre(LocalDateTime fInicio, LocalDateTime fFin) {
		return this.respuestasEntre(fInicio, fFin)
				.size();
	}

	private Collection<Respuesta> respuestasEntre(LocalDateTime fInicio, LocalDateTime fFin) {
		return WSCollectionUtils.filtrar(this.respuestas,
									resp -> resp.getPedido().getDiaYHora().isAfter(fInicio)
											&& resp.getPedido().getDiaYHora().isBefore(fFin));
	}
	
	public int cantRespuestasCuyoPayloadIncluye(String s) {
		return this.respuestasCuyoPayloadIncluye(s)
				.size();
	}

	private Collection<Respuesta> respuestasCuyoPayloadIncluye(String s) {
		return WSCollectionUtils.filtrar(this.respuestas,
									resp -> resp.getPayload().toLowerCase().contains(s.toLowerCase()));
	}
	
	public float porcPedidosConRespuestaExitosa() {
		return this.respuestasExitosas().size() / this.respuestas.size();
	}

	private Collection<Respuesta> respuestasExitosas() {
		return WSCollectionUtils.filtrar(this.respuestas,
									resp -> resp.getStatusCode().esExitoso());
	}
	
}
