package p02_pepero;

/**
 * costurero
 * puede agregar una costura extra a un uniforme
 */
public class Costurero extends Taller {

	// consultas
	
	/**
	 * indica si el costurero puede agregar una costura extra al uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return uniforme.soportaCosturaExtra();
	}

	/**
	 * indica si el uniforme necesita una costura extra
	 */
	@Override
	public boolean necesitaProcesar(Uniforme uniforme) {
		return !uniforme.tieneResistenciaRequerida();
	}

	// acciones
	
	/**
	 * agrega una costura extra al uniforme
	 */
	@Override
	public void procesar(Uniforme uniforme) {
		uniforme.agregarCosturaExtra();
	}

}
