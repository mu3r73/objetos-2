package p03_minions;

/**
 * LimpiarSector: Tarea específica que puede realizar un Minion
 */
public class LimpiarSector implements Tarea {

	protected static int dificultad = 10;
	protected Sector sector;
	
	// constructores
	
	public LimpiarSector(Sector sector) {
		super();
		this.sector = sector;
	}

	// getters / setters
	
	public static void setDificultad(int dificultad) {
		LimpiarSector.dificultad = dificultad;
	}

	// consultas
	
	/**
	 * retorna la dificultad de limpiar el sector para el minion
	 */
	@Override
	public double dificultad(Minion minion) {
		return LimpiarSector.dificultad;
	}

	/**
	 * indica si el sector puede ser limpiado por el minion
	 */
	@Override
	public boolean puedeSerEjecutadaPor(Minion minion) {
		return minion.puedeLimpiar(this.sector);
	}

	// acciones
	
	/**
	 * el sector es limpiado por el minion
	 */
	@Override
	public void serEjecutadaPor(Minion minion) {
		minion.limpiar(this.sector);
	}

}
