package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.Deposito;
import p02_pepero.Material;
import p02_pepero.Remendon;
import p02_pepero.Uniforme;

public class RemendonTests {

	private Material lino = new Material("lino", 4, 5);

	private Uniforme uni = new Uniforme(lino, "púrpura", 5);
	
	private Deposito depo = new Deposito("depo");

	private Remendon remen = new Remendon();
	
	@Test
	public void testEj2PuedeProcesar() {
		
		uni.setRefuerzos(2);
		uni.setCosturasExtra(3);
		assertTrue(remen.puedeProcesar(uni));
		
		uni.setRefuerzos(5);
		assertFalse(remen.puedeProcesar(uni));
		
	}

	@Test
	public void testNecesitaProcesar() {
		
		uni.setCliente(depo);
		uni.setRefuerzos(2);
		uni.setCosturasExtra(1);
		
		assertTrue(remen.necesitaProcesar(uni));
		
		uni.agregarCosturaExtra();
		
		assertFalse(remen.necesitaProcesar(uni));
		
	}

}
