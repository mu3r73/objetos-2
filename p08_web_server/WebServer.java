package p08_web_server;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class WebServer {
	
	private Collection<Modulo> modulos = new HashSet<>();
	private Collection<Analizador> analizadores = new HashSet<>();
	
	// altas/bajas a/de colecciones
	
	public void agregarModulo(Modulo modulo) {
		this.modulos.add(modulo);
	}
	
	public void agregarAnalizador(Analizador analizador) {
		this.analizadores.add(analizador);
	}
	
	public void quitarAnalizador(Analizador analizador) {
		this.analizadores.remove(analizador);
	}
	
	// acciones
	
	public Respuesta atender(Pedido pedido) {
		Respuesta resp;
		Optional<Modulo> modulo = Optional.empty();
		
		if (pedido.getURL().getProtocolo().toLowerCase().equals("http")) {
			resp = new Respuesta(0, new StatusCode(501), "");
		} else {
			modulo = this.buscarModuloQuePuedaAtender(pedido);
			if (!modulo.isPresent()) {
				resp = new Respuesta(0, new StatusCode(404), "");
			} else {
				resp = modulo.get().atender(pedido);
				resp.setStatusCode(new StatusCode(200));
			}
		}
		
		resp.setPedido(pedido);
		this.notificarAnalizadores(resp, modulo);
		return resp;
	}
	
	private Optional<Modulo> buscarModuloQuePuedaAtender(Pedido pedido) {
		return this.modulos.stream()
				.filter(modulo -> modulo.puedeAtender(pedido))
				.findFirst();
	}
	
	private void notificarAnalizadores(Respuesta resp, Optional<Modulo> modulo) {
		for (Analizador analizador : analizadores) {
			analizador.registrar(resp, modulo);
		}
	}
	
}
