package venta_masiva.modelo;

import java.util.Collection;
import java.util.HashSet;

import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.item.producto.Vino;

/**
 * clase singleton -
 * representa un store, con los clientes, libros y vinos del servicio
 */
public class Store {
	
	private static Store instancia;

	private Collection<Cliente> clientes = new HashSet<>();
	private Collection<Libro> libros = new HashSet<>();
	private Collection<Vino> vinos = new HashSet<>();
	
	// constructor
	private Store() {
		super();
	}
	
	// instancia - singleton
	public static Store getInstancia() {
		if (Store.instancia == null) {
			Store.instancia = new Store();
		}
		return Store.instancia;
	}
	
	// getters / setters
	public Collection<Cliente> getClientes() {
		return this.clientes;
	}
	
	// altas a colecciones
	public void agregarClientes(Cliente ... clientes) {
		for (Cliente cliente : clientes) {
			this.clientes.add(cliente);
		}
	}
	
	public void agregarLibros(Libro ... libros) {
		for (Libro libro : libros) {
			this.libros.add(libro);
		}
	}
	
	public void agregarVinos(Vino ... vinos) {
		for (Vino vino : vinos) {
			this.vinos.add(vino);
		}
	}
	
	// consultas
	/**
	 * retorna el cliente con el número de DNI indicado 
	 * (requiere que no haya más de un cliente con el mismo DNI)
	 * @param dni el dni buscado
	 * @return el cliente con ese dni
	 */
	public Cliente getCliente(int dni) {
		return this.clientes.stream()
				.filter(cliente -> cliente.getDNI() == dni)
				.findAny()
				.orElse(null);
	}
	
	/**
	 * retorna el libro con el título y autores indicados
	 * (requiere que no haya más de un libro con el mismo título para esos autores)
	 * @param titulo título del libro buscado
	 * @param autores autores del libro buscado (pueden indicarse algunos o todos)
	 * @return el libro con ese título y autores
	 */
	public Libro getLibro(String titulo, String ... autores) {
		return this.libros.stream()
				.filter(libro -> libro.getTitulo().equals(titulo)
						&& libro.tieneAutores(autores))
				.findAny()
				.orElse(null);
	}
	
	/**
	 * retorna el vino de la bodega indicada
	 * (requiere que no haya más de un vino para esa bodega)
	 * @param bodega bodega del vino buscado
	 * @return el vino de esa bodega
	 */
	public Vino getVino(String bodega) {
		return this.vinos.stream()
				.filter(vino -> vino.getBodega().equals(bodega))
				.findAny()
				.orElse(null);
	}
	
	/**
	 * retorna todos los productos en el store
	 * @return todos los productos (libros + vinos)
	 */
	public Collection<Producto> getProductos() {
		Collection<Producto> ps = new HashSet<>();
		
		ps.addAll(this.libros);
		ps.addAll(this.vinos);
		
		return ps;
	}
	
	// datos de prueba
	/**
	 * carga clientes, libros y vinos para tests
	 */
	public void cargarDatosDePrueba() {
		if (!this.libros.isEmpty()) {
			return;
		}
		// creo clientes, autores, libros, vinos
		this.cargarClientes();
		this.cargarLibros();
		this.cargarVinos();
	}
	
	private void cargarClientes() {
		this.clientes.add(new Cliente(12345678, "Álvaro Álvarez", "12 Nº 21", 7223, "General Belgrano"));
		this.clientes.add(new Cliente(23456789, "Benito Benítez", "1516 Nº 1615", 7203, "Rauch"));
		this.clientes.add(new Cliente(34567890, "Fernando Fernández", "1112 Nº 1211", 7200, "Las Flores"));
		this.clientes.add(new Cliente(45678901, "Gonzalo González", "910 Nº 10900", 1987, "Ranchos"));
		this.clientes.add(new Cliente(56789012, "Martín Martínez", "1314 Nº 1413", 7130, "Chascomús"));
		this.clientes.add(new Cliente(67890123, "Pedro Pérez", "34 Nº 43", 7116, "Pila"));
		this.clientes.add(new Cliente(78901234, "Ramiro Ramírez", "56 Nº 65", 7220, "Monte"));
		this.clientes.add(new Cliente(89012345, "Rodrigo Rodríguez", "78 Nº 87", 7245, "Roque Pérez"));
	}

	private void cargarLibros() {
		this.libros.add(new Libro(340.19, 214.2, 342.72,
				20.32, 13.97, 1.27, "Ficciones", "Jorge Luis Borges"));
		this.libros.add(new Libro(240.97, 182.4, 291.83,
				20.32, 13.21, 1.52, "El Aleph", "Jorge Luis Borges"));
		this.libros.add(new Libro(430.91, 245.11, 392.18,
				27.69, 21.59, 1.76, "Dioses americanos", "Neil Gaiman"));
		this.libros.add(new Libro(181.44, 70.98, 113.57,
				22.86, 15.24, 0.51, "En las montañas de la locura", "H. P. Lovecraft"));
		this.libros.add(new Libro(113.4, 290.57, 464.92,
				17.78, 11.43, 2.54, "El libro de la tierra negra", "Carlos Gardini"));
		this.libros.add(new Libro(226.8, 162.27, 259.63,
				20.32, 13.97, 0.76, "Mundo anillo", "Larry Niven"));
		this.libros.add(new Libro(635.03, 269.55, 431.28,
				22.86, 15.24, 2.29, "La magia desaparece", "Larry Niven"));
		this.libros.add(new Libro(181.44, 165.32, 264.52,
				20.32, 13.46, 1.78, "El hombre en el castillo", "Philip K. Dick"));
		this.libros.add(new Libro(345.86, 200.37, 320.58,
				20.32, 13.46, 1.52, "Ubik", "Philip K. Dick"));
		this.libros.add(new Libro(204.12, 112.31, 179.7,
				20.32, 13.21, 1.27, "Más que humano", "Theodore Sturgeon"));
		this.libros.add(new Libro(498.95, 206.66, 330.65,
				20.32, 13.46, 2.29, "Un mundo feliz", "Aldous Huxley"));
		this.libros.add(new Libro(635.03, 142.68, 228.29,
				21.34, 14.48, 4.32, "Un fuego sobre el abismo", "Vernor Vinge"));
		this.libros.add(new Libro(226.8, 32.17, 51.47,
				20.32, 13.21, 2.29, "Orgullo y prejuicio y zombis", "Seth Grahame-Smith", "Jane Austen"));
	}
	
	private void cargarVinos() {
		this.vinos.add(new Vino(1000, 26.99, 43.18,
				"Arizu", "Gidora", "King Gidora"));
		this.vinos.add(new Vino(1250, 23.09, 36.94,
				"Crotta", "Gojira"));
		this.vinos.add(new Vino(1000, 20.1, 32.16,
				"Marolio", "Majinga", "Majinga Zetto"));
		this.vinos.add(new Vino(750, 16, 25.6,
				"Toro", "Radon"));
		this.vinos.add(new Vino(750, 35.69, 57.1,
				"Uvita", "Mosura"));
	}
	
}
