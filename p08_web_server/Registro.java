package p08_web_server;

public class Registro {
	
	private String ip;
	private String ruta;
	private StatusCode statusCode;

	// constructores
	
	public Registro(String ip, String ruta, StatusCode statusCode) {
		super();
		this.ip = ip;
		this.ruta = ruta;
		this.statusCode = statusCode;
	}

	// getters / setters
	
	public String getIp() {
		return ip;
	}
	
	public String getRuta() {
		return ruta;
	}
	
	public StatusCode getStatusCode() {
		return statusCode;
	}
	
}
