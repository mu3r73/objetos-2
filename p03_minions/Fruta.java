package p03_minions;

/**
 * Fruta: puede ser consumida por un Minion para recuperar estámina
 */
public class Fruta {

	protected final String nombre;
	protected final double estaminaQueAporta;
	
	// constructores
	
	public Fruta(String nombre, double estamina) {
		super();
		this.nombre = nombre;
		this.estaminaQueAporta = estamina;
	}
	
	// getters / setters

	public double getEstaminaQueAporta() {
		return this.estaminaQueAporta;
	}
	
}
