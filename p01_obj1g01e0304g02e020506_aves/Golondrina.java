package p01_obj1g01e0304g02e020506_aves;

public class Golondrina implements Ave {

	protected int energia = 0;
	
	public void comer(Comida c) {
		this.energia += c.energia();
	}
	
	public void volar(int kilometros) {
		this.energia -= 10 + kilometros;
	}
	
	public int energia() {
		return this.energia;
	}
	
	public boolean estaDebil() {
		return (this.energia < 50);
	}
	
	public boolean estaFeliz() {
		return ((this.energia >= 500) && (this.energia <= 1000));
	}
	
	public int cuantoQuiereVolar() {
		int cuanto = this.energia / 5;
		if ((this.energia >= 300) && (this.energia <= 400)) {
			cuanto += 10;
		}
		if (this.energia % 20 == 0) {
			cuanto += 15;
		}
		return cuanto;
	}
	
	public void haceLoQueQuieras() {
		if (this.estaDebil()) {
			this.comer(new Alpiste(20));
		}
		if (this.estaFeliz()) {
			this.volar(this.cuantoQuiereVolar());
		}
	}
	
}
