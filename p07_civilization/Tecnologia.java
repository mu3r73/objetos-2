package p07_civilization;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Tecnologia {
	
	private String nombre;
	private Collection<Tecnologia> tecnologiasRequeridas = new HashSet<>();
	private int cultura;
	
	// constructores
	
	public Tecnologia(String nombre, Collection<Tecnologia> tecnologiasRequeridas, int cultura) {
		super();
		this.nombre = nombre;
		this.tecnologiasRequeridas = tecnologiasRequeridas;
		this.cultura = cultura;
	}
	
	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Collection<Tecnologia> getTecnologiasRequeridas() {
		return this.tecnologiasRequeridas;
	}
	
	public int getCultura() {
		return this.cultura;
	}

	public Collection<Tecnologia> getTecnologiasFaltantes(Collection<Tecnologia> tecnologias) {
		Set<Tecnologia> res = new HashSet<>();
		res.addAll(tecnologiasRequeridas);
		res.removeAll(tecnologias);
		return res;
	}
	
}
