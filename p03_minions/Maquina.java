package p03_minions;

import java.util.HashSet;
import java.util.Set;

/**
 * Máquina: puede ser reparada por un minion
 */
public class Maquina {

	protected double complejidad;
	protected Set<Herramienta> herramientasReq =  new HashSet<>();
	
	// constructores
	
	public Maquina(double complejidad) {
		super();
		this.complejidad = complejidad;
	}

	// getters / setters
	
	public double getComplejidad() {
		return this.complejidad;
	}

	public Set<Herramienta> getHerramientasReq() {
		return this.herramientasReq;
	}
	
	// altas a colecciones
	
	public void agregarHerramientaReq(Herramienta herramienta) {
		this.herramientasReq.add(herramienta);
	}
	
}
