package venta_masiva.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.item.elemento.Botella;
import venta_masiva.modelo.item.elemento.Ejemplar;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.pedido.Envio;
import venta_masiva.modelo.pedido.Pedido;

public class P3_4_EnvioMultiPedidoTests {

	private Deposito depo;
	private Store store;
	
	@Before
	public void setUp() {
		this.depo = Deposito.getInstancia();
		this.depo.cargarDatosDePrueba();
		this.depo.eliminarElementos();
		
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
	}
	
	@Test
	public void testAgregarseAEnvio() {
		// creo pedidos
		Libro l1 = this.store.getLibro("El libro de la tierra negra", "Carlos Gardini");
		Libro l2 = this.store.getLibro("En las montañas de la locura", "H. P. Lovecraft");
		Libro l3 = this.store.getLibro("Orgullo y prejuicio y zombis", "Seth Grahame-Smith", "Jane Austen");
		Vino v1 = this.store.getVino("Marolio");
		Vino v2 = this.store.getVino("Crotta");
		
		Cliente c = this.store.getCliente(23456789);
		Pedido p1 = new Pedido(c, l1, l2);
		Pedido p2 = new Pedido(c, v1, v2);
		Pedido p3 = new Pedido(c, l3, v1);
		
		// agrego stock a depósito
		Ejemplar e1 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e2 = l2.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e3 = l3.crearEjemplar(Deposito.getNroInventario());
		Botella b1 = v1.crearBotella(Deposito.getNroInventario());
		Botella b2 = v1.crearBotella(Deposito.getNroInventario());
		Botella b3 = v2.crearBotella(Deposito.getNroInventario());
		
		this.depo.hallarSectorSimpleLlamado("A1").agregarEjemplares(e1, e2, e3);
		this.depo.hallarSectorSimpleLlamado("F2").agregarBotellas(b1, b2, b3);
		
		// creo envío
		Envio e = (new Pedido(c)).crearEnvio(); // agrego pedido vacío
		assertTrue(e.getElementos().isEmpty()); // (para probar que no hay elementos)
		
		p1.agregarseAEnvio(e);
		assertEquals(e.getElementos(), new HashSet<>(Arrays.asList(e1, e2)));
		
		p2.agregarseAEnvio(e);
		p3.agregarseAEnvio(e);
		assertEquals(e.getElementos(), new HashSet<>(Arrays.asList(e1, e2, e3, b1, b2, b3)));
	}
	
	@Test(expected = RuntimeException.class)
	public void testAgregarseAEnvio_distintoCliente() {
		Cliente c1 = this.store.getCliente(56789012);
		Cliente c2 = this.store.getCliente(89012345);
		
		// creo envío para cliente c1, con pedido (vacío)
		Envio e = (new Pedido(c1)).crearEnvio();
		
		// intento agregar nuevo pedido (vacío) para cliente c2 -> debería generar excepción
		(new Pedido(c2)).agregarseAEnvio(e);
	}

}
