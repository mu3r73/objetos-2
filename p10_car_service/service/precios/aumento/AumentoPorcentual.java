package p10_car_service.service.precios.aumento;

public class AumentoPorcentual extends Aumento {

	private double porcentaje;

	public AumentoPorcentual(double porcentaje) {
		super();
		this.setPorcentaje(porcentaje);
	}
	
	public void setPorcentaje(double porcentaje) {
		if ((porcentaje < 0) || (porcentaje > 1)) {
			throw new RuntimeException("el porcentaje de aumento debe ser un número real entre 0 y 1");
		}
		this.porcentaje = porcentaje;
	}
	
	@Override
	public double agregarAumento(double precio) {
		return precio * (1 + this.porcentaje);
	}

}
