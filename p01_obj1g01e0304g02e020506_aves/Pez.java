package p01_obj1g01e0304g02e020506_aves;

public class Pez implements Comida {

	protected int edad;
	
	public Pez(int edad) {
		super();
		this.edad = edad;
	}
	
	@Override
	public int energia() {
		return this.edad * 2;
	}

}
