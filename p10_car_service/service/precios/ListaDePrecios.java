package p10_car_service.service.precios;

import java.util.HashMap;
import java.util.Map;

import p10_car_service.service.Tipo;
import p10_car_service.service.precios.aumento.Aumento;

public class ListaDePrecios {

	private static ListaDePrecios lista;
	private static Map<Tipo, Double> precios = new HashMap<>();
	private static Aumento aumento = new Aumento();
	
	private ListaDePrecios() {
		super();
	}
	
	public static ListaDePrecios getInstancia() {
		if (ListaDePrecios.lista == null) {
			ListaDePrecios.lista = new ListaDePrecios();
		}
		return ListaDePrecios.lista;
	}
	
	public double getPrecio(Tipo tipo) {
		return ListaDePrecios.aumento.agregarAumento(getPrecioBase(tipo));
	}

	private Double getPrecioBase(Tipo tipo) {
		return ListaDePrecios.precios.getOrDefault(tipo, 0.0);
	}
	
	public void setPrecio(Tipo tipo, double precio) {
		ListaDePrecios.precios.put(tipo, precio);
	}
	
	public void cargarDatos() {
		ListaDePrecios.precios.put(Tipo.ALINEACION, 1800.0);
		ListaDePrecios.precios.put(Tipo.CAMBIO_BUJIA, 200.0);
		ListaDePrecios.precios.put(Tipo.CAMBIO_FILTRO, 3500.0);
		ListaDePrecios.precios.put(Tipo.CARGA_ACEITE, 1200.0);
		ListaDePrecios.precios.put(Tipo.CARGA_AGUA, 300.0);
		ListaDePrecios.precios.put(Tipo.CARGA_NAFTA, 550.0);
		ListaDePrecios.precios.put(Tipo.ELIMINACION_BOLLOS, 2000.0);
		ListaDePrecios.precios.put(Tipo.ELIMINACION_RAJADURAS, 3000.0);
		ListaDePrecios.precios.put(Tipo.LIMPIEZA_EXTERIOR, 400.0);
		ListaDePrecios.precios.put(Tipo.LIMPIEZA_TANQUE_NAFTA, 600.0);
		ListaDePrecios.precios.put(Tipo.PINTURA, 30000.0);
		ListaDePrecios.precios.put(Tipo.VACIADO_ACEITE, 220.0);
		ListaDePrecios.precios.put(Tipo.VACIADO_AGUA, 40.0);
	}
	
}
