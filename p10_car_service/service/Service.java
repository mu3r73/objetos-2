package p10_car_service.service;

import p10_car_service.Cliente;
import p10_car_service.service.precios.ListaDePrecios;
import p10_car_service.vehiculo.Vehiculo;

public abstract class Service {

	public abstract Vehiculo getVehiculo();
	
	public abstract Cliente getCliente();
	
	public abstract Tipo getTipo();
	
	public abstract boolean seAbreCapot();
	
	public abstract boolean seTrabajaSobreChapa();
	
	public abstract boolean incluyeServicio(Tipo tipo);

	public abstract int getTiempo();

	public abstract double getPrecio();

	public double getPrecioLista() {
		return ListaDePrecios.getInstancia().getPrecio(this.getTipo());
	}

	public abstract void aplicar();

}
