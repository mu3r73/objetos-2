package p09_examen;

public class EnunciadoRiguroso extends Enunciado {

	int minRespCorrectas;
	
	// getters / setters

	public void setMinRespCorrectas(int minRespCorrectas) {
		this.minRespCorrectas = minRespCorrectas;
	}
	
	// consultas
	
	@Override
	public boolean estaAprobada(Resolucion resolucion) {
		return super.estaAprobada(resolucion)
				&& resolucion.cantRespCorrectas() >= this.minRespCorrectas;
	}

}
