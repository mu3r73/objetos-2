package venta_masiva.modelo.pedido;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.impresora.Impresora;
import venta_masiva.modelo.item.ItemConPeso;
import venta_masiva.modelo.item.elemento.Elemento;
import venta_masiva.modelo.item.paquete.Cierre;
import venta_masiva.modelo.item.paquete.Envoltorio;
import venta_masiva.modelo.item.producto.Producto;

/**
 * representa un envío -
 * tiene un identificador (que debería ser único), una colección de pedidos asociados,  
 * una colección de elementos a enviar, sus envoltorios y sus cierres
 */
public class Envio {

	protected int identificador;
	private Collection<Pedido> pedidos = new HashSet<>();
	private Collection<Elemento<? extends Producto>> elementos = new HashSet<>();
	private Collection<Envoltorio> envoltorios = new HashSet<>();
	private Collection<Cierre> cierres = new HashSet<>();
	
	// constructor
	public Envio(int identificador, Pedido pedido) {
		this.identificador = identificador;
		this.agregarPedido(pedido);
	}
	
	// getters / setters
	public int getIdentificador() {
		return this.identificador;
	}
	
	public Collection<Elemento<? extends Producto>> getElementos() {
		return this.elementos;
	}
	
	public Collection<Envoltorio> getEnvoltorios() {
		return this.envoltorios;
	}
	
	public Collection<Cierre> getCierres() {
		return this.cierres;
	}
	
	// altas a colecciones
	/**
	 * agrega un pedido al envío
	 * (genera una excepción si el pedido no pertenece al mismo cliente que este envío)
	 * @param pedido pedido a agregar
	 */
	protected void agregarPedido(Pedido pedido) {
		this.checkMismoCliente(pedido);
		this.pedidos.add(pedido);
	}
	
	/**
	 * si el pedido no pertenece al mismo cliente que este envío, genera una excepción
	 * @param pedido pedido a verificar
	 */
	private void checkMismoCliente(Pedido pedido) {
		if (!this.pedidos.isEmpty()
				&& (!this.getCliente().equals(pedido.getCliente()))) {
			throw new RuntimeException("no coincide el cliente del pedido (" + pedido.getCliente().getNombre()
					+ ") con el cliente del envío (" + this.getCliente().getNombre());
		}
	}
	
	public void agregarElemento(Elemento<? extends Producto> elemento) {
		this.elementos.add(elemento);
	}

	public void agregarEnvoltorios(Collection<Envoltorio> envoltorios) {
		this.envoltorios.addAll(envoltorios);
	}

	public void agregarCierres(Collection<Cierre> cierres) {
		this.cierres.addAll(cierres);
	}
	
	// consultas
	/**
	 * retorna el cliente de este envío
	 * @return cliente de este envío
	 */
	public Cliente getCliente() {
		return this.pedidos.stream()
				.findAny()
				.get()
				.getCliente();
	}
	
	/**
	 * retorna el peso total de este envío
	 * @return la suma de los pesos de todos los items en el envío (elementos + envoltorios + cierres)
	 */
	public double getPesoTotal() {
		return this.getTodosLosItems().stream()
				.mapToDouble(item -> item.getPeso())
				.sum();
	}
	
	/**
	 * retorna todos los items en este envío
	 * @return todos los items (elementos + envoltorios + cierres) en este envío
	 */
	private Collection<ItemConPeso> getTodosLosItems() {
		Collection<ItemConPeso> is = new ArrayList<>();
		is.addAll(this.elementos.stream()
					.map(elemento -> elemento.getProducto())
					.collect(Collectors.toList()));
		is.addAll(this.envoltorios);
		is.addAll(this.cierres);
		return is;
	}
	
	// acciones
	/**
	 * imprime la etiqueta correspondiente a este envío, usando la impresora del sistema
	 * (si no hay impresora configurada en el sistema, genera una excepción)
	 */
	public void imprimirEtiqueta() {
		Impresora impresora = Expedicion.getInstancia().getImpresora();
		if (impresora == null) {
			throw new RuntimeException("no se puede imprimir porque no hay impresora configurada");
		}
		
		Cliente c = this.getCliente();
		impresora.ponerTexto(c.getNombre());
		impresora.saltarLinea();
		impresora.ponerTexto(c.getDireccion());
		impresora.ponerTexto("(" + c.getCodPostal() + ") " + c.getLocalidad());
		impresora.saltarLinea();
		impresora.ponerTexto(this.getElementos().size() + " artículos - peso total "
								+ this.getPesoTotal() + " kg");
		impresora.saltarLinea();
	}
	
}
