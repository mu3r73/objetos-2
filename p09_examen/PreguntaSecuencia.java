package p09_examen;

import java.util.HashSet;
import java.util.Set;

public class PreguntaSecuencia extends Pregunta {

	private Set<String> opciones = new HashSet<>();
	private String ordenCorrecto;

	// getters / setters
	
	public void setOrdenCorrecto(String orden) {
		this.ordenCorrecto = orden;
	}
	
	// altas a colecciones
	
	public void agregarOpcion(String opcion) {
		this.opciones.add(opcion);
	}
	
	// consultas

	public boolean esCorrecta(RespuestaSecuencia respuesta) {
		return respuesta.getSecuencia().equals(this.ordenCorrecto)
				|| respuesta.getSecuencia().equals(this.ordenInverso());
	}

	private String ordenInverso() {
		return new StringBuilder(this.ordenCorrecto).reverse().toString();
	}

}
