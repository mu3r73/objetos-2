package venta_masiva.modelo.item.paquete;

import venta_masiva.modelo.item.ItemConPeso;

/**
 * clase abstracta -
 * representa un envoltorio, tiene un peso
 */
public abstract class Envoltorio extends ItemConPeso {
	
	// constructor
	public Envoltorio(double peso) {
		super(peso);
	}

}
