package p02_pepero;

/**
 * fábrica
 * requiere uniformes con un grado mínimo de resistencia (específico de cada instancia)
 * no tiene requisitos para el color de sus uniformes
 * realiza descuento por cantidad, al recbir > 100 uniformes (general de la clase)
 */
public class Fabrica extends Cliente {

	private double resistenciaMin;
	private static double descuentoPorCantidad = 8;
	
	// constructores
	
	public Fabrica(String nombre, double resistenciaMin) {
		super(nombre);
		this.resistenciaMin = resistenciaMin;
	}
	
	// getters y setters
	
	public static void setDescuentoPorCantidad(double descuentoPorCantidad) {
		Fabrica.descuentoPorCantidad = descuentoPorCantidad;
	}

	// consultas
	
	/**
	 * una fábrica no tiene color requerido
	 * nunca debería consultarse a una fábrica su color requerido
	 * => tira una RuntimeException 
	 */
	@Override
	public String colorRequerido() {
		throw new RuntimeException("no se le puede preguntar a una fábrica su color requerido");
	}

	/**
	 * indica que el color del uniforme siempre es aceptable
	 */
	@Override
	public boolean tieneColorCorrecto(Uniforme uniforme) {
		return true;
	}

	/**
	 * indica si el uniforme tiene la resistencia requerida
	 */
	@Override
	public boolean tieneResistenciaCorrecta(Uniforme uniforme) {
		return uniforme.resistencia() >= this.resistenciaMin;
	}

	/**
	 * ejercicio 5.b
	 * retorna cuánto hay que facturarle al cliente
	 * si el cliente recibió más de 100 uniformes, se realiza un descuento
	 */
	@Override
	public double totalAFacturar() {
		double total = super.totalAFacturar();
		if (this.uniformes.size() > 100) {
			total *= 1 - (descuentoPorCantidad / 100);
		}
		return total;
	}

}
