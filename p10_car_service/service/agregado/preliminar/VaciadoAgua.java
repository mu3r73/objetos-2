package p10_car_service.service.agregado.preliminar;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class VaciadoAgua extends ServiceAgregadoPreliminar {

	public VaciadoAgua(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.VACIADO_AGUA;
	}

	@Override
	protected int getTiempoAgregado() {
		return 0;
	}

	@Override
	protected double getPrecioAgregado() {
		return 0;
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().vaciar(Nivel.AGUA_RADIADOR);
		this.getVehiculo().vaciar(Nivel.AGUA_SAPITO);
	}
	
}
