package p01_obj1g05e03_remiseria;

public class ChevroletCorsa implements Vehiculo {

	protected final int capacidad = 4;
	protected final int velocidadMaxima = 150;
	protected final int peso = 1300;
	protected final String color;
	
	// constructor
	public ChevroletCorsa(String color) {
		super();
		this.color = color;
	}
	
	@Override
	public int capacidad() {
		return this.capacidad;
	}

	@Override
	public int velocidadMaxima() {
		return this.velocidadMaxima;
	}

	@Override
	public int peso() {
		return this.peso;
	}

	@Override
	public String color() {
		return this.color;
	}

}
