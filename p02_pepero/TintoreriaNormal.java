package p02_pepero;

import java.util.HashSet;
import java.util.Set;

/**
 * tintorería que sólo puede trabajar sobre ciertos materiales y colores base
 *
 */
public class TintoreriaNormal extends Tintoreria {

	// variables internas
	
	private Set<Material> materialesTenhibles = new HashSet<>();
	private Set<String> coloresTransformables = new HashSet<>();
	
	// altas y bajas sobre colecciones internas
	
	public void agregarMaterialTenhible(Material material) {
		this.materialesTenhibles.add(material);
	}
	
	public void quitarMaterialTenhible(Material material) {
		this.materialesTenhibles.remove(material);
	}
	
	public void agregarColorTransformable(String color) {
		this.coloresTransformables.add(color);
	}
	
	public void quitarColorTransformable(String color) {
		this.coloresTransformables.remove(color);
	}
	
	// consultas
	
	/**
	 * ejercicio 2
	 * indica si la tintorería puede teñir el uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return this.puedeTrabajarSobre(uniforme.getMaterial())
				&& this.puedeTransformar(uniforme.getColor());
	}

	/**
	 * indica si la tintorería puede teñir el material
	 */
	private boolean puedeTrabajarSobre(Material material) {
		return this.materialesTenhibles.contains(material);
	}

	/**
	 * indica si la tintorería puede transformar el color 
	 */
	private boolean puedeTransformar(String color) {
		return this.coloresTransformables.contains(color);
	}
	
}
