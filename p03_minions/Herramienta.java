package p03_minions;

/**
 * Herramienta: usada por un minion para reparar una máquina
 */
public class Herramienta {

	protected String descripcion;

	// constructores
	
	public Herramienta(String descripcion) {
		super();
		this.descripcion = descripcion;
	}

	// getters / setters
	
	public String getDescripcion() {
		return descripcion;
	}
	
	// consultas

	/**
	 * indica si dos herramientas son "del mismo tipo"
	 */
	public boolean mismoTipoQue(Herramienta herramienta) {
		return this.descripcion == herramienta.getDescripcion();
	}

}
