package p10_car_service.service.precios.aumento;

public class AumentoMontoFijo extends Aumento {

	private double montoAAumentar;

	public AumentoMontoFijo(double montoAAumentar) {
		super();
		this.montoAAumentar = montoAAumentar;
	}
	
	public void setMontoAAumentar(double monto) {
		this.montoAAumentar = monto;
	}

	@Override
	public double agregarAumento(double precio) {
		return precio + this.montoAAumentar;
	}
	
}
