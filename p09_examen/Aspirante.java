package p09_examen;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Aspirante {

	private String nombre;
	private Busqueda busqueda;
	private Set<Resolucion> resoluciones = new HashSet<>();

	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	// consultas

	public boolean entregoResolucionPara(Enunciado examen) {
		return this.resoluciones.stream()
				.anyMatch(resolucion -> resolucion.esPara(examen));
	}

	public double puntajeTotObt() {
		return this.resoluciones.stream()
				.mapToDouble(resolucion -> resolucion.puntajeObt())
				.sum();
	}

	public Set<Resolucion> resolucionesAprobadas() {
		return this.resoluciones.stream()
				.filter(resolucion -> resolucion.estaAprobada())
				.collect(Collectors.toSet());
	}

	// acciones
	
	public void entregarResolucion(Resolucion resolucion) {
		this.resoluciones.add(resolucion);
		busqueda.recibirResolucion(resolucion);
	}

}
