package p01_obj1g01e0304g02e020506_aves;

public class Gorrion implements Ave {
	
	protected int energia = 0;

	public void comer(Comida c) {
		this.energia += c.energia() / 2.0;
	}

	public void volar(int kilometros) {
		this.energia -= 1 + 0.5 * kilometros;
	}

	public int energia() {
		return this.energia;
	}

	public boolean estaDebil() {
		return (this.energia < 50);
	}

	public boolean estaFeliz() {
		return ((energia >= 500) && (energia <= 1000));
	}

	public int cuantoQuiereVolar() {
		return 1;
	}

	public void haceLoQueQuieras() {
		this.volar(this.cuantoQuiereVolar());
	}

}
