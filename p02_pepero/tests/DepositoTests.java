package p02_pepero.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p02_pepero.Deposito;
import p02_pepero.Material;

public class DepositoTests {

	private Material denim = new Material("denim", 6, 3);
	
	private Deposito depo1 = new Deposito("depo1");
	private Deposito depo2 = new Deposito("depo2");
	
	@Before
	public void b4() {
		// depo1 recibe 100 uniformes con valor = 400
		depo1.recibirUniformes(Generadores.generarUniformes(100, denim, 400));
		
		// depo2 recibe 101 uniformes con valor = 400
		depo2.recibirUniformes(Generadores.generarUniformes(101, denim, 400));
	}
	
	
	@Test
	public void test5bCuantoHayQueFacturarle() {
		
		assertEquals(40000, depo1.totalAFacturar(), 0);
		
		assertEquals(40400, depo2.totalAFacturar(), 0);
		
	}

}
