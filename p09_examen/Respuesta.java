package p09_examen;

public abstract class Respuesta<T extends Pregunta> {

	protected T pregunta;
	
	// consultas
	
	public abstract double puntajeObtenido();
	
	public abstract boolean esCorrecta();
	
	public T getPregunta() {
		return this.pregunta;
	}
	
}
