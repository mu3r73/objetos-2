package p02_pepero;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * centro de procesamiento
 * - recibe uniformes a procesar,
 * - los envía a sus talleres asociados para que los modifiquen según requisitos de los clientes,
 * - los entrega a los clientes cuando están listos
 */
public class CentroDeProcesamiento {

	private Set<Taller> talleres = new HashSet<>();
	private Set<Uniforme> uniformes = new HashSet<>();
	
	// altas a colecciones
	
	/**
	 * agrega un taller a su colección de talleres
	 */
	public void agregarTaller(Taller taller) {
		this.talleres.add(taller);
	}
	
	/**
	 * agrega un uniforme a su colección de uniformes
	 */
	public void recibirUniforme(Uniforme uniforme) {
		this.uniformes.add(uniforme);
	}
	
	// bajas de colecciones
	
	/**
	 * elimina todos los uniformes en us de su colección de uniformes
	 */
	private void retirarUniformes(Set<Uniforme> us) {
		this.uniformes.removeAll(us);
	}

	// consultas
	
	/**
	 * ejercicio 4.a
	 * retorna los clientes para los que ay uniformes listos, sin repeticiones
	 */
	public Set<Cliente> clientesConUniformesListos() {
		return this.uniformesListos().stream()
				.map(uniforme -> uniforme.getCliente())
				.collect(Collectors.toSet());
	}
	
	/**
	 * ejercicio 4.b
	 * retorna la cantidad de uniformes listos que tiene el centro
	 */
	public int cantUniformesListos() {
		return this.uniformesListos().size();
	}
	
	/**
	 * ejercicio 4.c
	 * retorna el valor total de los uniformes listos
	 */
	public double valorTotalUniformesListos() {
		return this.uniformesListos().stream()
				.mapToDouble(uniforme -> uniforme.getValor())
				.sum();
	}
	
	/**
	 * ejercicio 6.c.i
	 * retorna con qué talleres se trabajó en los uniformes del cliente
	 * DUDA: ¿debería trabajar sobre talleres, o sobre uniformesListos()?
	 */
	public Set<Taller> talleresQueProcesaronUniformesDe(Cliente cliente) {
		return this.UniformesDe(cliente).stream()
				.flatMap(uniforme -> uniforme.getTalleres().stream())
				.collect(Collectors.toSet());
	}
	
	/**
	 * retorna los uniformes del cliente
	 */
	private Set<Uniforme> UniformesDe(Cliente cliente) {
		return this.uniformes.stream()
				.filter(uniforme -> uniforme.getCliente() == cliente)
				.collect(Collectors.toSet());
	}

	/**
	 * ejercicio 6.c.ii
	 * retorna qué uniformes pasaron por el taller
	 * DUDA: ¿debería trabajar sobre talleres, o sobre uniformesListos()?
	 */
	public Set<Uniforme> uniformesQuePasaronPor(Taller taller) {
		return this.uniformes.stream()
				.filter(uniforme -> uniforme.pasoPor(taller))
				.collect(Collectors.toSet());
	}
	
	/**
	 * retorna los uniformes que están listos para ser entregados a sus clientes
	 */
	private Set<Uniforme> uniformesListos() {
		return this.uniformes.stream()
				.filter(uniforme -> uniforme.yaSePuedeEntregar())
				.collect(Collectors.toSet());
	}

	/**
	 * retorna los uniformes listos del cliente
	 */
	public Set<Uniforme> uniformesListosDe(Cliente cliente) {
		return this.uniformesListos().stream()
				.filter(uniforme -> uniforme.getCliente() == cliente)
				.collect(Collectors.toSet());
	}
	
	// acciones
	
	/**
	 * ejercicio 5.a
	 * entrega al cliente todos los uniformes listos que le pertenecen
	 */
	public void entregarUniformesListosDe(Cliente cliente) {
		Set<Uniforme> us = this.uniformesListosDe(cliente);
		this.retirarUniformes(us);
		cliente.recibirUniformes(us);
	}
	
	/**
	 * ejercicio 7
	 * envía un uniforme a un taller para ser procesado, si tiene sentido hacerlo
	 */
	public void enviarUniformeA(Taller taller, Uniforme uniforme) {
		if (taller.tieneSentidoProcesar(uniforme)) {
			uniforme.pasarPorTaller(taller);
		}
	}

}
