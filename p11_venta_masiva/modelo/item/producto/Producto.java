package venta_masiva.modelo.item.producto;

import venta_masiva.modelo.item.ItemConPeso;

/**
 * clase abstracta -
 * representa un producto con peso, costo y precio de venta
 */
public abstract class Producto extends ItemConPeso {
	
	private double costo;
	private double precioVenta;
	
	// constructor
	public Producto(double peso, double costo, double precioVenta) {
		super(peso);
		this.costo = costo;
		this.precioVenta = precioVenta;
	}
	
	// getters / setters
	public double getCosto() {
		return this.costo;
	}
	
	public double getPrecioVenta() {
		return this.precioVenta;
	}
	
	// consultas
	/**
	 * retorna la descripción del producto
	 * @return descripción del producto
	 */
	public abstract String getDescripcion();
	
}
