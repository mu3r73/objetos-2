package venta_masiva.modelo.cliente;

import java.util.List;

import venta_masiva.modelo.pedido.Pedido;

/**
 * representa a un cliente -
 * tiene un dni (debería ser único), un nombre, una dirección, un código postal, una localidad, 
 * y una cuenta corriente
 */
public class Cliente {
	
	private int dni;
	private String nombre;
	private String direccion;
	private int codPostal;
	private String localidad;
	private CuentaCorriente cuentaCorriente;
	
	// constructor
	public Cliente(int dni, String nombre, String direccion, int codPostal, String localidad) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.direccion = direccion;
		this.codPostal = codPostal;
		this.localidad = localidad;
		this.cuentaCorriente = new CuentaCorriente();
	}
	
	// getters / setters
	public int getDNI() {
		return this.dni;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
	
	public int getCodPostal() {
		return this.codPostal;
	}
	
	public String getLocalidad() {
		return this.localidad;
	}
	
	public CuentaCorriente getCuentaCorriente() {
		return this.cuentaCorriente;
	}
	
	// altas a colecciones
	public void agregarPedido(Pedido pedido) {
		this.cuentaCorriente.agregarPedido(pedido);
	}
	
	public void agregarPago(Pago pago) {
		this.cuentaCorriente.agregarPago(pago);
	}
	
	// comparación
	/**
	 * indica si éste es el mismo que el cliente indicado
	 * @param cliente cliente a comparar
	 * @return true si tienen el mismo número de DNI; false en caso contrario
	 */
	public boolean equals(Cliente cliente) {
		return this.dni == cliente.getDNI();
	}
	
	// consultas
	/**
	 * retorna el saldo de la cuenta corriente del cliente
	 * @return saldo de la cuenta corriente
	 */
	public double getSaldo() {
		return this.cuentaCorriente.getSaldo();
	}
	
	/**
	 * retorna los movimientos de la cuenta corriente del cliente, ordenados por fecha
	 * @return moviemientos de la cuenta corriente, ordenados por fecha
	 */
	public List<Movimiento> getMovimientosOrdenFecha() {
		return this.cuentaCorriente.getMovimientosOrdenFecha();
	}
	
	// acciones
	// para pruebas
	/**
	 * borra *todos* los movimientos de la cuenta corriente del cliente
	 */
	public void resetCuentaCorriente() {
		this.cuentaCorriente.reset();
	}
		
}
