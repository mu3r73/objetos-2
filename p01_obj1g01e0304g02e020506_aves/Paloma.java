package p01_obj1g01e0304g02e020506_aves;

public class Paloma implements Ave {

	protected int calorias = 0;
	protected int kilometros = 0;
	
	public void comer(Comida c) {
		// 1 kcaloria = 4184 joules
		// 1000 calorias = 4184 joules
		this.calorias += 4184 * c.energia() / 1000;
	}
	
	public void volar(int km) {
		this.kilometros += km;
	}
	
	public int kmsRecorridos() {
		return this.kilometros;
	}
	
	public int caloriasIngeridas() {
		return this.calorias;
	}
	
	public void haceLoQueQuieras() {
		// no hace nada
	}
	
}
