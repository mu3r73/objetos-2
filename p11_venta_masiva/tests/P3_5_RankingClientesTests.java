package venta_masiva.tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.expedicion.ClienteConImporteTotal;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.pedido.Pedido;

public class P3_5_RankingClientesTests {
	
	private Store store;
	private Expedicion exped;
	
	@Before
	public void setUp() {
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
		
		this.exped = Expedicion.getInstancia();
		this.exped.eliminarPedidosYEnvios();
	}
	
	@Test
	public void testRankingClientes() {
		
		
		// creo los pedidos
		Cliente c1 = this.store.getCliente(23456789);
		Cliente c2 = this.store.getCliente(34567890);
		Cliente c3 = this.store.getCliente(45678901);
		Cliente c4 = this.store.getCliente(67890123);
		
		Libro l1 = this.store.getLibro("Ficciones", "Jorge Luis Borges"); // $342.72
		Libro l2 = this.store.getLibro("Dioses americanos", "Neil Gaiman"); // $392.18
		Libro l3 = this.store.getLibro("El libro de la tierra negra", "Carlos Gardini"); // $464.92
		Libro l4 = this.store.getLibro("Un fuego sobre el abismo", "Vernor Vinge"); // $228.29
		Vino v1 = this.store.getVino("Arizu"); // $43.18
		Vino v2 = this.store.getVino("Marolio"); // $32.16
		
		// pedido cliente c1 - pre-fecha inicio
		this.exped.agregarPedido(new Pedido(c1, LocalDateTime.parse("2017-10-31T20:20:20"), v1, v2));
		
		// pedidos cliente c2 - $342.72 + $464.92 + $228.29
		this.exped.agregarPedido(new Pedido(c2, LocalDateTime.parse("2017-11-01T08:08:08"), l1, l3)); 
		this.exped.agregarPedido(new Pedido(c2, LocalDateTime.parse("2017-11-13T23:23:23"), l4));
		
		// pedido cliente c3 - $32.16 x 3
		this.exped.agregarPedido(new Pedido(c3, LocalDateTime.parse("2017-11-06T14:14:14"), v2, v2, v2));
		
		// pedidos cliente c4 - $392.18 + $228.29 + $43.18 x 2; último pedido post-fecha fin
		this.exped.agregarPedido(new Pedido(c4, LocalDateTime.parse("2017-11-05T13:13:13"), l2, l4)); 
		this.exped.agregarPedido(new Pedido(c4, LocalDateTime.parse("2017-11-15T18:18:18"), v1, v1)); 
		this.exped.agregarPedido(new Pedido(c4, LocalDateTime.parse("2017-11-16T03:03:03"), l1, l3)); 
		
		// creo ranking
		List<ClienteConImporteTotal> ranking = this.exped.getRankingClientesOrdenImporteTotal(
				LocalDateTime.parse("2017-11-01T00:00:00"), LocalDateTime.parse("2017-11-15T23:59:59"));
		
		assertEquals(ranking.size(), 3);
		assertEquals(ranking.get(0).getCliente(), c2);
		assertEquals(ranking.get(0).getImporteTotal(), 342.72 + 464.92 + 228.29, 0);
		assertEquals(ranking.get(1).getCliente(), c4);
		assertEquals(ranking.get(1).getImporteTotal(), 392.18 + 228.29 + 2 * 43.18, 0);
		assertEquals(ranking.get(2).getCliente(), c3);
		assertEquals(ranking.get(2).getImporteTotal(), 3 * 32.16, 0);
	}
	
}
