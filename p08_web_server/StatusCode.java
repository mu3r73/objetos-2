package p08_web_server;

public class StatusCode {
	
	private int statusCode;
	
	// constructores
	
	public StatusCode(int statusCode) {
		super();
		this.statusCode = statusCode;
	}
	
	// consultas
	
	public boolean esExitoso() {
		return this.statusCode == 200;
	}

}
