package p03_minions;

/**
 * Soldado: Rol específico de un empleado
 */
public class Soldado extends Rol {

	protected int practica = 0;
	
	// constructores
	
	public Soldado(Minion minion) {
		super(minion);
	}

	// consultas
	
	/**
	 * retorna la fuerza del Soldado
	 */
	@Override
	public double fuerza() {
		return super.fuerza() + this.practica;
	}

	// acciones
	
	/**
	 * defiende el sector
	 */
	@Override
	public void defender(Sector sector) {
		// no pierde estamina
		this.ganarPractica();
	}

	/**
	 * incrementa práctica (por defender un sector)
	 */
	public void ganarPractica() {
		this.practica += 2;
	}

}
