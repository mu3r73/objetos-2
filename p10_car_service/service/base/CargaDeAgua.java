package p10_car_service.service.base;

import p10_car_service.Cliente;
import p10_car_service.service.Tipo;
import p10_car_service.vehiculo.Nivel;
import p10_car_service.vehiculo.Vehiculo;

public class CargaDeAgua extends ServiceBase {

	public CargaDeAgua(Vehiculo vehiculo, Cliente cliente) {
		super(vehiculo, cliente);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.CARGA_AGUA;
	}
	
	@Override
	public boolean seAbreCapot() {
		return true;
	}

	@Override
	public int getTiempo() {
		return 30;
	}
	
	@Override
	public double getPrecio() {
		return this.getPrecioLista();
	}

	@Override
	public void aplicar() {
		this.vehiculo.cargar(Nivel.AGUA_RADIADOR, 2.0);
		this.vehiculo.cargar(Nivel.AGUA_SAPITO, 5.0);
	}

}
