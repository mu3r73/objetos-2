package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.BordadorDescosedor;
import p02_pepero.BordadorNormal;
import p02_pepero.Deposito;
import p02_pepero.Material;
import p02_pepero.Uniforme;

public class BordadorTests {

	private Material lino = new Material("lino", 4, 5);

	private Uniforme uni = new Uniforme(lino, "rojo", 5);
	
	private Deposito depo = new Deposito("depo");
	
	private BordadorNormal bordan = new BordadorNormal();
	private BordadorDescosedor bordad = new BordadorDescosedor();

	@Test
	public void testNecesitaProcesar() {
		
		uni.setCliente(depo);
		assertTrue(bordad.necesitaProcesar(uni));
		assertTrue(bordan.necesitaProcesar(uni));
		
		uni.setLeyenda(uni.leyendaRequerida());
		assertFalse(bordad.necesitaProcesar(uni));
		assertFalse(bordan.necesitaProcesar(uni));
		
	}

}
