package p03_minions;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Capataz: Rol específico de un empleado
 */
public class Capataz extends Rol {

	Set<Minion> subordinados = new HashSet<>();
	
	// constructores
	
	public Capataz(Minion minion) {
		super(minion);
	}

	// altas a colecciones
	
	/**
	 * agrega un subordinado
	 */
	public void agregarSubordinado(Minion minion) {
		this.subordinados.add(minion);
	}

	// consultas
	
	/**
	 * indica si el capataz o alguno de sus subordinados pueden realizar la tarea
	 */
	@Override
	public boolean puedeEjecutar(Tarea tarea) {
		return super.puedeEjecutar(tarea)
				|| !this.subordinadosQuePuedenEjecutar(tarea).isEmpty();
	}
	
	/**
	 * retorna los subordinados capaces de realizar la tarea
	 */
	private Set<Minion> subordinadosQuePuedenEjecutar(Tarea tarea) {
		return this.subordinados.stream()
				.filter(empleado -> empleado.puedeEjecutar(tarea))
				.collect(Collectors.toSet());
	}

	/**
	 * retorna el subordinado con más experiencia entre los de es 
	 */
	private Optional<Minion> subordinadoMasExperimentado(Set<Minion> es) {
		return es.stream()
				.max(Comparator.comparing(Minion::experiencia));
	}
	
	// acciones
	
	/**
	 * ejecuta la tarea
	 * ... si algún subordinado la puede ejecutar, delega
	 * ... si ningún subordinado la puede ejecutar, intenta ejecutarla el Capataz
	 */
	@Override
	public void ejecutar(Tarea tarea) {
		Optional<Minion> subord = this.subordinadoMasExperimentado(
				this.subordinadosQuePuedenEjecutar(tarea));
		if (subord.isPresent()) {
			subord.get().ejecutar(tarea);
		} else {
			super.ejecutar(tarea);
		}
	}

}
