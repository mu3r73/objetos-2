package p09_examen;

public class RespuestaNumerica extends Respuesta<PreguntaNumerica> {

	private PreguntaNumerica pregunta;
	double numero;

	// getters / setters
	
	public void setPregunta(PreguntaNumerica pregunta) {
		this.pregunta = pregunta;
	}
	
	public double getNumero() {
		return this.numero;
	}
	
	public void setNumero(double respuesta) {
		this.numero = respuesta;
	}
	
	// consultas
	
	@Override
	public double puntajeObtenido() {
		if (pregunta.esExacta(this)) {
			return pregunta.getPuntaje();
		} else {
			if (pregunta.esMarginal(this)) {
				return pregunta.getPuntajeMarginal();
			} else {
				return 0;
			}
		}
	}

	@Override
	public boolean esCorrecta() {
		return this.pregunta.esCorrecta(this);
	}

}
