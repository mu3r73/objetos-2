package p10_car_service.service.precios.aumento;

public class AumentoSegunUmbral extends Aumento {

	private double precioUmbral;
	private double montoUmbralYMenos;
	private double porcentMasQueUmbral;

	public AumentoSegunUmbral(double precioUmbral, double montoUmbralYMenos, double porcentMasQueUmbral) {
		super();
		this.precioUmbral = precioUmbral;
		this.montoUmbralYMenos = montoUmbralYMenos;
		this.setPorcentMasQueUmbral(porcentMasQueUmbral);
	}

	public void setPrecioUmbral(double umbral) {
		this.precioUmbral = umbral;
	}
	
	public void setMontoMenoresQueUmbral(double monto) {
		this.montoUmbralYMenos = monto;
	}
	
	public void setPorcentMasQueUmbral(double porcentaje) {
		if ((porcentaje < 0) || (porcentaje > 1)) {
			throw new RuntimeException("el porcentaje de aumento debe ser un número real entre 0 y 1");
		}
		this.porcentMasQueUmbral = porcentaje;
	}

	@Override
	public double agregarAumento(double precio) {
		if (precio <= precioUmbral) {
			return precio + this.montoUmbralYMenos;
		} else {
			return precio * (1 + this.porcentMasQueUmbral);
		}
	}
	
}
