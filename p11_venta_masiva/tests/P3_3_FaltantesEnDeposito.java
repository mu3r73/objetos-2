package venta_masiva.tests;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.pedido.Pedido;

public class P3_3_FaltantesEnDeposito {

	private Deposito depo;
	private Store store;
	private Expedicion exped;
	
	@Before
	public void setUp() {
		this.depo = Deposito.getInstancia();
		this.depo.cargarDatosDePrueba();
		this.depo.eliminarElementos();
		
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
		
		this.exped = Expedicion.getInstancia();
		this.exped.eliminarPedidosYEnvios();
	}
	
	@Test
	public void testFaltantesEnDeposito() {
		// productos que vamos a tener en stock
		Libro l1 = this.store.getLibro("Ubik", "Philip K. Dick");
		Libro l2 = this.store.getLibro("Un mundo feliz", "Aldous Huxley");
		Libro l3 = this.store.getLibro("Mundo anillo", "Larry Niven");
		Vino v1 = this.store.getVino("Marolio");
		Vino v2 = this.store.getVino("Uvita");
		Vino v3 = this.store.getVino("Toro");
		
		// inicializo stock - 2 ejemplares de l1, 1 ejemplar de l2, 1 ejemplar de l3,
		// 1 botella de v1, 2 botellas de v2, 2 botellas de v3
		this.depo.hallarSectorSimpleLlamado("Primer Piso").agregarEjemplares(
				l1.crearEjemplar(Deposito.getNroInventario()), l1.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(
				l2.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("C").agregarEjemplares(
				l3.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("F1").agregarBotellas(
				v1.crearBotella(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("E").agregarBotellas(
				v2.crearBotella(Deposito.getNroInventario()), v2.crearBotella(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("D").agregarBotellas(
				v3.crearBotella(Deposito.getNroInventario()), v3.crearBotella(Deposito.getNroInventario()));
		
		//creo pedidos
		Cliente c1 = this.store.getCliente(56789012);
		Cliente c2 = this.store.getCliente(67890123);
		Cliente c3 = this.store.getCliente(12345678);
		
		// cliente c1 tiene 2 pedidos: (l1, l2) ya enviado, y (l3) no enviado
		this.exped.procesar(new Pedido(c1, l1, l2));
		this.exped.agregarPedido(new Pedido(c1, l3));
		
		// cliente c2 tiene 2 pedidos: (l1, v1) no enviado, y (l3, v3) no enviado
		this.exped.agregarPedido(new Pedido(c2, l1, v1));
		this.exped.agregarPedido(new Pedido(c2, l3, v3));
		
		// cliente c3 tiene 3 pedidos: (v1, v2) no enviado, (v2, v3) ya enviado, y (v1, v3) no enviado
		this.exped.agregarPedido(new Pedido(c3, v1, v2));
		this.exped.procesar(new Pedido(c3, v2, v3));
		this.exped.agregarPedido(new Pedido(c3, v1, v3));
		
		// faltan: l3 x 1, v1 x 2, v3 x 1 
		Map<Producto, Integer> faltantes = this.depo.obtenerFaltantes();
		
		assertEquals(faltantes.getOrDefault(l1, 0).intValue(), 0);
		assertEquals(faltantes.getOrDefault(l2, 0).intValue(), 0);
		assertEquals(faltantes.getOrDefault(l3, 0).intValue(), 1);
		assertEquals(faltantes.getOrDefault(v1, 0).intValue(), 2);
		assertEquals(faltantes.getOrDefault(v2, 0).intValue(), 0);
		assertEquals(faltantes.getOrDefault(v3, 0).intValue(), 1);
	}

}
