package p08_web_server;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class AnalizadorDeteccionDemora implements Analizador {
	
	private int demoraMin;
	private Collection<Integer> demoras = new HashSet<>();
	
	// constructor
	
	public AnalizadorDeteccionDemora(int demoraMin) {
		super();
		this.demoraMin = demoraMin;
	}
	
	// getters / setters
	
	public void setDemoraMin(int demoraMin) {
		this.demoraMin = demoraMin;
	}
	
	// altas a colecciones
	
	@Override
	public void registrar(Respuesta respuesta, Optional<Modulo> modulo) {
		this.demoras.add(respuesta.getDemora());
	}
	
	// consultas
	
	public int cantRespuestasDemoradas() {
		return this.respuestasDemoradas()
				.size();
	}

	private Collection<Integer> respuestasDemoradas() {
		return WSCollectionUtils.filtrar(this.demoras, demora -> this.excedeDemoraMinima(demora));
	}

	private boolean excedeDemoraMinima(int demora) {
		return demora > this.demoraMin;
	}
	
}
