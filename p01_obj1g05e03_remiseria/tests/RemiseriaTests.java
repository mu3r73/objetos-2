package p01_obj1g05e03_remiseria.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import p01_obj1g05e03_remiseria.AutoDistinto;
import p01_obj1g05e03_remiseria.AutoStandardAGas;
import p01_obj1g05e03_remiseria.ChevroletCorsa;
import p01_obj1g05e03_remiseria.Remiseria;
import p01_obj1g05e03_remiseria.Vehiculo;
import p01_obj1g05e03_remiseria.Viaje;

public class RemiseriaTests {

	protected final Set<String> ningunColor = new HashSet<>();
	protected final Set<String> colVerde = new HashSet<>(Arrays.asList("verde"));
	protected final Set<String> colAzul = new HashSet<>(Arrays.asList("azul"));
	protected final Set<String> colNegro = new HashSet<>(Arrays.asList("negro"));
	
	@Test
	public void testCasoDePruebaB() {
	
		Vehiculo cachito = new ChevroletCorsa("rojo");
		
		Remiseria remiseria1 = new Remiseria(3, 30);
		Remiseria remiseria2 = new Remiseria(2, 20);
		
		remiseria1.agregarAFlota(cachito);
		remiseria1.agregarAFlota(new ChevroletCorsa("negro"));
		remiseria1.agregarAFlota(new ChevroletCorsa("verde"));
		remiseria1.agregarAFlota(new AutoStandardAGas(true));
		remiseria1.agregarAFlota(new AutoDistinto(5, 160, 1200, "beige"));
		
		remiseria2.agregarAFlota(cachito);
		remiseria2.agregarAFlota(new AutoStandardAGas(true));
		remiseria2.agregarAFlota(new AutoStandardAGas(false));
		remiseria2.agregarAFlota(new AutoStandardAGas(false));
		
		assertEquals(1300 * 3 + 1200 + 150 + 1200, remiseria1.pesoTotal());
		assertEquals(1300 + 1200 * 3 + 150, remiseria2.pesoTotal());
		
		assertTrue(remiseria1.esRecomendable());
		assertTrue(remiseria2.esRecomendable());
		
		assertEquals(4 * 3 + 5, remiseria1.capacidadTotalYendoA(140));
		assertEquals(4, remiseria2.capacidadTotalYendoA(140));
		
		assertEquals("beige", remiseria1.colorDelAutoMasRapido());
		assertEquals("rojo", remiseria2.colorDelAutoMasRapido());
		
	}
		
	@Test
	public void testCuantosViajesHizoXvehiculo() {
	
		Remiseria remiseria = new Remiseria(4, 40);
		Vehiculo auto = new ChevroletCorsa("fucsia");
		
		remiseria.agregarAFlota(auto);
		
		// antes de realizar viajes
		assertEquals(0, remiseria.cuantosViajesHizo(auto));
		
		remiseria.registrarViaje(new Viaje(80, 1, 3, colVerde), auto);
		remiseria.registrarViaje(new Viaje(120, 1.5, 1, colAzul), auto);
		remiseria.registrarViaje(new Viaje(200, 2, 2, colNegro), new AutoStandardAGas(true));
		
		assertEquals(2, remiseria.cuantosViajesHizo(auto));
		
	}
	
	@Test
	public void testViajesRealizadosDistanciaMayorAXKm() {
	
		Remiseria remiseria = new Remiseria(4, 40);
		Vehiculo auto = new ChevroletCorsa("fucsia");
		
		remiseria.agregarAFlota(auto);
		
		// antes de realizar viajes
		assertEquals(0, remiseria.viajesRealizadosDistanciaMayorA(100));
		
		remiseria.registrarViaje(new Viaje(80, 1, 3, colVerde), auto);
		remiseria.registrarViaje(new Viaje(120, 1.5, 1, colAzul), auto);
		remiseria.registrarViaje(new Viaje(200, 2, 2, colNegro), new AutoStandardAGas(true));
		
		assertEquals(2, remiseria.viajesRealizadosDistanciaMayorA(100));
		
	}
	
	@Test
	public void testTotalLugaresLibresEnViajesRealizados() {
	
		Remiseria remiseria = new Remiseria(4, 40);
		Vehiculo auto = new ChevroletCorsa("fucsia");
		
		remiseria.agregarAFlota(auto);
		
		// antes de realizar viajes
		assertEquals(0, remiseria.totalLugaresLibresEnViajesRealizados());
		
		remiseria.registrarViaje(new Viaje(80, 1, 3, colVerde), auto);
		remiseria.registrarViaje(new Viaje(120, 1.5, 1, colAzul), auto);
		remiseria.registrarViaje(new Viaje(200, 2, 2, colNegro), new AutoStandardAGas(true));
		
		assertEquals(4 - 3 + 4 - 1 + 3 - 2, remiseria.totalLugaresLibresEnViajesRealizados());
		
	}	
	
	@Test
	public void testCuantoPagarPorViaje() {
		
		Remiseria remiseria = new Remiseria(3, 30);
				
		assertEquals(30, remiseria.cuantoPagarPor(new Viaje(7, 1, 1, ningunColor)));
		assertEquals(75, remiseria.cuantoPagarPor(new Viaje(25, 1, 1, ningunColor)));

	}
		
	@Test
	public void testCuantoPagarleAXVehiculo() {
		
		Remiseria remiseria = new Remiseria(3, 30);
				
		Vehiculo auto1 = new ChevroletCorsa("fucsia");
		Vehiculo auto2 = new AutoStandardAGas(true);
		
		remiseria.agregarAFlota(auto1);
		remiseria.agregarAFlota(auto2);
		
		// antes de realizar viajes
		assertEquals(0, remiseria.cuantoPagarleA(auto1));
		assertEquals(0, remiseria.cuantoPagarleA(auto2));
		
		remiseria.registrarViaje(new Viaje(7, 1, 1, ningunColor), auto1);
		remiseria.registrarViaje(new Viaje(25, 1, 1, ningunColor), auto1);
		remiseria.registrarViaje(new Viaje(33, 1, 1, ningunColor), auto2);

		assertEquals(105, remiseria.cuantoPagarleA(auto1));
		assertEquals(99, remiseria.cuantoPagarleA(auto2));
		
	}

}
