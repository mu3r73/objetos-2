package p04_examen;

public abstract class Respuesta {

	// consultas
	
	public abstract double puntajeObtenido();
	
	public abstract boolean estaAprobada();
	
}
