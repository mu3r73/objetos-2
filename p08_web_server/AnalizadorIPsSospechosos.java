package p08_web_server;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

public class AnalizadorIPsSospechosos implements Analizador {
	
	private Collection<String> ipsSospechosos = new HashSet<>();
	private RegistroDeActivSospechosa reg = new RegistroDeActivSospechosa();
	
	// altas a colecciones
	
	public void agregarIPSospechoso(String ipSospechoso) {
		this.ipsSospechosos.add(ipSospechoso);
	}
	
	// getters / setters
	
	public RegistroDeActivSospechosa getReg() {
		return this.reg;
	}
	
	// acciones
	
	@Override
	public void registrar(Respuesta respuesta, Optional<Modulo> modulo) {
		if (this.esSospechoso(respuesta.getPedido().getIpCliente())) {
			this.reg.registrarActividad(respuesta.getPedido().getIpCliente(),
										respuesta.getPedido().getURL().getRuta(),
										respuesta.getStatusCode());
		}
	}

	private boolean esSospechoso(String ip) {
		return this.ipsSospechosos.contains(ip);
	}

}
