package p09_examen;

public class BusquedaCantMinExamenesAprobados extends Busqueda {

	private int cantMinExamenesAprobados;

	// getters / setters
	
	public void setCantMinExamenesAprobados(int cantMinExamenesAprobados) {
		this.cantMinExamenesAprobados = cantMinExamenesAprobados;
	}

	// consultas
	
	@Override
	public boolean califica(Aspirante aspirante) {
		return super.califica(aspirante)
				&& aspirante.resolucionesAprobadas().size() >= this.cantMinExamenesAprobados;
	}

}
