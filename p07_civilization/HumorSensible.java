package p07_civilization;

public class HumorSensible implements Humor {
	
	@Override
	public int getDisconformidad(Ciudad ciudad) {
		int disconf = Integer.min(ciudad.getCantHabitantes(), 200000) / 20000;
		if (ciudad.getCantHabitantes() > 200000) {
			disconf += (ciudad.getCantHabitantes() - 200000) / 40000;
		}
		
		disconf -= ciudad.getCantEdificiosCulturales();
		
		return disconf;
	}
	
	@Override
	public int getTranquilidad(EdificioCultural edificio) {
		return edificio.getCultura() / edificio.getCiudad().getImperio().getFactorTranquilidad();
	}
	
	@Override
	public int getTranquilidad(EdificioEconomico edificio) {
		return 6;
	}
	
	@Override
	public int getTranquilidad(EdificioMilitar edificio) {
		return 0;
	}
	
}
