package p08_web_server;

import java.util.Collection;
import java.util.HashSet;

public class RegistroDeActivSospechosa {
	
	Collection<Registro> registros = new HashSet<>();
	
	// altas a colecciones
	
	public void registrarActividad(String ip, String ruta, StatusCode statusCode) {
		this.registros.add(new Registro(ip, ruta, statusCode));
	}
	
	// consultas
	
	public int cantPedidosConRespuestaExitosa(String ip) {
		return this.pedidosConRespuestaExitosa(ip)
				.size();
	}
	
	public int cantPedidosConRespuestaNoExitosa(String ip) {
		return this.pedidosConRespuestaNoExitosa(ip)
				.size();
	}
	
	private Collection<Registro> pedidosConRespuestaExitosa(String ip) {
		return WSCollectionUtils.filtrar(this.registrosDeIP(ip), reg -> reg.getStatusCode().esExitoso());
	}
	
	private Collection<Registro> pedidosConRespuestaNoExitosa(String ip) {
		return WSCollectionUtils.filtrar(this.registrosDeIP(ip), reg -> !reg.getStatusCode().esExitoso());
	}
	
	private Collection<Registro> registrosDeIP(String ip) {
		return WSCollectionUtils.filtrar(this.registros, reg -> reg.getIp().equals(ip));
	}
	
	public boolean seConsultoRuta(String ip, String ruta) {
		return this.registrosDeIP(ip).stream()
				.anyMatch(reg -> reg.getRuta().equals(ruta));		
	}
	
}
