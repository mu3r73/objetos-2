package p02_pepero.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p02_pepero.BordadorNormal;
import p02_pepero.Material;
import p02_pepero.Taller;
import p02_pepero.Uniforme;

public class BordadorNormalTests {

	private Material lino = new Material("lino", 4, 5);

	@Test
	public void testEj2PuedeProcesar() {
		
		Uniforme uni = new Uniforme(lino, "púrpura", 5);
		Taller bd = new BordadorNormal();
		assertTrue(bd.puedeProcesar(uni));
		
		uni.setLeyenda("hola");
		assertFalse(bd.puedeProcesar(uni));
		
	}

}
