package p07_civilization;

public abstract class Edificio {
	
	private Ciudad ciudad;
	private int costoMantenimiento;
	private int costoConstruccion;
	
	// constructores
	
	public Edificio(Ciudad ciudad, int costoMantenimiento, int costoConstruccion) {
		super();
		this.ciudad = ciudad;
		this.costoMantenimiento = costoMantenimiento;
		this.costoConstruccion = costoConstruccion;
	}
	
	// getters / setters
	
	public Ciudad getCiudad() {
		return this.ciudad;
	}
	
	public int getCostoMantenimiento() {
		return this.costoMantenimiento;
	}

	public int getCostoConstruccion() {
		return this.costoConstruccion;
	}
	
	// consultas
	
	public abstract int getCultura();
	
	public abstract int getTranquilidad();
	
	public abstract int getValor();
	
	public abstract boolean esDestacado();
	
	// acciones
	
	public abstract void boostear();
	
}
