package p10_car_service.service.agregado.posterior;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class LimpiezaExterior extends ServiceAgregadoPosterior {

	public LimpiezaExterior(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.LIMPIEZA_EXTERIOR;
	}
	
	@Override
	protected int getTiempoAgregado() {
		return 30;
	}

	@Override
	protected double getPrecioAgregado() {
		if (this.servBase.seTrabajaSobreChapa()) {
			return this.getPrecioLista();
		} else {
			return 0;
		}
	}
	
	@Override
	public void aplicarAgregado() {
		this.getVehiculo().vaciar(Nivel.GRADO_SUCIEDAD);
	}
	
}
