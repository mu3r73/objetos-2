package p10_car_service.service.base;

import p10_car_service.Cliente;
import p10_car_service.service.Service;
import p10_car_service.service.Tipo;
import p10_car_service.vehiculo.Vehiculo;

public abstract class ServiceBase extends Service {

	protected Vehiculo vehiculo;
	protected Cliente cliente;

	public ServiceBase(Vehiculo vehiculo, Cliente cliente) {
		super();
		this.vehiculo = vehiculo;
		this.cliente = cliente;
	}
	
	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}
	
	public Cliente getCliente() {
		return this.cliente;
	}

	@Override
	public boolean seAbreCapot() {
		return false;
	}

	@Override
	public boolean seTrabajaSobreChapa() {
		return false;
	}

	@Override
	public boolean incluyeServicio(Tipo tipo) {
		return this.getTipo().equals(tipo);
	}
	
}
