package venta_masiva.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.Store;
import venta_masiva.modelo.item.elemento.Botella;
import venta_masiva.modelo.item.elemento.Ejemplar;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.sector.SectorSimple;

public class P1_DepositoTests {
	
	private Deposito depo;
	private Store store;
	
	@Before
	public void setUp() {
		this.depo = Deposito.getInstancia();
		this.depo.cargarDatosDePrueba();
		this.depo.eliminarElementos();
		
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
	}
	
	@Rule
	public ExpectedException ee = ExpectedException.none();
	
	@Test
	public void testAgregarEjemplarASectorSimple() {
		Libro l = this.store.getLibro("Más que humano", "Theodore Sturgeon");
		int inv = Deposito.getNroInventario();
		Ejemplar e1 = l.crearEjemplar(inv);
		Ejemplar e2 = l.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e3 = l.crearEjemplar(Deposito.getNroInventario());
		
		// antes de agregar elementos a depósito
		this.ee.expect(RuntimeException.class);
		this.ee.expectMessage("no hay un elemento en el depósito con número de inventario " + inv);
		this.depo.hallarSectorQueContieneElementoNro(inv);
		
		// agrego elementos a depósito
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(e1);
		this.depo.hallarSectorSimpleLlamado("A1").agregarEjemplares(e2);
		this.depo.hallarSectorSimpleLlamado("C").agregarEjemplares(e3);
		
		// después de agregar elementos a depósito
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv).getNombre(), "B");
	}
	
	@Test
	public void testAgregarBotellaASectorSimple() {
		Vino v = this.store.getVino("Toro");
		int inv = Deposito.getNroInventario();
		Botella b1 = v.crearBotella(inv);
		Botella b2 = v.crearBotella(Deposito.getNroInventario());
		Botella b3 = v.crearBotella(Deposito.getNroInventario());
		
		// antes de agregar elementos a depósito
		this.ee.expect(RuntimeException.class);
		this.ee.expectMessage("no hay un elemento en el depósito con número de inventario " + inv);
		this.depo.hallarSectorQueContieneElementoNro(inv);
		
		// agrego elementos a depósito
		this.depo.hallarSectorSimpleLlamado("D").agregarBotellas(b1);
		this.depo.hallarSectorSimpleLlamado("E").agregarBotellas(b2);
		this.depo.hallarSectorSimpleLlamado("F2").agregarBotellas(b3);
		
		// después de agregar elementos a depósito
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv).getNombre(), "D");
	}
	
	@Test
	public void testObtenerLibrosDeAutor() {
		// autor tiene varios ejemplares de cada libro en depósito
		Libro l1 = this.store.getLibro("Ficciones", "Jorge Luis Borges");
		Libro l2 = this.store.getLibro("El Aleph", "Jorge Luis Borges");
		Libro l3 = this.store.getLibro("Un mundo feliz", "Aldous Huxley");
		
		// agrego ejemplares a depósito
		Ejemplar e1 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e2 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e3 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e4 = l2.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e5 = l2.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e6 = l3.crearEjemplar(Deposito.getNroInventario());
		
		this.depo.hallarSectorSimpleLlamado("Primer Piso").agregarEjemplares(e1, e4);
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(e2, e5);
		this.depo.hallarSectorSimpleLlamado("A1").agregarEjemplares(e3);
		this.depo.hallarSectorSimpleLlamado("C").agregarEjemplares(e6);
		
		Set<Libro> resEsperado = new HashSet<>(Arrays.asList(l1, l2));  
		assertEquals(this.depo.obtenerLibrosDe("Jorge Luis Borges"), resEsperado);
		
		// autor existe, pero tiene libros en depósito
		assertTrue(this.depo.obtenerLibrosDe("Larry Niven").isEmpty());
	}
	
	@Test
	public void testObtenerElementosDeProducto() {
		Libro l1 = this.store.getLibro("Un fuego sobre el abismo", "Vernor Vinge");
		Libro l2 = this.store.getLibro("En las montañas de la locura", "H. P. Lovecraft");
		Ejemplar e1 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e2 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e3 = l1.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e4 = l2.crearEjemplar(Deposito.getNroInventario());
		
		// libro sin ejemplares en depósito
		assertEquals(this.depo.obtenerElementosDe(l1), new HashSet<>());
		
		// agrego ejemplares a depósito
		this.depo.hallarSectorSimpleLlamado("Primer Piso").agregarEjemplares(e1);
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(e2, e3);
		this.depo.hallarSectorSimpleLlamado("A2").agregarEjemplares(e4);
		
		// libro con ejemplares en depósito
		assertEquals(this.depo.obtenerElementosDe(l1), new HashSet<>(Arrays.asList(e1, e2, e3)));
		
		Vino v1 = this.store.getVino("Arizu");
		Vino v2 = this.store.getVino("Toro");
		Botella b1 = v1.crearBotella(Deposito.getNroInventario());
		Botella b2 = v1.crearBotella(Deposito.getNroInventario());
		Botella b3 = v2.crearBotella(Deposito.getNroInventario());
		
		// vino sin botellas en depósito
		assertEquals(this.depo.obtenerElementosDe(v1), new HashSet<>());
		
		// agrego botellas a depósito
		this.depo.hallarSectorSimpleLlamado("D").agregarBotellas(b1);
		this.depo.hallarSectorSimpleLlamado("F2").agregarBotellas(b2);
		this.depo.hallarSectorSimpleLlamado("E").agregarBotellas(b3);
		
		// vino con botellas en depósito
		assertEquals(this.depo.obtenerElementosDe(v1), new HashSet<>(Arrays.asList(b1, b2)));
	}
	
	@Test
	public void testObtenerSectorQueContieneElementoNro() {
		// agrego ejemplares a depósito
		Libro l = this.store.getLibro("La magia desaparece", "Larry Niven");
		int inv1 = Deposito.getNroInventario();
		Ejemplar e1 = l.crearEjemplar(inv1);
		Ejemplar e2 = l.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e3 = l.crearEjemplar(Deposito.getNroInventario());
		
		this.depo.hallarSectorSimpleLlamado("C").agregarEjemplares(e1);
		this.depo.hallarSectorSimpleLlamado("Primer Piso").agregarEjemplares(e2);
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(e3);
		
		// ejemplar e en sector "C"
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv1).getNombre(), "C");
		
		// agrego botellas a depósito
		Vino v = this.store.getVino("Crotta");
		int inv2 = Deposito.getNroInventario();
		Botella b1 = v.crearBotella(inv2);
		Botella b2 = v.crearBotella(Deposito.getNroInventario());
		Botella b3 = v.crearBotella(Deposito.getNroInventario());
		
		this.depo.hallarSectorSimpleLlamado("E").agregarBotellas(b1);
		this.depo.hallarSectorSimpleLlamado("D").agregarBotellas(b2);
		this.depo.hallarSectorSimpleLlamado("F2").agregarBotellas(b3);
		
		// botella b en sector "E"
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv2).getNombre(), "E");
	}
	
	@Test(expected = RuntimeException.class)
	public void testObtenerSectorQueContieneElementoNro_inexistente() {
		// no existe elemento en depósito
		this.depo.hallarSectorQueContieneElementoNro(Deposito.getNroInventario());
	}
	
	@Test
	public void testRetirarElementoDeSectorSimple() {
		Libro l = this.store.getLibro("Mundo anillo", "Larry Niven");
		int inv1 = Deposito.getNroInventario();
		Ejemplar ej1 = l.crearEjemplar(inv1);
		int inv2 = Deposito.getNroInventario();
		Ejemplar ej2 = l.crearEjemplar(inv2);
		
		// agrego elemento a depósito
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(ej1);
		this.depo.hallarSectorSimpleLlamado("C").agregarEjemplares(ej2);
		
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv1).getNombre(), "B");
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv2).getNombre(), "C");
		
		// quito elemento de depósito
		this.depo.hallarSectorSimpleLlamado("B").retirarElemento(ej1);
		
		assertEquals(this.depo.hallarSectorQueContieneElementoNro(inv2).getNombre(), "C");
		
		this.ee.expect(RuntimeException.class);
		this.ee.expectMessage("no hay un elemento en el depósito con número de inventario " + inv1);
		this.depo.hallarSectorQueContieneElementoNro(inv1);
	}
	
	@Test(expected = RuntimeException.class)
	public void testRetirarElementoDeSectorSimple_inexistente() {
		Libro l = this.store.getLibro("Orgullo y prejuicio y zombis", "Seth Grahame-Smith", "Jane Austen");
		Ejemplar e = l.crearEjemplar(Deposito.getNroInventario());
		
		this.depo.hallarSectorSimpleLlamado("B").retirarElemento(e);
	}
	
	@Test
	public void testVaciarSectorSimple() {
		// agrego elementos a sector "C"
		Libro l = this.store.getLibro("En las montañas de la locura", "H. P. Lovecraft");
		Ejemplar e1 = l.crearEjemplar(Deposito.getNroInventario());
		Ejemplar e2 = l.crearEjemplar(Deposito.getNroInventario()); 
		
		Vino v = this.store.getVino("Uvita");
		Botella b1 = v.crearBotella(Deposito.getNroInventario());
		Botella b2 = v.crearBotella(Deposito.getNroInventario());
		
		SectorSimple ss1 = this.depo.hallarSectorSimpleLlamado("C");		
		ss1.agregarEjemplares(e1, e2);
		ss1.agregarBotellas(b1, b2);
		assertEquals(ss1.getTodosLosElementos(), new HashSet<>(Arrays.asList(e1, e2, b1, b2)));
		
		// sector "F1" está vacío
		SectorSimple ss2 = this.depo.hallarSectorSimpleLlamado("F1");
		assertEquals(ss2.getTodosLosElementos(), new HashSet<>());
		
		// paso todos los elementos de "C" a "F1"
		ss1.vaciar(ss2);
		assertEquals(ss1.getTodosLosElementos(), new HashSet<>());
		assertEquals(ss2.getTodosLosElementos(), new HashSet<>(Arrays.asList(e1, e2, b1, b2)));
	}
	
}
