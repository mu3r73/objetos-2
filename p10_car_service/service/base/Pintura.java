package p10_car_service.service.base;

import p10_car_service.Cliente;
import p10_car_service.service.Tipo;
import p10_car_service.vehiculo.Color;
import p10_car_service.vehiculo.Vehiculo;

public class Pintura extends ServiceBase {
	
	private Color color;
	
	private Pintura(Vehiculo vehiculo, Cliente cliente, Color color) {
		super(vehiculo, cliente);
		this.color = color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public Tipo getTipo() {
		return Tipo.PINTURA;
	}

	@Override
	public boolean seTrabajaSobreChapa() {
		return true;
	}

	@Override
	public int getTiempo() {
		return 600;
	}

	@Override
	public double getPrecio() {
		return this.getPrecioLista() * this.vehiculo.getSuperficie();
	}

	@Override
	public void aplicar() {
		this.vehiculo.pintar(this.color);
	}

}
