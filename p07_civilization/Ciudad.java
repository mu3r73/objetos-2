package p07_civilization;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ciudad {
	
	private String nombre;
	private Imperio imperio;
	private Collection<EdificioCultural> edifCulturales = new HashSet<>();
	private Collection<EdificioEconomico> edifEconomicos = new HashSet<>();
	private Collection<EdificioMilitar> edifMilitares = new HashSet<>();
	private Collection<UnidadMilitar> uMilitares = new HashSet<>();
	private int cantHabitantes;
	private Estado estado;
	
	// constructores
	
	public Ciudad(String nombre, Imperio imperio, int cantHabitantes) {
		super();
		this.nombre = nombre;
		this.imperio = imperio;
		this.cantHabitantes = cantHabitantes;
		this.estado = new EstadoNormal();
	}
	
	// altas/bajas de/a colecciones
	
	public void agregarEdifCultural(EdificioCultural edificio) {
		this.edifCulturales.add(edificio);
	}
	
	public void agregarEdifEconomico(EdificioEconomico edificio) {
		this.edifEconomicos.add(edificio);
	}
	
	public void agregarEdifMilitar(EdificioMilitar edificio) {
		this.edifMilitares.add(edificio);
	}
	
	public void agregarUMilitar(UnidadMilitar uMilitar) {
		this.uMilitares.add(uMilitar);
	}
	
	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Imperio getImperio() {
		return this.imperio;
	}
	
	public Collection<EdificioMilitar> getEdifMilitares() {
		return this.edifMilitares;
	}
	
	public Collection<UnidadMilitar> getUMilitares() {
		return this.uMilitares;
	}
	
	public int getCantHabitantes() {
		return this.cantHabitantes;
	}
	
	public void setCantHabitantes(int cantHabitantes) {
		this.cantHabitantes = cantHabitantes;
	}
	
	// modificaciones
	
	public void incPoblacionPorc(int porcent) {
		this.cantHabitantes += this.cantHabitantes * porcent / 100;
	}
	
	// consultas
	
	public int getCultura() {
		return this.getCulturaAportadaPorEdificios()
				+ this.getImperio().getCulturaAportadaPorTecnologias();
	}
	
	private int getCulturaAportadaPorEdificios() {
		return this.getEdifStr()
				.mapToInt(edif -> edif.getCultura())
				.sum();
	}
	
	public int getTranquilidad() {
		return this.getEdifStr()
				.mapToInt(edif -> edif.getTranquilidad())
				.sum();
	}
	
	public int getDisconformidad() {
		return this.getImperio().getHumor().getDisconformidad(this);
	}
	
	public int getCantUMilitares() {
		return this.uMilitares.size();
	}
	
	public int getCantEdificiosCulturales() {
		return this.edifCulturales.size();
	}
	
	// req 2
	public Edificio getEdificioMasValioso() {
		return this.getEdifStr()
				.max(Comparator.comparing(edif -> edif.getValor()))
				.get();
	}
	
	// req 3
	public boolean esFeliz() {
		return this.estado.esFeliz(this);
	}
	
	public Collection<Edificio> getEdif() {
		return this.getEdifStr()
				.collect(Collectors.toSet());
	}
	
	private Stream<Edificio> getEdifStr() {
		return Stream.of(this.edifCulturales, this.edifEconomicos, this.edifMilitares)
				.flatMap(Collection::stream);
	}

	public int getIngresosPorTurno() {
		return this.edifEconomicos.stream()
				.mapToInt(edif -> edif.getProduccion())
				.sum();
	}

	public int getEgresosPorTurno() {
		return this.calcEgresosPorTurno(this.getEdifStr());
	}
	
	public int getEgresosPorTurnoEdifEconomicosYMilitares() {
		return this.calcEgresosPorTurno(this.getEdifEconYMil());
	}
	
	private Stream<Edificio> getEdifEconYMil() {
		return Stream.of(this.edifEconomicos, this.edifMilitares)
				.flatMap(Collection::stream);
	}
	
	public int calcEgresosPorTurno(Stream<Edificio> es) {
		return es.mapToInt(edif -> edif.getCostoMantenimiento())
				.sum();
	}

	public int getPotenciaGeneradaPorTurno() {
		return this.edifMilitares.stream()
				.mapToInt(edif -> edif.getPotenciaUnidadesMilitares())
				.sum();
	}
	
	public boolean hayAlgunEdifCulturalQueIrradieMasDe(int cultura) {
		return this.edifCulturales.stream()
				.anyMatch(edif -> edif.getCultura() > cultura);
	}
	
	public EdificioCultural getEdifCulturalConMinIrradiacion() {
		return this.getEdifCulturalConMinIrradiacion(1).stream()
				.findAny()
				.get();
	}
	
	public Collection<EdificioCultural> getEdifCulturalConMinIrradiacion(int cant) {
		return this.edifCulturales.stream()
				.sorted(Comparator.comparing(edif -> edif.getCultura()))
				.limit(cant)
				.collect(Collectors.toSet());
	}
	
	public boolean hayAlgunEdifEconomicoQueProduzcaMasDe(int suma) {
		return this.edifEconomicos.stream()
				.anyMatch(edif -> edif.getProduccion() > suma);
	}

	public EdificioEconomico getEdifEconomicoConMinProduccion() {
		return this.getEdifEconomicoConMinProduccion(1).stream()
				.findAny()
				.get();
	}
	
	public Collection<EdificioEconomico> getEdifEconomicoConMinProduccion(int cant) {
		return this.edifEconomicos.stream()
				.sorted(Comparator.comparing(edif -> edif.getProduccion()))
				.limit(cant)
				.collect(Collectors.toSet());
	}
	
	public int getAporteEdificioCulturalMaxIrradiacion() {
		return this.getEdifCulturalConMaxIrradiacion().getCultura();
	}
	
	private EdificioCultural getEdifCulturalConMaxIrradiacion() {
		return this.edifCulturales.stream()
				.max(Comparator.comparing(edif -> edif.getCultura()))
				.get();
	}
	
	public Optional<EdificioMilitar> getEdifMilitarAleatorio() {
		return this.edifMilitares.stream()
				.skip(Math.round(this.edifMilitares.size() * Math.random()))
				.findFirst();
	}
	
	public int getPotenciaTotDeUMilitares() {
		return this.uMilitares.stream()
				.mapToInt(uMil -> uMil.getPotencia())
				.sum();
	}
	
	public double getCantEdificiosDestacados() {
		return this.getEdifStr()
				.filter(edif -> edif.esDestacado())
				.collect(Collectors.toSet())
				.size();
	}
	
	public Optional<Edificio> getEdificioAleatorio() {
		return this.getEdifStr()
				.skip(Math.round(this.getEdif().size() * Math.random()))
				.findFirst();
	}
	
	// acciones
	
	// req 6
	public void jugarTurno() {
		this.estado.aumentarPoblacion(this);
		this.estado.descontarTesoroImperioMantenimEdif(this);
		this.estado.aumentarTesoroImperioProduccEdifEconomicos(this);
		this.estado.crearUnidadesMilitaresEdifMilitares(this);
		this.estado.boostearEdificios(this);
	}
	
	public void potenciarse() {
		this.estado = this.estado.getEstadoMejor();
	}
	
	public void complicarse() {
		this.estado = this.estado.getEstadoPeor();
	}

}
