package p04_examen;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Resolucion {

	private Aspirante aspirante;
	private Enunciado examen;
	private Set<Respuesta> respuestas = new HashSet<>();

	// altas a colecciones
	
	public void agregarRespuesta(Respuesta respuesta) {
		respuestas.add(respuesta);
	}

	// consultas
	
	public double puntajeObt() {
		return this.respuestas.stream()
				.mapToDouble(respuesta -> respuesta.puntajeObtenido())
				.sum();
	}
	
	public boolean estaAprobada() {
		return this.examen.estaAprobada(this);
	}

	public int cantRespCorrectas() {
		return this.respuestas.stream()
				.filter(respuesta -> respuesta.estaAprobada())
				.collect(Collectors.toSet())
				.size();
	}

	public boolean perteneceA(Aspirante aspirante) {
		return aspirante == this.aspirante;
	}

	public boolean esPara(Enunciado examen) {
		return examen == this.examen;
	}
	
}
