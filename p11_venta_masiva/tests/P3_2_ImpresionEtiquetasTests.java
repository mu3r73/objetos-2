package venta_masiva.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.impresora.ImpresoraLogger;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.pedido.Envio;
import venta_masiva.modelo.pedido.Pedido;

public class P3_2_ImpresionEtiquetasTests {

	private Deposito depo;
	private Store store;
	
	@Before
	public void setUp() {
		this.depo = Deposito.getInstancia();
		this.depo.cargarDatosDePrueba();
		this.depo.eliminarElementos();

		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
	}
	
	@Test
	public void testImprimirEtiqueta() {
		// creo pedido
		Cliente c = this.store.getCliente(12345678);
		Libro l = this.store.getLibro("El Aleph", "Jorge Luis Borges");
		Vino v = this.store.getVino("Crotta");
		Pedido p = new Pedido(c, l, v);
		
		// agrego stock a depósito
		this.depo.hallarSectorSimpleLlamado("B").agregarEjemplares(
				l.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("D").agregarBotellas(
				v.crearBotella(Deposito.getNroInventario()));
		
		// creo envío
		Envio e = p.crearEnvio();
		
		// configuro impresora
		ImpresoraLogger impre = ImpresoraLogger.getInstancia();
		Expedicion.getInstancia().setImpresora(impre);
		
		// creo etiqueta
		e.imprimirEtiqueta();
		List<String> log = impre.getLog();
		
		assertEquals(log.get(0), "Álvaro Álvarez");
		assertEquals(log.get(1), "");
		assertEquals(log.get(2), "12 Nº 21");
		assertEquals(log.get(3), "(7223) General Belgrano");
		assertEquals(log.get(4), "");
		assertEquals(log.get(5), "2 artículos - peso total " + e.getPesoTotal() + " kg");
		assertEquals(log.get(6), "");
	}

}
