package p03_minions;

import java.util.HashSet;
import java.util.Set;

/**
 * representa a un empleado genérico
 */
public abstract class Minion {

	protected float estamina = 0;
	protected Rol rol;
	protected Set<Tarea> tareas = new HashSet<>();

	// getters / setters
	
	public float getEstamina() {
		return estamina;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	// altas a colecciones
	
	/**
	 * agrega una tarea realizada
	 */
	public void agregarTarea(Tarea tarea) {
		this.tareas.add(tarea);
	}

	// consultas
	
	/**
	 * retorna la cantidad de tareasd realizadas
	 */
	public int cantidadDeTareas() {
		return this.tareas.size();
	}

	/**
	 * retorna la experiencia del Minion
	 */
	public double experiencia() {
		return this.cantidadDeTareas() * this.sumarDificultadesTareas();
	}
	
	/**
	 * retorna la suma de las dificultades de las tareas realizadas
	 */
	private double sumarDificultadesTareas() {
		return this.tareas
				.stream()
				.mapToDouble(tarea -> tarea.dificultad(this))
				.sum();
	}

	/**
	 * indica si el Minion puede realizar la tarea
	 */
	public boolean puedeEjecutar(Tarea tarea) {
		return this.rol.puedeEjecutar(tarea);
	}
	
	/**
	 * retorna la dificultad de defender el sector
	 */
	public abstract double dificultadDeDefender(Sector sector);

	/**
	 * indica si el Minion puede arreglar la máquina
	 */
	public boolean puedeArreglar(Maquina maquina) {
		return this.rol.puedeArreglar(maquina);
	};

	/**
	 * indica si el Minion puede defender el sector
	 */
	public boolean puedeDefender(Sector sector) {
		return this.rol.puedeDefender(sector);
	}

	/**
	 * indica si el Minion puede limpiar el sector
	 */
	public boolean puedeLimpiar(Sector sector) {
		return this.rol.puedeLimpiar(sector);
	}

	/**
	 * indica si el Minion tiene suficiente estámina
	 */
	protected boolean tieneSuficienteEstamina(double estamina) {
		return this.estamina >= estamina;
	}

	/**
	 * indica si el Minion tiene suficiente fuerza
	 */
	public boolean tieneSuficienteFuerza(double fuerza) {
		return this.fuerza() >= fuerza;
	}

	/**
	 * retorna la fuerza del Minion
	 */
	protected double fuerza() {
		return this.rol.fuerza();
	}

	// acciones
	
	/**
	 * incrementa estámina al consumir una fruta 
	 */
	public void comer(Fruta fruta) {
		this.incrementarEstamina(fruta.getEstaminaQueAporta());
	}
	
	/**
	 * (intenta) ejecuta(r) la tarea
	 */
	public void ejecutar(Tarea tarea) {
		this.rol.ejecutar(tarea);
	}

	/**
	 * arregla la máquina
	 */
	public void arreglar(Maquina maquina) {
		this.rol.arreglar(maquina);
	}

	/**
	 * defiende el sector
	 */
	public void defender(Sector sector) {
		this.rol.defender(sector);
	}

	/**
	 * limpia el sector
	 */
	public void limpiar(Sector sector) {
		this.rol.limpiar(sector);
	}

	/**
	 * aumenta estámina
	 */
	private void incrementarEstamina(double estamina) {
		this.estamina += estamina;
	}

	/**
	 * reduce estámina
	 */
	protected void reducirEstamina(double estamina) {
		this.estamina -= estamina;
	}

}
