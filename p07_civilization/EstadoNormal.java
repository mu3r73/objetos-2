package p07_civilization;

import java.util.Optional;

public class EstadoNormal extends Estado {
	
	@Override
	public boolean esFeliz(Ciudad ciudad) {
		return ciudad.getTranquilidad() > ciudad.getDisconformidad();
	}
	
	@Override
	public void aumentarPoblacion(Ciudad ciudad) {
		if (ciudad.esFeliz()) {
			ciudad.incPoblacionPorc(5);
		}
	}
	
	@Override
	public void descontarTesoroImperioMantenimEdif(Ciudad ciudad) {
		ciudad.getImperio().decTesoro(ciudad.getEgresosPorTurno());
	}
	
	@Override
	public void aumentarTesoroImperioProduccEdifEconomicos(Ciudad ciudad) {
		ciudad.getImperio().incTesoro(ciudad.getIngresosPorTurno());
	}
	
	@Override
	public void crearUnidadesMilitaresEdifMilitares(Ciudad ciudad) {
		ciudad.getEdifMilitares().forEach(
			edif -> ciudad.getUMilitares().add(edif.crearUnidadMilitar())
		);
	}
	
	@Override
	public void boostearEdificios(Ciudad ciudad) {
		double probabilidad = 0.2 + (ciudad.getCantEdificiosDestacados() * 0.1);
		
		if ((probabilidad >= 1) || (Math.random() < probabilidad)) {
			Optional<Edificio> edif = ciudad.getEdificioAleatorio();
			if (edif.isPresent()) {
				edif.get().boostear();
			}
		}
	}
	
	@Override
	public void boostearEdifCultural(Ciudad ciudad) {
		if (ciudad.hayAlgunEdifCulturalQueIrradieMasDe(100)) {
			ciudad.getEdifCulturalConMinIrradiacion().boostear();
		}
	}
	
	@Override
	public void boostearEdifEconomico(Ciudad ciudad) {
		if (ciudad.hayAlgunEdifEconomicoQueProduzcaMasDe(200)) {
			ciudad.getEdifEconomicoConMinProduccion().boostear();
		}
	}

	@Override
	public void boostearEdifMilitar(Ciudad ciudad) {
		// no pasa nada
	}
	
	@Override
	public Estado getEstadoMejor() {
		return new EstadoExcitada();
	}
	
	@Override
	public Estado getEstadoPeor() {
		return new EstadoConvulsionada();
	}
	
}
