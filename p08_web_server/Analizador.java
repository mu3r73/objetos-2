package p08_web_server;

import java.util.Optional;

// patrón observer
public interface Analizador {
	
	public void registrar(Respuesta respuesta, Optional<Modulo> modulo);
	
}
