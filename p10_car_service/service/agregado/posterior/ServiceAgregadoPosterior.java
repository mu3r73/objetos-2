package p10_car_service.service.agregado.posterior;

import p10_car_service.service.agregado.ServiceAgregado;
import p10_car_service.service.base.ServiceBase;

public abstract class ServiceAgregadoPosterior extends ServiceAgregado {

	public ServiceAgregadoPosterior(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public void aplicar() {
		this.servBase.aplicar();
		this.aplicarAgregado();
	}

}
