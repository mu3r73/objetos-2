package venta_masiva.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Deposito;
import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.expedicion.Expedicion;
import venta_masiva.modelo.item.paquete.Cinta;
import venta_masiva.modelo.item.paquete.Etiqueta;
import venta_masiva.modelo.item.paquete.FilmAlveolar;
import venta_masiva.modelo.item.paquete.Folio;
import venta_masiva.modelo.item.producto.Libro;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.item.producto.Vino;
import venta_masiva.modelo.pedido.Envio;
import venta_masiva.modelo.pedido.Pedido;

public class P2_PedidoEnvioTests {

	private Deposito depo;
	private Store store;
	private Expedicion exped;
	
	@Before
	public void setUp() {
		this.depo = Deposito.getInstancia();
		this.depo.cargarDatosDePrueba();
		this.depo.eliminarElementos();
		
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
		
		this.exped = Expedicion.getInstancia();
		this.exped.eliminarPedidosYEnvios();
	}
	
	@Test
	public void testCrearEnvioAPartirDePedido() {
		// creo pedido
		Libro l1 = this.store.getLibro("El hombre en el castillo", "Philip K. Dick");
		Libro l2 = this.store.getLibro("Un mundo feliz", "Aldous Huxley");
		Vino v = this.store.getVino("Crotta");
		Pedido p = new Pedido(this.store.getCliente(12345678), l1, l2, v);
		
		// agrego stock a depósito
		this.depo.hallarSectorSimpleLlamado("Primer Piso").agregarEjemplares(
				l1.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("A2").agregarEjemplares(
				l2.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("F1").agregarBotellas(
				v.crearBotella(Deposito.getNroInventario()));
		
		// creo envío
		Envio e = p.crearEnvio();
		
		Set<Producto> ps = e.getElementos().stream()
									.map(elemento -> elemento.getProducto())
									.collect(Collectors.toSet());
		assertEquals(ps, new HashSet<>(Arrays.asList(l1, l2, v)));
	}
	
	@Test(expected = RuntimeException.class)
	public void testCrearEnvioAPartirDePedido_inexistente() {
		// no hay stock de productos en depósito
		Libro l = this.store.getLibro("El libro de la tierra negra", "Carlos Gardini");
		Vino v = this.store.getVino("Uvita");
		
		// creo el pedido (incluyendo productos sin stock)
		Pedido p = new Pedido(this.store.getCliente(34567890), l, v);
		
		// acá debería generar excepción
		p.crearEnvio();
	}
	
	@Test
	public void testPesoTotalDeEnvio() {
		Libro l1 = this.store.getLibro("Ubik", "Philip K. Dick"); // usa 1 folio y 3 etiquetas
		Libro l2 = this.store.getLibro("Dioses americanos", "Neil Gaiman"); // usa 2 folios y 6 etiquetas
		Vino v1 = this.store.getVino("Arizu"); // usa 1 film alveolar y 1 cinta
		Vino v2 = this.store.getVino("Crotta"); // usa 1 film alveolar y 3 cintas
		Pedido p = new Pedido(this.store.getCliente(89012345), l1, l2, v1, v2);
		
		// agrego stock a depósito
		this.depo.hallarSectorSimpleLlamado("A1").agregarEjemplares(
				l1.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("A2").agregarEjemplares(
				l2.crearEjemplar(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("F1").agregarBotellas(
				v1.crearBotella(Deposito.getNroInventario()));
		this.depo.hallarSectorSimpleLlamado("F2").agregarBotellas(
				v2.crearBotella(Deposito.getNroInventario()));
		
		// creo envío
		Envio e = p.crearEnvio();
		
		double pesoEsperado = l1.getPeso() + (new Folio()).getPeso() + 3 * (new Etiqueta()).getPeso()
							+ l2.getPeso() + 2 * (new Folio()).getPeso() + 6 * (new Etiqueta()).getPeso()
							+ v1.getPeso() + (new FilmAlveolar()).getPeso() + (new Cinta()).getPeso()
							+ v2.getPeso() + (new FilmAlveolar()).getPeso() + 3 * (new Cinta()).getPeso();
		
		assertEquals(e.getPesoTotal(), pesoEsperado, 0);
	}
	
	@Test
	public void testTieneEnviosEnTransito() {
		Cliente c = this.store.getCliente(45678901);
		c.resetCuentaCorriente();
		
		// no tiene envíos en tránsito
		assertFalse(this.exped.tieneEnviosEnTransito(c));
		
		// agrego un envío
		Pedido p = new Pedido(c);
		this.exped.procesar(p);
		
		// ahora debería tener un envío en tránsito
		assertTrue(this.exped.tieneEnviosEnTransito(c));
	}
	
}
