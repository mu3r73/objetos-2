package p02_pepero;

/**
 * bordador descosedor
 * puede cambiar la leyenda de cualquier uniforme
 */
public class BordadorDescosedor extends Bordador {

	// consultas
	
	/**
	 * indica que el bordador siempre puede cambiar la leyenda del uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return true;
	}

}
