package p02_pepero;

/**
 * material
 * tiene una resistencia dada, y soporta una cantidad máxima de costuras extra
 */
public class Material {

	private String descripcion;
	private double resistencia;
	private int maxCosturasExtra;
	
	// constructores

	public Material(String descripcion, double resistencia, int maxCosturasExtra) {
		super();
		this.descripcion = descripcion;
		this.resistencia = resistencia;
		this.maxCosturasExtra = maxCosturasExtra;
	}

	// getters y setters
	
	public String getDescripcion() {
		return descripcion;
	}

	public double getResistencia() {
		return resistencia;
	}
	
	public int getMaxCosturasExtra() {
		return maxCosturasExtra;
	}
	
}
