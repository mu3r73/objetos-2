package p10_car_service.service.base;

import p10_car_service.Cliente;
import p10_car_service.service.Tipo;
import p10_car_service.service.precios.ListaDePrecios;
import p10_car_service.vehiculo.Nivel;
import p10_car_service.vehiculo.Vehiculo;

public class ServiceTotalDeChapa extends ServiceBase {

	// para poder consultar el precio después de haber aplicado el service
	private int bollos;
	private int rajaduras;
	
	public ServiceTotalDeChapa(Vehiculo vehiculo, Cliente cliente) {
		super(vehiculo, cliente);
		this.bollos = vehiculo.getValor(Nivel.CANT_BOLLOS_CHAPA).intValue();
		this.rajaduras = vehiculo.getValor(Nivel.CANT_RAJADURAS_CHAPA).intValue();
	}

	@Override
	public Tipo getTipo() {
		return Tipo.SERVICE_TOTAL_CHAPA;
	}
	
	@Override
	public boolean seTrabajaSobreChapa() {
		return true;
	}
	
	@Override
	public int getTiempo() {
		return this.getTiempoBollos() + this.getTiempoRajaduras();
	}

	private int getTiempoBollos() {
		return Math.max(120, this.bollos * 30);
	}
	
	private int getTiempoRajaduras() {
		return this.getRajadurasPares() * 40;
	}

	private int getRajadurasPares() {
		if (this.rajaduras % 2 == 0) {
			// cantidad par
			return this.rajaduras;
		} else {
			// cantidad impar
			return this.rajaduras + 1;
		}
	}
	
	@Override
	public double getPrecio() {
		return this.getPrecioBollos() + this.getPrecioRajaduras();
	}

	private double getPrecioBollos() {
		return this.bollos * this.getPrecioDeLista(Tipo.ELIMINACION_BOLLOS);
	}

	private double getPrecioRajaduras() {
		return this.rajaduras * this.getPrecioDeLista(Tipo.ELIMINACION_RAJADURAS);
	}

	private double getPrecioDeLista(Tipo tipo) {
		return ListaDePrecios.getInstancia().getPrecio(tipo);
	}

	@Override
	public void aplicar() {
		this.vehiculo.vaciar(Nivel.CANT_BOLLOS_CHAPA);
		this.vehiculo.vaciar(Nivel.CANT_RAJADURAS_CHAPA);
	}

}
