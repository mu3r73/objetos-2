package p04_examen.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import p04_examen.Enunciado;
import p04_examen.Pregunta;
import p04_examen.PreguntaMultChoice;
import p04_examen.PreguntaNumerica;
import p04_examen.PreguntaSecuencia;

public class ExamenTests {

	@Test
	public void test() {
		
		Enunciado e = new Enunciado();
		Pregunta p1 = new PreguntaMultChoice();
		p1.setPuntaje(5);
		Pregunta p2 = new PreguntaSecuencia();
		p2.setPuntaje(10);
		Pregunta p3 = new PreguntaNumerica();
		p3.setPuntaje(20);
		
		e.agregarPregunta(p1);
		e.agregarPregunta(p2);
		e.agregarPregunta(p3);
		
		assertEquals(35, e.puntajeMax(), 0);
		
	}

}
