package p07_civilization;

public class EdificioCultural extends Edificio {
	
	private int cultura;
	
	// constructores
	
	public EdificioCultural(Ciudad ciudad, int costoMantenimiento, int costoConstruccion, int cultura) {
		super(ciudad, costoMantenimiento, costoConstruccion);
		this.cultura = cultura;
	}
	
	// consultas
	
	@Override
	public int getCultura() {
		return this.cultura;
	}
	
	@Override
	public int getTranquilidad() {
		// double dispatch
		return this.cultura / this.getCiudad().getImperio().getFactorTranquilidad()
				+ this.getCiudad().getImperio().getHumor().getTranquilidad(this);
	}
	
	@Override
	public int getValor() {
		return this.getCultura();
	}
	
	@Override
	public boolean esDestacado() {
		return this.cultura > 100;
	}
	
	// acciones
	
	@Override
	public void boostear() {
		this.cultura *= 2;
	}
	
}
