package p10_car_service.service.agregado.posterior;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Bujia;

public class CambioBujias extends ServiceAgregadoPosterior {

	private boolean yaSeEligioBujia;
	private Bujia bujiaACambiar;

	public CambioBujias(ServiceBase servBase) {
		super(servBase);
		this.yaSeEligioBujia = false;
	}

	@Override
	public Tipo getTipo() {
		return Tipo.CAMBIO_BUJIA;
	}

	@Override
	public boolean seAbreCapot() {
		return true;
	}
	
	@Override
	protected int getTiempoAgregado() {
		if (this.servBase.seAbreCapot()) {
			return 0;
		} else {
			return 60;
		}
	}
	
	@Override
	protected double getPrecioAgregado() {
		if (this.getCliente().esVIP()) {
			return this.getPrecioLista() * 0.80;
		} else {
			return this.getPrecioLista();
		}
	}

	@Override
	public void aplicarAgregado() {
		if (this.haceFaltaCambiar()) {
			this.getVehiculo().sacarBujia(this.bujiaACambiar);
			this.getVehiculo().agregarBujia(new Bujia(10));
			this.bujiaACambiar = null;
		}
	}

	private boolean haceFaltaCambiar() {
		this.seleccionarBujiaACambiar();
		return this.bujiaACambiar != null;
	}

	private void seleccionarBujiaACambiar() {
		if (!this.yaSeEligioBujia) {
			this.bujiaACambiar = this.getVehiculo().getBujiaMenosPotente();
			if (this.bujiaACambiar.getPotencia() > 5) {
				this.bujiaACambiar = null;
			}
			this.yaSeEligioBujia = true;
		}
	}
	
}
