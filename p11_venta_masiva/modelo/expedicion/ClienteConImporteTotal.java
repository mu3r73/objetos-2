package venta_masiva.modelo.expedicion;

import venta_masiva.modelo.cliente.Cliente;

public class ClienteConImporteTotal {
	
	private Cliente cliente;
	private double importeTotal;
	
	// constructor
	public ClienteConImporteTotal(Cliente cliente, double importeTotal) {
		super();
		this.cliente = cliente;
		this.importeTotal = importeTotal;
	}
	
	//getters / setters
	public Cliente getCliente() {
		return this.cliente;
	}
	
	public double getImporteTotal() {
		return this.importeTotal;
	}
	
	public void setImporteTotal(double importe) {
		this.importeTotal = importe;
	}
	
}
