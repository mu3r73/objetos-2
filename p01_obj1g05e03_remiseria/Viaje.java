package p01_obj1g05e03_remiseria;

import java.util.Set;

public class Viaje {

	protected final int distancia; // en km
	protected final double tiempoMaximo; // en horas
	protected final int cantPasajeros;
	protected final Set<String> coloresIncompatibles;	// conj de colores
	protected Vehiculo vehiculo;
	
	// constructor
	public Viaje(int distancia, double tiempoMaximo, int cantPasajeros, Set<String> coloresIncompatibles) {
		super();
		this.distancia = distancia;
		this.tiempoMaximo = tiempoMaximo;
		this.cantPasajeros = cantPasajeros;
		this.coloresIncompatibles = coloresIncompatibles;
	}
	
	public int distancia() {
		return this.distancia;
	}
	
	public int cantPasajeros() {
		return this.cantPasajeros;
	}
	
	public Vehiculo vehiculo() {
		return this.vehiculo;
	}

	public void vehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}
	
	public boolean puedeSerHechoPor(Vehiculo vehiculo) {
		return vehiculo.velocidadMaxima() >= 10 + (this.distancia / this.tiempoMaximo)
			&& vehiculo.capacidad() >= this.cantPasajeros
			&& !coloresIncompatibles.contains(vehiculo.color());
	}

}
