package p07_civilization;

public class UnidadMilitar {
	
	private int potencia;
	
	// constructores
	
	public UnidadMilitar(int potencia) {
		super();
		this.potencia = potencia;
	}
	
	// getters / setters
	
	public int getPotencia() {
		return this.potencia;
	}
	
}
