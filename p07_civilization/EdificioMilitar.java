package p07_civilization;

public class EdificioMilitar extends Edificio {
	
	private int potenciaUnidadesMilitares;

	// constructores
	
	public EdificioMilitar(Ciudad ciudad, int costoMantenimiento, int costoConstruccion, int potenciaUnidadesMilitares) {
		super(ciudad, costoMantenimiento, costoConstruccion);
		this.potenciaUnidadesMilitares = potenciaUnidadesMilitares;
	}
	
	// getters / setters
	
	public int getPotenciaUnidadesMilitares() {
		return this.potenciaUnidadesMilitares;
	}
	
	// consultas
	
	@Override
	public int getCultura() {
		return 0;
	}
	
	@Override
	public int getTranquilidad() {
		// double dispatch
		return 1 + this.getCiudad().getImperio().getHumor().getTranquilidad(this);
	}
	
	@Override
	public boolean esDestacado() {
		return false;
	}
	
	// acciones
	
	public UnidadMilitar crearUnidadMilitar() {
		return new UnidadMilitar(this.potenciaUnidadesMilitares);
	}

	@Override
	public int getValor() {
		return this.getPotenciaUnidadesMilitares();
	}

	@Override
	public void boostear() {
		this.potenciaUnidadesMilitares += 5;
	}
	
}
