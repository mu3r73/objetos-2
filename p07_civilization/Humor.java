package p07_civilization;

public interface Humor {
	
	public int getDisconformidad(Ciudad ciudad);
	
	public int getTranquilidad(EdificioCultural edificio);
	
	public int getTranquilidad(EdificioEconomico edificio);
	
	public int getTranquilidad(EdificioMilitar edificio);
	
}
