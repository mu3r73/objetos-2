package p04_examen;

import java.util.HashSet;
import java.util.Set;

public class PreguntaMultChoice extends Pregunta {

	private Set<String> opciones = new HashSet<>();
	private String opcionCorrecta;

	// getters / setters
	
	public void setOpcionCorrecta(String opcionCorrecta) {
		this.opcionCorrecta = opcionCorrecta;
	}

	// altas a colecciones
	
	public void agregarOpcion(String opcion) {
		this.opciones.add(opcion);
	}
	
	// consultas
	
	public boolean esCorrecta(RespuestaMultChoice respuesta) {
		return respuesta.getOpcion().equals(this.opcionCorrecta);
	}
	
}
