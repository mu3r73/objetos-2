package p10_car_service.service.agregado.preliminar;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class LimpiezaTanqueNafta extends ServiceAgregadoPreliminar {

	public LimpiezaTanqueNafta(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.LIMPIEZA_TANQUE_NAFTA;
	}

	@Override
	protected int getTiempoAgregado() {
		return 180;
	}

	@Override
	protected double getPrecioAgregado() {
		return this.getPrecioLista();
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().vaciar(Nivel.CANT_NAFTA);
	}

}
