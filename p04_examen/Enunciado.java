package p04_examen;

import java.util.HashSet;
import java.util.Set;

public class Enunciado {

	private Set<Pregunta> preguntas = new HashSet<>();
	private double puntajeMin;
	
	// getters / setters
	
	public void setPuntajeMin(double puntajeMin) {
		this.puntajeMin = puntajeMin;
	}
	
	// altas a colecciones
	
	public void agregarPregunta(Pregunta pregunta) {
		this.preguntas.add(pregunta);
	}
	
	// consultas
	
	public double puntajeMax() {
		return preguntas.stream()
				.mapToDouble(pregunta -> pregunta.getPuntaje())
				.sum();
	}
	
	public boolean estaAprobada(Resolucion resolucion) {
		return resolucion.puntajeObt() >= this.puntajeMin;
	}

}
