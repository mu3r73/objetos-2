package p09_examen;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class Busqueda {

	Set<Enunciado> examenes = new HashSet<>();
	Set<Aspirante> aspirantes = new HashSet<>();
	Set<Resolucion> resoluciones = new HashSet<>();
	
	// altas a colecciones
	
	public void agregarExamen(Enunciado examen) {
		this.examenes.add(examen);
	}
	
	public void agregarInscripcion(Aspirante aspirante) {
		this.aspirantes.add(aspirante);
	}

	public void recibirResolucion(Resolucion resolucion) {
		this.resoluciones.add(resolucion);
	}
	
	// consultas
	
	public boolean califica(Aspirante aspirante) {
		return this.sePuedeEvaluar(aspirante);
	}
	
	public Set<Aspirante> aspirantesQueNoSePuedenEvaluar() {
		return this.aspirantes.stream()
				.filter(aspirante -> !this.sePuedeEvaluar(aspirante))
				.collect(Collectors.toSet());
	}
	
	private boolean sePuedeEvaluar(Aspirante aspirante) {
		return this.examenes.stream()
				.allMatch(examen -> aspirante.entregoResolucionPara(examen));
	}

	public Set<Enunciado> examenesSinResolucionDe(Aspirante aspirante) {
		return this.examenes.stream()
				.filter(examen -> aspirante.entregoResolucionPara(examen))
				.collect(Collectors.toSet());
	}
	
	public Set<Aspirante> rankingDeAspirantesCalificados() {
		return this.aspirantesQueCalifican().stream()
				.sorted(Comparator.comparing(Aspirante::puntajeTotObt))
				.collect(Collectors.toSet());
	}
	
	private Set<Aspirante> aspirantesQueCalifican() {
		return this.aspirantes.stream()
				.filter(aspirante -> this.califica(aspirante))
				.collect(Collectors.toSet());
	}
	
	public Set<Resolucion> resolucionesPara(Enunciado examen) {
		return this.resoluciones.stream()
				.filter(resolucion -> resolucion.esPara(examen))
				.collect(Collectors.toSet());
	}

}
