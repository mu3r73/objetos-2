package p03_minions.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import p03_minions.ArreglarMaquina;
import p03_minions.Biclope;
import p03_minions.LimpiarSector;
import p03_minions.Maquina;
import p03_minions.Minion;
import p03_minions.Sector;
import p03_minions.Tarea;

public class ArreglarMaquinaTests {

	@Test
	public void testEjercicio2() {
		Maquina m1 = new Maquina(15);
		Tarea t1 = new ArreglarMaquina(m1);
		Sector s1 = new Sector();
		Tarea t2 = new LimpiarSector(s1);
		Minion emp1 = new Biclope();
		emp1.agregarTarea(t1);
		emp1.agregarTarea(t2);
		assertEquals(80, emp1.experiencia(), 0);
	}

}
