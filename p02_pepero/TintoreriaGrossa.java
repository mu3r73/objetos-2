package p02_pepero;

/**
 * tintorería que puede trabajar sobre cualquier tipo de material y color base
 */
public class TintoreriaGrossa extends Tintoreria {

	/**
	 * ejercicio 2
	 * indica que la tintorería siempre puede cambiar el color del uniforme
	 */
	@Override
	public boolean puedeProcesar(Uniforme uniforme) {
		return true;
	}

}
