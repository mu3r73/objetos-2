package p01_obj1g05e03_remiseria;

public interface Vehiculo {

	public abstract int capacidad();
	
	public abstract int velocidadMaxima();
	
	public abstract int peso();
	
	public abstract String color();
	
}
