package p01_obj1g05e03_remiseria;

import java.util.HashSet;
import java.util.Set;

public class Remiseria {
	
	protected Set<Vehiculo> flota = new HashSet<>();
	protected Set<Viaje> viajes = new HashSet<>();
	protected final int valorPorKm;
	protected final int minimoPorViaje;

	// constructor
	public Remiseria(int valorPorKm, int minimoPorViaje) {
		super();
		this.valorPorKm = valorPorKm;
		this.minimoPorViaje = minimoPorViaje;
	}

	public void agregarAFlota(Vehiculo vehiculo) {
		this.flota.add(vehiculo);
	}

	public void quitarDeFlota(Vehiculo vehiculo) {
		this.flota.remove(vehiculo);
	}

	public void registrarViaje(Viaje viaje, Vehiculo vehiculo) {
		viaje.vehiculo(vehiculo);
		this.viajes.add(viaje);
	}

	public int pesoTotal() {
		int suma = 0;
		for (Vehiculo veh : this.flota) {
			suma += veh.peso();
		}
		return suma;
	}

	public boolean esRecomendable() {
		return this.flota.size() >= 3 && this.todosLosVehiculosSonMasRapidosQue(100);
	}

	private boolean todosLosVehiculosSonMasRapidosQue(int velocidad) {
		for (Vehiculo veh : this.flota) {
			if (veh.velocidadMaxima() < velocidad) {
				return false;
			}
		}
		return true;
	}

	public int capacidadTotalYendoA(int velocidad) {
		int suma = 0;
		for (Vehiculo veh : this.flota) {
			if (veh.velocidadMaxima() >= velocidad) {
				suma += veh.capacidad();
			}
		}
		return suma;
	}
	
	public String colorDelAutoMasRapido() {
		return this.autoMasRapido().color();
	}
	
	private Vehiculo autoMasRapido() {
		Vehiculo vmr = null;
		int velmax = -1;
		for (Vehiculo veh : this.flota) {
			if (veh.velocidadMaxima() > velmax) {
				velmax = veh.velocidadMaxima();
				vmr = veh;
			}
		}
		return vmr;
	}
	
	public Set<Vehiculo> queVehiculosPuedenHacer(Viaje viaje) {
		Set<Vehiculo> vs = new HashSet<>();
		for (Vehiculo veh : this.flota) {
			if (viaje.puedeSerHechoPor(veh)) {
				vs.add(veh);
			}
		}
		return vs;
	}
	
	public int cuantosViajesHizo(Vehiculo vehiculo) {
		int cant = 0;
		for (Viaje viaje : this.viajes) {
			if (viaje.vehiculo() == vehiculo) {
				cant++;
			}
		}
		return cant;
	}
	
	public int viajesRealizadosDistanciaMayorA(int km) {
		int cant = 0;
		for (Viaje viaje : this.viajes) {
			if (viaje.distancia() > km) {
				cant++;
			}
		}
		return cant;
	}
	
	public int totalLugaresLibresEnViajesRealizados() {
		int libres = 0;
		for (Viaje viaje : this.viajes) {
			libres += viaje.vehiculo().capacidad() - viaje.cantPasajeros();
		}
		return libres;
	}
	
	public int costo(Viaje viaje) {
		return viaje.distancia() * this.valorPorKm;
	}
	
	public int cuantoPagarPor(Viaje viaje) {
		if (this.costo(viaje) > this.minimoPorViaje) {
			return this.costo(viaje);
		} else {
			return this.minimoPorViaje;
		}
	}
	
	public int cuantoPagarleA(Vehiculo vehiculo) {
		int total = 0;
		for (Viaje viaje : this.viajes) {
			if (viaje.vehiculo() == vehiculo) {
				total += this.cuantoPagarPor(viaje);
			}
		}
		return total;
	}
	
}
