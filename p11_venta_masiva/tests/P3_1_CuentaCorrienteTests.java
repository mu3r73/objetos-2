package venta_masiva.tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import venta_masiva.modelo.Store;
import venta_masiva.modelo.cliente.Cliente;
import venta_masiva.modelo.cliente.Pago;
import venta_masiva.modelo.item.producto.Producto;
import venta_masiva.modelo.pedido.Pedido;

public class P3_1_CuentaCorrienteTests {

	private Store store;
	
	@Before
	public void setUp() {
		this.store = Store.getInstancia();
		this.store.cargarDatosDePrueba();
	}
	
	@Test
	public void testSaldoDeCliente() {
		Cliente c = this.store.getCliente(67890123);
		c.resetCuentaCorriente();
		
		// antes de que haga pedidos
		assertEquals(c.getSaldo(), 0, 0);
		
		Producto p1 = this.store.getLibro("Ficciones", "Jorge Luis Borges");
		Producto p2 = this.store.getLibro("El libro de la tierra negra", "Carlos Gardini");
		Producto p3 = this.store.getVino("Uvita");
		Producto p4 = this.store.getVino("Arizu");
		
		Pedido ped1 = new Pedido(c, p1, p3);
		Pedido ped2 = new Pedido(c, p2, p4);
		
		// después de cargarle los 2 pedidos
		double totGastado = ped1.getPrecioVentaTotal() + ped2.getPrecioVentaTotal();
		assertEquals(c.getSaldo(), -totGastado, 0);
		
		Pago pago1 = new Pago(c, 100);
		Pago pago2 = new Pago(c, 300);

		// después de realizar 2 pagos
		double totPagado = pago1.getMonto() + pago2.getMonto();
		assertEquals(c.getSaldo(), totPagado - totGastado, 0);
	}
	
	@Test
	public void testMovimientosDeClienteOrdenadosPorFecha() {
		Cliente c = this.store.getCliente(78901234);
		c.resetCuentaCorriente();
		
		Producto p1 = this.store.getLibro("Orgullo y prejuicio y zombis", "Seth Grahame-Smith", "Jane Austen");
		Producto p2 = this.store.getVino("Arizu");
		Producto p3 = this.store.getVino("Crotta");
		
		Pedido ped1 = new Pedido(c, LocalDateTime.parse("2017-10-30T17:00:30"), p1);
		Pago pago1 = new Pago(c, LocalDateTime.parse("2017-10-30T17:05:08"), 100);
		Pedido ped2 = new Pedido(c, LocalDateTime.parse("2017-10-30T17:08:26"), p2, p3);
		Pago pago2 = new Pago(c, LocalDateTime.parse("2017-10-30T17:12:43"), 500);
		
		assertEquals(c.getMovimientosOrdenFecha(), Arrays.asList(ped1, pago1, ped2, pago2));
	}

}
