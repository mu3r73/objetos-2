package p01_obj1g05e03_remiseria.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import p01_obj1g05e03_remiseria.ChevroletCorsa;
import p01_obj1g05e03_remiseria.Viaje;

public class ViajeTests {

	protected final Set<String> colVerdeYAzul =
			new HashSet<>(Arrays.asList("verde", "azul"));
	protected final Set<String> colRojoVerdeYAzul =
			new HashSet<>(Arrays.asList("rojo", "verde", "azul"));
	
	@Test
	public void testViajePuedeSerHechoPor() {
	
		assertTrue((new Viaje(140, 1, 4, colVerdeYAzul))
				.puedeSerHechoPor(new ChevroletCorsa("rojo")));
		
		assertFalse((new Viaje(150, 1, 4, colVerdeYAzul))
				.puedeSerHechoPor(new ChevroletCorsa("rojo")));
		
		assertFalse((new Viaje(140, 1, 5, colVerdeYAzul))
				.puedeSerHechoPor(new ChevroletCorsa("rojo")));
		
		assertFalse((new Viaje(140, 1, 4, colRojoVerdeYAzul))
				.puedeSerHechoPor(new ChevroletCorsa("rojo")));
		
	}

}
