package p08_web_server;

import java.net.MalformedURLException;
import java.net.URL;

public class WSURL {

	private URL url;
	
	// constructores
	
	public WSURL(String url) {
		super();
		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {
			throw new RuntimeException("URL no válida");
		}
	}
	
	// getters / setters
	
	public String getProtocolo() {
		return this.url.getProtocol();
	}
	
	public String getRuta() {
		return this.url.getPath();
	}
	
	public String getExtension() {
		String fileName = this.url.getFile();
		return fileName.substring(fileName.lastIndexOf('.') + 1);
	}
	
}
