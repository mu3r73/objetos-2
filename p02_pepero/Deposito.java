package p02_pepero;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * depósito
 * requiere uniformes con un grado mínimo de resistencia (general de la clase)
 * requiere uniformes de color negro, azul o marrón
 */
public class Deposito extends Cliente {

	private static double resistenciaMin = 7;
	private Set<String> coloresAceptables =
			new HashSet<>(Arrays.asList("negro", "azul", "marron"));
	private final static String colorPreferido = "negro";
	
	// constructores
	
	public Deposito(String nombre) {
		super(nombre);
	}
	
	// getters y setters
	
	public static void setResistenciaMin(double resistenciaMin) {
		Deposito.resistenciaMin = resistenciaMin;
	}

	// consultas
	
	/**
	 * retorna el color requerido por el depósito
	 */
	@Override
	public String colorRequerido() {
		return Deposito.colorPreferido;
	}

	/**
	 * indica si el uniforme es del color requerido
	 */
	@Override
	public boolean tieneColorCorrecto(Uniforme uniforme) {
		return this.coloresAceptables.contains(uniforme.getColor());
	}

	/**
	 * indica si el uniforme tiene la resistencia requerida
	 */
	@Override
	public boolean tieneResistenciaCorrecta(Uniforme uniforme) {
		return uniforme.resistencia() >= Deposito.resistenciaMin;
	}

}
