package venta_masiva.modelo.cliente;

import java.time.LocalDateTime;

/**
 * representa un pago efectuado - tiene un cliente, una fecha y un monto
 */
public class Pago extends Movimiento {

	private double monto;
	
	// constructores
	public Pago(Cliente cliente, double monto) {
		this(cliente, LocalDateTime.now(), monto);
	}
	
	public Pago(Cliente cliente, LocalDateTime fecha, double monto) {
		super(cliente, fecha);
		this.monto = monto;
		cliente.agregarPago(this);
	}

	// getters / setters
	public double getMonto() {
		return this.monto;
	}

}
