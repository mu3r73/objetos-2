package p10_car_service.service.agregado.posterior;

import p10_car_service.service.Tipo;
import p10_car_service.service.base.ServiceBase;
import p10_car_service.vehiculo.Nivel;

public class CargaNafta extends ServiceAgregadoPosterior {

	public CargaNafta(ServiceBase servBase) {
		super(servBase);
	}

	@Override
	public Tipo getTipo() {
		return Tipo.CARGA_NAFTA;
	}

	@Override
	protected int getTiempoAgregado() {
		if (this.servBase.getTiempo() >= 120) {
			return 0;
		} else {
			return 20;
		}
	}
	
	@Override
	protected double getPrecioAgregado() {
		return this.getPrecioLista();
	}

	@Override
	public void aplicarAgregado() {
		this.getVehiculo().cargar(Nivel.CANT_NAFTA, 20);
	}

}
