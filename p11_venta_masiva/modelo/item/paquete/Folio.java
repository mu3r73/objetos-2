package venta_masiva.modelo.item.paquete;

/**
 * representa un envoltorio tipo folio, tiene un peso (y una superficie)
 */
public class Folio extends Envoltorio {
	
	// constructor
	public Folio() {
		super(60);
	}
	
	// consultas
	/**
	 * retorna la superficie del folio
	 * @return superficie del folio
	 */
	public static double getSuperficie() {
		return 1500;
	}

}
